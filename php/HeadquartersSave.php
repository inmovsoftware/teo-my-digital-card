<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', '1');
session_start();
require('../include/mysql_class.php');

/* declaracion de variables */
$CompanyId 				= $_REQUEST[CompanyId];
$HeadquatersName 		= $_REQUEST[HeadquatersName];
$CompanyLanguage   		= $_REQUEST[CompanyLanguage];
$CompanyAddress   		= $_REQUEST[CompanyAddress];
$CompanyPhone    		= $_REQUEST[CompanyPhone];

$Phone=substr($CompanyPhone,5,strlen($CompanyPhone)-5);
$Phone=str_replace('(','',$Phone);
$Phone=str_replace(')','',$Phone);

$CountryPhone=substr($CompanyPhone,1,3);
$CountryPhone=ltrim($CountryPhone,'0');
$CountryPhone='+'.$CountryPhone;

$CompanyCountry		= $_REQUEST[CompanyCountry];
$CompanyState    	= $_REQUEST[CompanyState];
$CompanyCity    	= $_REQUEST[CompanyCity];
$CompanyLatitude    = $_REQUEST[CompanyLatitude];
$CompanyLongitude	= $_REQUEST[CompanyLongitude];
$CompanyPlaceID    	= $_REQUEST[CompanyPlaceID];

$Id            	= $_REQUEST[id];
$action        	= $_REQUEST[action];
$activate 	   	= $_REQUEST[activate];
$delete 	   	= $_REQUEST[delete];
$open 	   	   	= $_REQUEST[open];

if($activate=='true'){
	// NO EXISTE ESTA OPCION
	//$sql="UPDATE `it_business` SET `status` = 'A',deleted_at = '' 
	//	WHERE id = '$Id'";
	//$micon->consulta($sql);	
}else{	
	if($delete=='true'){
		$sql="DELETE FROM `it_branches`WHERE id = '$Id'";
		$micon->consulta($sql);	
		
	}else{
		if($action=='Insert'){
			
			$sql="INSERT INTO `it_branches` 
				(
				`it_business_id`, 
				`name`, 
				`is_main`, 
				`address`, 
				`city_from_map`, 
				`state_from_map`, 
				`country_from_map`, 
				`phone`, 
				`country_phone`, 
				`languaje`, 
				`latitude`, 
				`longitude`, 
				`placeid`, 
				`created_at`
				)
				VALUES
				(
				'$CompanyId', 
				'$HeadquatersName', 
				'0', 
				'$CompanyAddress', 
				'$CompanyCity', 
				'$CompanyState', 
				'$CompanyCountry', 
				'$Phone', 
				'$CountryPhone', 
				'$CompanyLanguage', 
				'$CompanyLatitude', 
				'$CompanyLongitude', 
				'$CompanyPlaceID',
				now());";
			//echo $sql.'<br>';		
			$micon->consulta($sql);	
			
			/// Crea Redes Sociales ///
			
			// Toma id de Branch
			
			$sql = "SELECT @@identity AS id";
			$micon->consulta($sql);
			$row = $micon->campoconsultaA();
			$BranchID = trim($row[id]);
				
		}else{
			/// actualiza sucursal ///
			// complementa información de la BRANCH is_main 
			$Phone=substr($CompanyPhone,5,strlen($CompanyPhone)-5);
			$Phone=str_replace('(','',$Phone);
			$Phone=str_replace(')','',$Phone);
			
			$CountryPhone=substr($CompanyPhone,1,3);
			$CountryPhone=ltrim($CountryPhone,'0');
			$CountryPhone='+'.$CountryPhone;
			
			$sql="UPDATE `it_branches` 
					SET
					`it_business_id` 	= '$CompanyId 	', 
					`address` 			= '$CompanyAddress', 
					`name` 				= '$HeadquatersName', 
					`city_from_map` 	= '$CompanyCity', 
					`state_from_map`	= '$CompanyState', 
					`country_from_map` 	= '$CompanyCountry', 
					`phone` 			= '$Phone', 
					`country_phone` 	= '$CountryPhone', 
					`languaje` 			= '$CompanyLanguage', 
					`latitude` 			= '$CompanyLatitude', 
					`longitude` 		= '$CompanyLongitude', 
					`placeid` 			= '$CompanyPlaceID', 
					`updated_at` =  now()
				
				WHERE
				`id` = '$Id' ";
			//echo $sql.'<br>';		
			$micon->consulta($sql);
			// Toma id de Branch
			$BranchID=$Id;
		}	
		/// actualiza redes sociales ///
		// borra todos los registros y inserta lo nuevo
		$sql="DELETE FROM `it_branch_social_network` 
				WHERE `it_branches_id` ='$BranchID'";
		$micon->consulta($sql);
		
		// Inserta nuevos registos
		$qtySocial = $_REQUEST[qtySocial];
		$miOrden=1;
		for ($i = 0; $i <= $qtySocial; $i++) {
			$Var	='MiSocialData'.$i;
			$VarData=$_REQUEST[$Var];
			$Var	='MiSocialKey'.$i;
			$VarKey=$_REQUEST[$Var];
			if($VarData!=""){
				$sql="INSERT INTO `it_branch_social_network` 
						(
						`it_branches_id`, 
						`it_social_networks_id`, 
						`account`, 
						`order`, 
						`created_at`
						)
						VALUES
						(
						'$BranchID', 
						'$VarKey', 
						'$VarData', 
						'$miOrden', 
						NOW() 
						)";
				$micon->consulta($sql);
				$miOrden++;
			}	
		}

	}		
}
//echo $sql;
//$micon->consulta($sql);
 
if($activate=='true' or $delete=='true'){
	header("Location: ../HeadquartersListtersList.php?open=".$open);
}else{
	if($action=='Insert'){
		header("Location: ../HeadquartersCreate.php?open=".$open);
	}else{
		header("Location: ../HeadquartersUpdate.php?open=".$open."&edit=true&id=".$Id);
	}
}	 
?>