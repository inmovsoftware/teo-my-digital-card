<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', '1');
session_start();
require('../include/mysql_class.php');
require('../include/resizeImagen.php');

/* declaracion de variables */
$CompanyPhoto           = $_REQUEST[CompanyPhoto];
$CompanyName            = $_REQUEST[CompanyName];
$CompanyName            = $_REQUEST[CompanyName];
$CompanyAlias           = $_REQUEST[CompanyAlias];
$CompanyInfo            = $_REQUEST[CompanyInfo];
$CompanyLanguage        = $_REQUEST[CompanyLanguage];
$CompanyPhone           = $_REQUEST[CompanyPhone];
$CompanyLeads           = $_REQUEST[CompanyLeads];
$CompanyBackgroundColor = $_REQUEST[CompanyBackgroundColor];
$CompanyTextColor       = $_REQUEST[CompanyTextColor];

$CompanyWebsite         = $_REQUEST[CompanyWebsite];
$CompanyFacebook        = $_REQUEST[CompanyFacebook];
$CompanyInstagram       = $_REQUEST[CompanyInstagram];
$CompanyLinked          = $_REQUEST[CompanyLinked];
$CompanyYoutube         = $_REQUEST[CompanyYoutube];
$CompanyTwitter         = $_REQUEST[CompanyTwitter];
$CompanyGooglePlus      = $_REQUEST[CompanyGooglePlus];

$CompanyAddress         = $_REQUEST[CompanyAddress];
$CompanyCountry         = $_REQUEST[CompanyCountry];
$CompanyState           = $_REQUEST[CompanyState];
$CompanyCity            = $_REQUEST[CompanyCity];
$CompanyLatitude        = $_REQUEST[CompanyLatitude];
$CompanyLongitude       = $_REQUEST[CompanyLongitude];
$CompanyPlaceID         = $_REQUEST[CompanyPlaceID];


$useradmin              = $_REQUEST[useradmin];
$contryphone            = $_REQUEST[contryphone];
$password               = password_hash(  $_REQUEST[password]  , PASSWORD_DEFAULT, ['cost' => 12]);

$Id                     = $_REQUEST[id];
$action                 = $_REQUEST[action];
$activate               = $_REQUEST[activate];
$delete                 = $_REQUEST[delete];
$open                   = $_REQUEST[open];

if($activate=='true'){
	$sql="UPDATE `it_business` SET `status` = 'A',deleted_at = '' 
		WHERE id = '$Id'";
	$micon->consulta($sql);	
}else{	
	if($delete=='true'){
		$sql="UPDATE `it_business` SET `status` = 'S',deleted_at =  NOW()
			WHERE id = '$Id'";
		$micon->consulta($sql);	
		
	} else {
		if($action=='Insert'){
			$base64_string = $_POST['CompanyPhoto'];
			$data = explode(',', $base64_string);
			$content = base64_decode($data[1]);
			$CompanyPhotoName = 'temp_' . time() . '.jpeg';
			$sql="INSERT INTO `it_business` 
					(
					`name`, 
					`alias`, 
					`logo`, 
					`backgroundcolor`, 
					`textcolor`, 
					`html`, 
					`is_partner`
					,mailadmin
					,passadmin,
					`created_at`
					)
					VALUES
					(
					'$CompanyName', 
					'$CompanyAlias', 
					'$CompanyPhotoName', 
					'$CompanyBackgroundColor', 
					'$CompanyTextColor', 
					'$CompanyInfo',
					true
					,'$useradmin'
					,'$password'
						,now()
					)";
			if (!$micon->consulta($sql)) {
				header("Location: ../PartnerList.php?&menss=NO_CREATED");
				return;
			}
			// toma el id generado para grabar la imagen
			$sql = "SELECT @@identity AS id";
			$micon->consulta($sql);
			$row = $micon->campoconsultaA();
			$id = trim($row[id]);
			$Id = trim($row[id]); // para actualizar BRACH

			//// sacar el id de branches 
			$sql="SELECT id FROM it_branches WHERE it_business_id ='".$Id."'";
			$micon->consulta($sql);
			$DtaBranches = $micon->campoconsultaA();
			
			// Nombre foto
			$nameF = $CompanyPhotoName;
			$foto = $nameF;
			$directorio = '../srvs/media/images/business'; // directorio de tu elección
			
			// al macenar imagen inicial en el servidor
			$file = fopen($directorio.'/'.$foto, "wb");
			fwrite($file, $content);
			$sql="UPDATE `it_business` 	SET `logo` = '$foto', `updated_at` = NOW() WHERE `id` = '$Id'";
			$micon->consulta($sql);

			//// crear position 
			$sql = "INSERT INTO it_positions ( it_business_id, name,  created_at)
					VALUES ('".$Id."', 'Administrator', NOW());";
			$micon->consulta($sql);

			$sql = "SELECT @@identity AS id";
			$micon->consulta($sql);
			$DtaPosition = $micon->campoconsultaA();
			$idPosition = trim($DtaPosition[id]);
			$IdPosition = trim($DtaPosition[id]);

			/// para  crear perfiles 
			$sql = "INSERT INTO it_profiles ( it_business_id,  name, created_at) 
					VALUES ('$Id','App User',NOW()),
							('$Id','Partner Administrator',NOW())";
			$micon->consulta($sql);	

			/// ultimo id de perfiles 	
			$sql = "SELECT MAX(id) AS id FROM it_profiles  WHERE it_business_id='".$Id."'";
			$micon->consulta($sql);
			$DtaProfile = $micon->campoconsultaA();
			$IdPro = trim($DtaProfile[id]);


			$sql = "INSERT INTO it_profile_option (it_profiles_id, it_options_id, created_at) VALUES ('$IdPro',4, NOW()),('$IdPro',6, NOW()),('$IdPro',9, NOW()),('$IdPro',10, NOW()),('$IdPro',11, NOW()),('$IdPro',12, NOW()),('$IdPro',16, NOW()),('$IdPro',19, NOW()),('$IdPro',1, NOW()),('$IdPro',20, NOW()),('$IdPro',15, NOW())";
			$micon->consulta($sql);

			/// Crear Usuario
			$sql = "INSERT INTO it_users (it_branches_id, email, login_alt, password,name,it_profile_id, it_positions_id )
					VALUE ('".$IdBranches."', '".$useradmin."', '".$useradmin."', '".$password."',name,'".$IdPro."', '".$IdPosition."' )";
			$micon->consulta($sql);

			/// actualiza LEADS ///

			// Grabando LEADS
			if($CompanyLeads!="0") {
				$sql="UPDATE `it_leads` SET `status`='P' ,`it_business_id`='$Id', `updated_at` = NOW() WHERE `id`='$CompanyLeads'";
				$micon->consulta($sql);
			}
		} else {
			// actualiza empresa
			$sql0="UPDATE `it_business` 
					SET
					`name` = '$CompanyName', 
					`alias` = '$CompanyAlias', ";

					$sql2="	`backgroundcolor` = '$CompanyBackgroundColor', 
					`textcolor` = '$CompanyTextColor ', 
					`html` = '$CompanyInfo', 
					`updated_at` = now()
					
					WHERE
					`id` = '$Id'";
			$sql=$sql0.$sql1.$sql2;	
			//echo $sql;
			if (!$micon->consulta($sql)) {
				header("Location: ../PartnerList.php?&menss=NO_CREATED");
				return;
			}
		}
		
		$sql="UPDATE `it_branches` 
				SET
				`address` 			= '$CompanyAddress', 
				`city_from_map` 	= '$CompanyCity', 
				`state_from_map`	= '$CompanyState', 
				`country_from_map` 	= '$CompanyCountry', 
				`phone` 			= '$CompanyPhone', 
				`country_phone` 	= '$contryphone',
				`languaje` 			= 'EN', 
				`latitude` 			= '$CompanyLatitude', 
				`longitude` 		= '$CompanyLongitude', 
				`placeid` 			= '$CompanyPlaceID',
				`country_phone`    = '$_POST[country_phone]',
				`updated_at` =  now()
			
			WHERE
				`it_business_id` = '$Id' and `is_main`=1 ";
		if (!$micon->consulta($sql)) {
			$menss = $action=='Insert' ? 'NO_CREATED' : 'NO_UPDATED';
			header("Location: ../PartnerList.php?&menss=" . $menss);
			return;
		}
		
		/// actualiza LEADS ///
		// Primero borra dato anterior
		$sql="UPDATE `it_leads` SET `status`='A' ,`it_business_id`='NULL' , `updated_at` = NOW() WHERE `it_business_id`='$Id'";
		$micon->consulta($sql);
		
		// Grabando LEADS
		if($CompanyLeads!="0"){
			$sql="UPDATE `it_leads` SET `status`='P' ,`it_business_id`='$Id', `updated_at` = NOW() WHERE `id`='$CompanyLeads'";
			//echo $sql."<br>";
			$micon->consulta($sql);
		}
		
					
		/////
		/// actualiza REDES ///
		// Toma id de Branch Princial
		$sql="SELECT `id` FROM `it_branches` WHERE `it_business_id`='$Id' AND is_main=1";
		$micon->consulta($sql); $row=$micon->campoconsultaA();
		$BranchID= $row[id];
		// Borra los datos actuales
		$sql="DELETE FROM `it_branch_social_network` WHERE	`it_branches_id` = '$BranchID'";
		$micon->consulta($sql);

		// Inserta nuevos registos
		$qtySocial = $_REQUEST[qtySocial];
		$miOrden=1;
		for ($i = 0; $i <= $qtySocial; $i++) {
			$Var	='MiSocialData'.$i;
			$VarData=$_REQUEST[$Var];
			$Var	='MiSocialKey'.$i;
			$VarKey=$_REQUEST[$Var];
			if($VarData!=""){
				$sql="INSERT INTO `it_branch_social_network` 
						(
						`it_branches_id`, 
						`it_social_networks_id`, 
						`account`, 
						`order`, 
						`created_at`
						)
						VALUES
						(
						'$BranchID', 
						'$VarKey', 
						'$VarData', 
						'$miOrden', 
						NOW() 
						)";
				$micon->consulta($sql);
				$miOrden++;
			}	
		}
	}		
}
//echo $sql;
//$micon->consulta($sql);

if($activate=='true' or $delete=='true'){
	header("Location: ../PartnerList.php?open=".$open);
}else{
	if($action=='Insert'){
		header("Location: ../PartnerList.php?&menss=CREATED");
	}else{
		header("Location: ../PartnerList.php?&menss=UPDATED");
	}
}	 
?>