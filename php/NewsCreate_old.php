<?
session_start();
require('../include/mysql_class.php');

/* declaracion de variables */
$companyID         = $_REQUEST[companyID];
$InputTitle        = $_REQUEST[InputTitle];
$UserId            = $_REQUEST[UserId];
$id                = $_REQUEST[id];
$action            = $_REQUEST[action];
$delete            = $_REQUEST[delete];
$activate          = $_REQUEST[activate];
$open              = $_REQUEST[open];
$InputIntroductory = filter_var( $_REQUEST['InputIntroductory'] ,FILTER_SANITIZE_STRING );
$InputPhoto        = $_REQUEST[InputPhoto];
$FileInputPhoto    = $_FILES[InputPhoto];
$InputDatePub      = $_REQUEST[InputDatePub];
$InputDateEnd      = $_REQUEST[InputDateEnd];
$InputBranch       = $_REQUEST[InputBranch];
$InputCategory     = $_REQUEST[InputCategory];
$InputPosition     = $_REQUEST[InputPosition];
$InputGroup        = $_REQUEST[InputGroup];
$inputType         = $_REQUEST[inputType];
$InputSend         = $_REQUEST[InputSend];
//$InputText       = filter_var( $_REQUEST['InputText'] ,FILTER_SANITIZE_STRING );
$InputText         =  $_REQUEST['InputText'];

$fileExt           = pathinfo($_FILES[InputPhoto][name], PATHINFO_EXTENSION);
$ruta              = 'srvs/media/images/news/';
$path              = getcwd();
$name              .= date("YmdHis");
$name              .= substr(md5(rand(0, PHP_INT_MAX)), 10);
$ruta              = str_replace('php', $ruta, $path);
$Images            =   'Cover-'.$name.'.'.$fileExt;
$destination       = $ruta . $Images ; //change this directory
$location          = $_FILES["InputPhoto"]["tmp_name"];


//echo $_FILES[InputPhoto][name].'<br>';


//echo $action;

if($activate=='true'){
	$sql = "UPDATE `it_news` SET `status` = 'A', `deleted_at` = 'NULL' WHERE `id` = '$id'";
	$micon->consulta($sql);
}else{
	if($delete=='true'){
		$sql = "UPDATE `it_news` SET `status` = 'C', `deleted_at` = NOW() WHERE `id` = '$id'";
		//echo $sql;				
		$micon->consulta($sql);	
	}else{
		if($action=='Insert'){
			
            move_uploaded_file($location, $destination);

			$sql="INSERT INTO it_news (id_business_id,it_users_id,title,photo,it_categories_news_id,creation_date,publication_date,
				close_date,intro,it_news.text, it_news.status, created_at)
				VALUES
				  ( '".$companyID ."', '".$UserId."', '".$InputTitle."', '".$Images."','".$InputCategory."', NOW(), '".$InputDatePub."','".$InputDateEnd."',  '".$InputIntroductory."', '".$InputText."',  'A',  NOW() );";
		//echo $sql;	   
			$micon->consulta($sql);	  

		/*saber el ultimo id de news*/
			$sql = "SELECT MAX(id) Idmax FROM it_news WHERE id_business_id='".$companyID."'";
			$micon->consulta($sql);
			$DtaId = $micon->campoconsultaA();
			$id = $DtaId[Idmax];			
			
		}else{
			$sql = "UPDATE it_news SET  title = '".$InputTitle."',  it_categories_news_id = '".$InputCategory."', 
				update_date = NOW(), publication_date = '".$InputDatePub."', close_date = '".$InputDateEnd."', intro = '".$InputIntroductory."', 
				text = '".$InputText."', updated_at = NOW() WHERE id = '".$id."';";
				//echo $sql.'<br>';
			$micon->consulta($sql);	

			if($_FILES[InputPhoto][name]!=''){
				$sql ="update it_news set photo = '".$Images."' ,  updated_at = NOW() WHERE id = '".$id."';";
			//	echo $sql;
				$micon->consulta($sql);
			//	echo $destination;
				move_uploaded_file($location, $destination);
			}

		}	

			/* $InputBranch*/
			$sql = "delete from it_new_branch where it_news_id = '".$id."'";
			//echo $sql;
			$micon->consulta($sql);
			if(count($InputBranch)>0){
				foreach ($InputBranch as  $ranch) {
					$sql = "INSERT INTO it_new_branch (it_news_id,it_branch_id,created_at)
						VALUES ('".$id."','".$ranch."',NOW() );";
					$micon->consulta($sql);	
				}
			}
			$sql = "delete from it_new_position where it_news_id = '".$id."'";
			$micon->consulta($sql);
			if(count($InputPosition)>0){
				foreach ($InputPosition as  $Position) {
					$sql = "INSERT INTO it_new_position (it_news_id,it_position_id,created_at)
						VALUES ('".$id."','".$Position."',NOW() );";
					$micon->consulta($sql);	
				}
			}
			$sql = "delete from it_new_groupuser where it_news_id = '".$id."'";
			$micon->consulta($sql);
			if(count($InputGroup)>0){
				foreach ($InputGroup as  $Group) {
					$sql = "INSERT INTO it_new_groupuser (it_news_id,it_groups_users_id,created_at)
						VALUES ('".$id."','".$Group."',NOW() );";
					$micon->consulta($sql);	
				}
			} 
	}
}	
//echo $sql;

header("Location: ../NewsList.php?open=".$open);
?>