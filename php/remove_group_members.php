<?
session_start();
require('../include/mysql_class.php');
include('../include/seguridad.php');

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', '1');

if (empty($_POST['group'])) {
    die('Group is required');
}
if (empty($_POST['user'])) {
    die('user is required');
}
$sql = "DELETE FROM it_user_group WHERE it_users_id = " . $_POST[user] . " AND it_groups_users_id = " . $_POST[group];
if (!$micon->consulta($sql)) {
    header("HTTP/1.1 500 Internal Server Error");
    echo $micon->Error;
}