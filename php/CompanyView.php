<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', '1');
session_start();
require('../include/mysql_class.php');
require('../include/resizeImagen.php');



/* declaracion de variables */
$Id						= $_REQUEST[id];
$CompanyPhoto 			= $_REQUEST[CompanyPhoto];
$CompanyName 			= $_REQUEST[CompanyName];
$CompanyAlias	  		= $_REQUEST[CompanyAlias];
$CompanyInfo    		= $_REQUEST[CompanyInfo];
$CompanyLanguage   		= $_REQUEST[CompanyLanguage];
$CompanyAddress   		= $_REQUEST[CompanyAddress];
$CompanyPhone    		= $_REQUEST[CompanyPhone];
$CompanyBackgroundColor	= $_REQUEST[CompanyBackgroundColor];
$CompanyTextColor    	= $_REQUEST[CompanyTextColor];
$open					= $_REQUEST[open];

$CompanyPhotoName = $_FILES['CompanyPhoto']['name'];

/// actualiza BUSINESS ///
$sql="UPDATE `it_business` 
		SET
		`name` 				= '$CompanyName', 
		`alias` 			= '$CompanyAlias', 
		`backgroundcolor` 	= '$CompanyBackgroundColor', 
		`textcolor` 		= '$CompanyTextColor ', 
		`html` 				= '$CompanyInfo', 
		`updated_at` = NOW()
		WHERE `id` = '$Id'";
if (!$micon->consulta($sql)) {
	header("Location: ../CompanyView.php?open=".$open."&menss=ERROR");
	return;
}

/// actualiza REDES ///
// Borra los datos actuales
$sql="DELETE FROM `it_branch_social_network` WHERE	`it_branches_id` = '$Id'";
$micon->consulta($sql);
// Toma id de Branch Princial
$sql="SELECT `id` FROM `it_branches` WHERE `it_business_id`='$Id' AND is_main=1";
$micon->consulta($sql); $row=$micon->campoconsultaA();
$BranchID= $row[id];

// Inserta nuevos registos
$qtySocial = $_REQUEST[qtySocial];
$miOrden=1;
for ($i = 0; $i <= $qtySocial; $i++) {
	$Var	='MiSocialData'.$i;
    $VarData=$_REQUEST[$Var];
	$Var	='MiSocialKey'.$i;
    $VarKey=$_REQUEST[$Var];
	if($VarData!=""){
		$sql="INSERT INTO `it_branch_social_network` 
				(
				`it_branches_id`, 
				`it_social_networks_id`, 
				`account`, 
				`order`, 
				`created_at`
				)
				VALUES
				(
				'$BranchID', 
				'$VarKey', 
				'$VarData', 
				'$miOrden', 
				NOW() 
				)";
		//echo $sql.'<br>';		
		if (!$micon->consulta($sql)) {
			header("Location: ../CompanyView.php?open=".$open."&menss=ERROR");
			return;
		}
		
		$miOrden++;
	}	
}

$CompanyCountry		= $_REQUEST[CompanyCountry];
$CompanyState    	= $_REQUEST[CompanyState];
$CompanyCity    	= $_REQUEST[CompanyCity];
$CompanyLatitude    = $_REQUEST[CompanyLatitude];
$CompanyLongitude	= $_REQUEST[CompanyLongitude];
$CompanyPlaceID    	= $_REQUEST[CompanyPlaceID];

// complementa información de la BRANCH is_main 
$Phone=$CompanyPhone;
$Phone=str_replace('(','',$Phone);
$Phone=str_replace(')','',$Phone);

$CountryPhone = $_REQUEST['country_phone'];

$sql="UPDATE `it_branches` 
		SET
		`address` 			= '$CompanyAddress', 
		`city_from_map` 	= '$CompanyCity', 
		`state_from_map`	= '$CompanyState', 
		`country_from_map` 	= '$CompanyCountry', 
		`phone` 			= '$Phone', 
		`country_phone` 	= '$CountryPhone', 
		`languaje` 			= '$CompanyLanguage', 
		`latitude` 			= '$CompanyLatitude', 
		`longitude` 		= '$CompanyLongitude', 
		`placeid` 			= '$CompanyPlaceID', 
		`updated_at` =  now()
	WHERE
	`it_business_id` = '$Id' and `is_main`=1 ";


if (!$micon->consulta($sql)) {
	header("Location: ../CompanyView.php?open=".$open."&menss=ERROR");
	return;
}

header("Location: ../CompanyView.php?open=".$open."&menss=OK");
?>