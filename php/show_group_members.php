<?
session_start();
require('../include/mysql_class.php');
include('../include/seguridad.php');

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', '1');


$sql = "select
    it_users.id as it_users_id,
    it_users.name as it_users_name,
    it_users.last_name as it_users_last_name,
    it_users.avatar as it_users_avatar
    from it_groups_users
    LEFT JOIN it_user_group ON it_user_group.it_groups_users_id = it_groups_users.id
    LEFT JOIN it_users ON it_users.id = it_user_group.it_users_id
    where it_groups_users.id = " . $_GET['id'];

$micon->consulta($sql);
?>
<div>
    <div class="m-input-icon m-input-icon--left">
        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="js-search-group">
        <span class="m-input-icon__icon m-input-icon__icon--left">
            <span><i class="la la-search"></i></span>
        </span>
    </div>
    <div class="m-scrollable m-widget4" style="max-height: 57vh" data-mcs-theme="minimal-dark">
        <?
        while($member = $micon->campoconsultaA()) {
            if (empty($member['it_users_name'])) {
        ?>
            <div style="padding: 0.5em;">
                this group has no members
            </div>
        <?
            } else {
        ?>
            <div class="m-widget4__item member" data-title="<?= $member['it_users_name'] ?> <?= $member['it_users_last_name'] ?>">
                <div class="m-widget4__img m-widget4__img--logo">							 
                    <img
                        src="./srvs/media/images/profiles/<?= $member['it_users_avatar'] ?>"
                        style="object-fit: cover; width: 45px; height: 45px"
                        onerror="this.src = './srvs/media/images/profiles/nouser.svg'"
                    />
                </div>
                <div class="m-widget4__info">
                    <span class="m-widget4__title">
                        <?= $member['it_users_name'] ?> <?= $member['it_users_last_name'] ?>
                    </span><br> 
                    <span class="m-widget4__sub">
                    Group member
                    </span>		 
                </div>
                <span class="m-widget4__ext">
                    <a style="cursor: pointer" tabindex="0" class="btn btn-link text-danger" onclick="removeMember(<?= $member['it_users_id'] ?>)">
                        <i class="fal fa-trash"></i>
                        Remove
                    </a>
                </span>	
            </div>
        <?
            }
        }
        ?>
    </div>
</div>
<script>
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function( elem ) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });
    $("#js-search-group").on('change keyup', function(event) {

        $('.member:not(:contains("' + event.target.value + '"))').css({ 'display': 'none' });
        $('.member:contains("' + event.target.value + '")').css({ 'display': 'block' });
    })
</script>
