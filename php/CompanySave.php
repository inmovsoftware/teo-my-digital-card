<?
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', '1');
session_start();
require('../include/mysql_class.php');
require('../include/resizeImagen.php');

/* declaracion de variables */
$CompanyPhoto 			= $_REQUEST[CompanyPhoto];
$CompanyName 			= $_REQUEST[CompanyName];
$CompanyAlias	  		= $_REQUEST[CompanyAlias];
$CompanyInfo    		= $_REQUEST[CompanyInfo];
$CompanyLanguage   		= $_REQUEST[CompanyLanguage];
$CompanyAddress   		= $_REQUEST[CompanyAddress];
$CompanyPhone    		= $_REQUEST[CompanyPhone];
$CompanyBackgroundColor	= $_REQUEST[CompanyBackgroundColor];
$CompanyTextColor    	= $_REQUEST[CompanyTextColor];

$CompanyWebsite    	= $_REQUEST[CompanyWebsite];
$CompanyFacebook   	= $_REQUEST[CompanyFacebook];
$CompanyInstagram   = $_REQUEST[CompanyInstagram];
$CompanyLinked    	= $_REQUEST[CompanyLinked];
$CompanyYoutube    	= $_REQUEST[CompanyYoutube];
$CompanyTwitter    	= $_REQUEST[CompanyTwitter];
$CompanyGooglePlus	= $_REQUEST[CompanyGooglePlus];

$CompanyCountry		= $_REQUEST[CompanyCountry];
$CompanyState    	= $_REQUEST[CompanyState];
$CompanyCity    	= $_REQUEST[CompanyCity];
$CompanyLatitude    = $_REQUEST[CompanyLatitude];
$CompanyLongitude	= $_REQUEST[CompanyLongitude];
$CompanyPlaceID    	= $_REQUEST[CompanyPlaceID];

$Id            	= $_REQUEST[id];
$action        	= $_REQUEST[action];
$activate 	   	= $_REQUEST[activate];
$delete 	   	= $_REQUEST[delete];
$open 	   	   	= $_REQUEST[open];

if($activate=='true'){
	$sql = "UPDATE it_categories_news SET it_categories_news.status = 'A', deleted_at = ''
		WHERE id = '".$_GET[id]."'";
}else{	
	if($delete=='true'){
		$sql = "UPDATE it_categories_news SET it_categories_news.status = 'S', deleted_at = NOW()
		WHERE id = '".$_GET[id]."'";
	}else{
		if($action=='Insert'){
			
			$CompanyPhotoName = $_FILES['CompanyPhoto']['name'];
			echo 'Error='.$_FILES['CompanyPhoto']['error'].'<br>';
			echo $_FILES['CompanyPhoto']['name'].' - '.$CompanyPhotoName.'<br>';
			echo $CompanyName.' <==== <br>';

			if($_FILES['CompanyPhoto']['name'] != ""){ // El campo foto contiene una imagen...
        
				// Primero, hay que validar que se trata de un JPG/GIF/PNG
				$allowedExts = array("jpg", "jpeg", "gif", "png", "JPG", "GIF", "PNG");
				$extension = end(explode(".", $_FILES["CompanyPhoto"]["name"]));
				if ((($_FILES["CompanyPhoto"]["type"] == "image/gif")
					|| ($_FILES["CompanyPhoto"]["type"] == "image/jpeg")
					|| ($_FILES["CompanyPhoto"]["type"] == "image/png")
					|| ($_FILES["CompanyPhoto"]["type"] == "image/pjpeg"))
					&& in_array($extension, $allowedExts)) {
					// el archivo es un JPG/GIF/PNG, entonces... graba  Company
					
					$sql="INSERT INTO `it_business` 
							(
							`name`, 
							`alias`, 
							`logo`, 
							`backgroundcolor`, 
							`textcolor`, 
							`html`, 
							`created_at`
							)
							VALUES
							(
							'$CompanyName', 
							'$CompanyAlias', 
							'$CompanyPhotoName', 
							'$CompanyBackgroundColor', 
							'$CompanyTextColor', 
							'$CompanyInfo', 
							 now()
							)";
					echo $sql.'<br>';		
					$micon->consulta($sql);
					
					// toma el id generado para grabar la imagen
					$sql = "SELECT @@identity AS id";
					$micon->consulta($sql);
					$row = $micon->campoconsultaA();
					$id = trim($row[id]);
					
					$extension = end(explode('.', $_FILES['CompanyPhoto']['name']));
					$foto = $id."_logo_I.".$extension;
					$directorio = '../srvs/media/images/business'; // directorio de tu elección
					
					// almacenar imagen inicial en el servidor
					move_uploaded_file($_FILES['CompanyPhoto']['tmp_name'], $directorio.'/'.$foto);
			
					// crea versiones de la foto según tabla se quiera
					/* 
					$sql="SELECT  REPLACE(valor,' ','') valor FROM `cyc_parametros` WHERE id='20-02-01'";
					$micon->consulta($sql);
					$row=$micon->campoconsultaA();
					$tamano=explode("X",strtoupper($row[valor]));
					$tamano[0]=512;
					$tamano[1]=512;
					$FotoConversion=$id."_logo_F.".$extension;
					resizeImagen($directorio.'/', $foto, $tamano[0], $tamano[1],$FotoConversion,$extension); 
					*/
					
					// complementa información de la BRANCH is_main creado por Trigger
					$Phone=substr($CompanyPhone,5,strlen($CompanyPhone)-5);
					$Phone=str_replace('(','',$Phone);
					$Phone=str_replace(')','',$Phone);
					
					$CountryPhone=substr($CompanyPhone,1,3);
					$CountryPhone=ltrim($CountryPhone,'0');
					$CountryPhone='+'.$CountryPhone;
					
					$sql="UPDATE `it_branches` 
							SET
							`address` 			= '$CompanyAddress', 
							`city_from_map` 	= '$CompanyCity', 
							`state_from_map`	= '$CompanyState', 
							`country_from_map` 	= '$CompanyCountry', 
							`phone` 			= '$Phone', 
							`country_phone` 	= '$CountryPhone', 
							`languaje` 			= '$CompanyLanguage', 
							`latitude` 			= '$CompanyLatitude', 
							`longitude` 		= '$CompanyLongitude', 
							`placeid` 			= '$CompanyPlaceID', 
							`updated_at` =  now()
						
						WHERE
						`it_business_id` = '$id' and `is_main`=1 ";
					//echo $sql.'<br>';		
					$micon->consulta($sql);	
					
					/// Crea Redes Sociales ///
					
					// Toma id de Branch
					$sql="SELECT 	`id` FROM `it_branches` WHERE `it_business_id`='$id'";
					$micon->consulta($sql); $row=$micon->campoconsultaA();
					$BranchID= $row[id];
					
					// Inserta de HOMEPAGE si lo hay
					if($CompanyWebsite!=""){
						
						// Toma id de HOMEPAGE
						$sql="SELECT `id` FROM `it_social_networks` WHERE UPPER(NAME) LIKE 'HOMEPAGE'";
						$micon->consulta($sql); $row=$micon->campoconsultaA();
						$SocialID= $row[id];
					
						$sql="INSERT INTO `it_branch_social_network` 
								(
								`it_branches_id`, 
								`it_social_networks_id`, 
								`account`, 
								`created_at`
								)
								VALUES
								(
								'$BranchID', 
								'$SocialID', 
								'$CompanyWebsite', 
								now()
								)";
						$micon->consulta($sql);
					}
					//echo $sql."<br>";
					// Inserta de Facebook si lo hay
					if($CompanyFacebook!=""){
						
						// Toma id de FACEBOOK
						$sql="SELECT `id` FROM `it_social_networks` WHERE UPPER(NAME) LIKE 'FACEBOOK'";
						$micon->consulta($sql); $row=$micon->campoconsultaA();
						$SocialID= $row[id];
						
						$sql="INSERT INTO `it_branch_social_network` 
								(
								`it_branches_id`, 
								`it_social_networks_id`, 
								`account`, 
								`created_at`
								)
								VALUES
								(
								'$BranchID', 
								'$SocialID', 
								'$CompanyFacebook', 
								now()
								)";
						$micon->consulta($sql);
					}

					// Inserta de Instagram si lo hay
					if($CompanyInstagram!=""){
						
						// Toma id de Instagram
						$sql="SELECT `id` FROM `it_social_networks` WHERE UPPER(NAME) LIKE 'INSTAGRAM'";
						$micon->consulta($sql); $row=$micon->campoconsultaA();
						$SocialID= $row[id];
						
						$sql="INSERT INTO `it_branch_social_network` 
								(
								`it_branches_id`, 
								`it_social_networks_id`, 
								`account`, 
								`created_at`
								)
								VALUES
								(
								'$BranchID', 
								'$SocialID', 
								'$CompanyInstagram', 
								now()
								)";
						$micon->consulta($sql);
					}		

					// Inserta de Linked  si lo hay
					if($CompanyLinked!=""){
						
						// Toma id de Linked
						$sql="SELECT `id` FROM `it_social_networks` WHERE UPPER(NAME) LIKE 'LINKEDIN'";
						$micon->consulta($sql); $row=$micon->campoconsultaA();
						$SocialID= $row[id];
						
						$sql="INSERT INTO `it_branch_social_network` 
								(
								`it_branches_id`, 
								`it_social_networks_id`, 
								`account`, 
								`created_at`
								)
								VALUES
								(
								'$BranchID', 
								'$SocialID', 
								'$CompanyLinked', 
								now()
								)";
						$micon->consulta($sql);
					}
					
					// Inserta de YouTube  si lo hay
					if($CompanyYoutube!=""){
						
						// Toma id de Linked
						$sql="SELECT `id` FROM `it_social_networks` WHERE UPPER(NAME) LIKE '%YOUTUBE%'";
						$micon->consulta($sql); $row=$micon->campoconsultaA();
						$SocialID= $row[id];
						
						$sql="INSERT INTO `it_branch_social_network` 
								(
								`it_branches_id`, 
								`it_social_networks_id`, 
								`account`, 
								`created_at`
								)
								VALUES
								(
								'$BranchID', 
								'$SocialID', 
								'$CompanyYoutube', 
								now()
								)";
						$micon->consulta($sql);
					}
					
					// Inserta de Twitter  si lo hay
					if($CompanyTwitter!=""){
						
						// Toma id de Linked
						$sql="SELECT `id` FROM `it_social_networks` WHERE UPPER(NAME) LIKE '%TWITTER%'";
						$micon->consulta($sql); $row=$micon->campoconsultaA();
						$SocialID= $row[id];
						
						$sql="INSERT INTO `it_branch_social_network` 
								(
								`it_branches_id`, 
								`it_social_networks_id`, 
								`account`, 
								`created_at`
								)
								VALUES
								(
								'$BranchID', 
								'$SocialID', 
								'$CompanyTwitter', 
								now()
								)";
						$micon->consulta($sql);
					}
					
					// Inserta de Google Plus  si lo hay
					if($CompanyGooglePlus!=""){
						
						// Toma id de Linked
						$sql="SELECT `id` FROM `it_social_networks` WHERE UPPER(NAME) LIKE '%GOOGLE%'";
						$micon->consulta($sql); $row=$micon->campoconsultaA();
						$SocialID= $row[id];
						
						$sql="INSERT INTO `it_branch_social_network` 
								(
								`it_branches_id`, 
								`it_social_networks_id`, 
								`account`, 
								`created_at`
								)
								VALUES
								(
								'$BranchID', 
								'$SocialID', 
								'$CompanyGooglePlus', 
								now()
								)";
						$micon->consulta($sql);
					}
						
				}else{ // El archivo no es JPG/GIF/PNG
					$malformato = $_FILES["CamisetaFoto"]["type"];
					header("Location: ../CompanyView.php?error=noFormato&formato=$malformato&open=".$open);
					exit;
				}
			}else{ // El campo foto NO contiene una imagen
				header("Location: ../CompanyView.php?error=noImagen&open=".$open);
				exit;
			}
				
		}else{
			$sql = "UPDATE it_categories_news SET it_categories_news.name = '".$CategoryName."', color = '".$CategoryColor."' , updated_at = NOW()
			WHERE id = '".$Id."'";
		}
	}		
}
//echo $sql;
//$micon->consulta($sql);
header("Location: ../CompanyView.php?open=".$open);
?>