<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Places Search Box</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
       #map {
			 height: 300px;
			width:100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
    </style>
  </head>
  <body>
    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
    <div id="map"></div>
    <div id="info">ddd</div>
	<div class="form-group m-form__group row">
														<div class="col-lg-3 m-form__group-sub">
															<label>Company Address:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyAddress" id="CompanyAddress"  value="<?=$DatPartner['address']?>">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
														<div class="col-lg-3 m-form__group-sub">
															<label>Country:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyCountry" id="CompanyCountry"  readonly value="<?=$DatPartner['country_from_map']?>">
																<input type="Hidden" name="CompanyCountryID" id="CompanyCountryID" readonly>
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
														<div class="col-lg-3 m-form__group-sub">
															<label class="">State:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyState" id="CompanyState" readonly value="<?=$DatPartner['state_from_map']?>">
																<input type="Hidden" name="CompanyStateID" id="CompanyStateID" readonly>
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
														<div class="col-lg-3 m-form__group-sub">
															<label class="">City:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyCity" id="CompanyCity" readonly value="<?=$DatPartner['city_from_map']?>">
																<input type="Hidden" name="CompanyCityID" id="CompanyCityID" readonly>
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
													</div>
													<div class="form-group m-form__group row">
														<div class="col-lg-4">
															<label>Latitude:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyLatitude" id="CompanyLatitude"  readonly value="<?=$DatPartner['latitude']?>">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
														<div class="col-lg-4">
															<label class="">Longitude:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyLongitude" id="CompanyLongitude" readonly value="<?=$DatPartner['longitude']?>">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
														<div class="col-lg-4">
															<label class="">Place ID:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyPlaceID" id="CompanyPlaceID" readonly value="<?=$DatPartner['placeid']?>">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
  <script type="text/javascript"> 
	// This example adds a search box to a map, using the Google Place Autocomplete
	// feature. People can enter geographical searches. The search box will return a
	// pick list containing a mix of places and predicted search terms.

	// This example requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

	var MiMarker=0;
	var infowindow;
    var messagewindow;

	var miAdd="Cra. 48 #72-65, Barranquilla, Atlántico, Colombia";
	
	
	function initAutocomplete() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -33.8688, lng: 151.2195},
			zoom: 13,
			mapTypeId: 'roadmap'
		});

		
		// Create the search box and link it to the UI element.
		
		var input = document.getElementById('pac-input');
	
		var searchBox = new google.maps.places.SearchBox(input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});

		var markers = [];
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			var places = searchBox.getPlaces();

			if (places.length == 0) {
			return;
			}

			// Clear out the old markers.
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];
			
			
			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();
			places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};

			 	// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  draggable: true,
				  position: place.geometry.location
				})); 
				
				// micodigo
				
				var marker=markers[markers.length-1];
				
				var latlng = {
				
					lat: place.geometry.location.lat(),
					lng: place.geometry.location.lng()
				};
		  
				var geocoder = new google.maps.Geocoder;
				
				document.getElementById('CompanyLatitude').value = place.geometry.location.lat();
				document.getElementById('CompanyLongitude').value = place.geometry.location.lng();
				
                Migeocode(geocoder, map, latlng) ;

				google.maps.event.addListener(marker, 'dragend', function(event){
					
					latlng = marker.getPosition();
					//console.log(latlng.lat());
					document.getElementById('CompanyLatitude').value = latlng.lat();
					document.getElementById('CompanyLongitude').value = latlng.lng();
					
					Migeocode(geocoder, map, latlng) ;
				});
				
				// end micodigo

		
					
					if (place.geometry.viewport) {
					  // Only geocodes have viewport.
					  bounds.union(place.geometry.viewport);
					 
					} else {
					  bounds.extend(place.geometry.location);
					}
					
				});
				map.fitBounds(bounds);
			});

	
	}	

	function Migeocode(geocoder, map, latlng) {	
		geocoder.geocode({
			'location': latlng
			}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						map.setZoom(18);
						console.log(results);
						//console.log(latlng);
						var placeID = results[0].place_id;
						document.getElementById('CompanyPlaceID').value=placeID;

						var cant=results[0].address_components.length;

						for(var j=0; j<cant; j++) {
							miObj=results[0].address_components[j];
							
							Object.keys(miObj).forEach(function (key) {
								
								if(key=='long_name'){
									long_name=miObj[key];
								}
								if(key=='short_name'){
									short_name =miObj[key];
								}						
								if(key=='types'){
									Miarray=miObj[key];
									if(Miarray[0]=='country'){
										document.getElementById('CompanyCountry').value=long_name;
										document.getElementById('CompanyCountryID').value=short_name;
									}
									if(Miarray[0]=='administrative_area_level_1'){
										document.getElementById('CompanyState').value=long_name;
										document.getElementById('CompanyStateID').value=short_name;
									}
									if(Miarray[0]=='administrative_area_level_2'){
										document.getElementById('CompanyCity').value=long_name;
										document.getElementById('CompanyCityID').value=short_name;
									}
								}
							})
						}
					}
					document.getElementById('CompanyAddress').value=results[1].formatted_address;
					//document.getElementById('CompanyLatitude').value = latlng[0];
					//document.getElementById('CompanyLongitude').value = latlng[1];
				}
				
			}
		);	
	};

	
document.getElementById('CompanyAddress').value='';
		 
</script>
	
    <script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM-x4fGWDESn1d2wjRqh_qhrPCfXZtSlY&libraries=places,geometry&callback=initAutocomplete&language=EN">
    </script>
  </body>
</html>
