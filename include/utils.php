<?
function getMonth_ES($i){
	$arrayMonth = array( "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" );
	return $arrayMonth[$i-1];
}
function friendlyDate( $date, $lDay, $lMonth, $dispHour ) {
	if ( $lDay ) {
		$arrayDays = array( "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo" );
	} else {
		$arrayDays = array( "Lun.", "Mar.", "Mie.", "Jue.", "Vie.", "Sab.", "Dom." );
	}
	if ( $lMonth ) {
		$arrayMonth = array( "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" );
	} else {
		$arrayMonth = array( "Ene.", "Feb.", "Mar.", "Abr.", "May.", "Jun.", "Jul.", "Ago.", "Sep.", "Oct.", "Nov.", "Dic." );
	}
	$hourFormatted = "";
	if ( $dispHour ) {
		$hourFormatted = " | " . date( 'H:i', strtotime( $date ) );
	}

	$day = $arrayDays[ date( "N", strtotime( $date ) ) - 1 ];
	$month = $arrayMonth[ date( "n", strtotime( $date ) ) - 1 ];
	$friendlyFormat = $day . " " . date( 'd', strtotime( $date ) ) . " de " . $month . $hourFormatted;

	return $friendlyFormat;
}






// return the interpolated value between pBegin and pEnd
function interpolate( $pBegin, $pEnd, $pStep, $pMax ) {
	if ( $pBegin < $pEnd ) {
		return ( ( $pEnd - $pBegin ) * ( $pStep / $pMax ) ) + $pBegin;
	} else {
		return ( ( $pBegin - $pEnd ) * ( 1 - ( $pStep / $pMax ) ) ) + $pEnd;
	}
}

// generate gradient swathe now

////ee3524 - c41230
function colorScale( $theColorBegin, $theColorEnd, $theNumSteps ) {
	$ArrColor = array();
	$theColorBegin = hexdec( $theColorBegin );
	$theColorEnd = hexdec( $theColorEnd );
	$theNumSteps = $theNumSteps - 1;

	$theR0 = ( $theColorBegin & 0xff0000 ) >> 16;
	$theG0 = ( $theColorBegin & 0x00ff00 ) >> 8;
	$theB0 = ( $theColorBegin & 0x0000ff ) >> 0;

	$theR1 = ( $theColorEnd & 0xff0000 ) >> 16;
	$theG1 = ( $theColorEnd & 0x00ff00 ) >> 8;
	$theB1 = ( $theColorEnd & 0x0000ff ) >> 0;


	for ( $i = 0; $i <= $theNumSteps; $i++ ) {
		$theR = interpolate( $theR0, $theR1, $i, $theNumSteps );
		$theG = interpolate( $theG0, $theG1, $i, $theNumSteps );
		$theB = interpolate( $theB0, $theB1, $i, $theNumSteps );

		$theVal = ( ( ( $theR << 8 ) | $theG ) << 8 ) | $theB;
		$hexColor = sprintf( "%06X", $theVal );
		// echo $theVal  . "-" ;
		array_push( $ArrColor, "#" . $hexColor );
		//$ArrColor $theTDARTag . "-";

	}
	return $ArrColor;
}
//print_r(colorScale("ee3524","c41230",1))  

 function time_ago($timestamp)  {  
      $time_ago = strtotime($timestamp);  
      $current_time = time();  
      $time_difference = $current_time - $time_ago;  
      $seconds = $time_difference;  
      $minutes      = round($seconds / 60 );           // value 60 is seconds  
      $hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
      $days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
      $weeks          = round($seconds / 604800);          // 7*24*60*60;  
      $months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
      $years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
      if($seconds <= 60)  
      {  
     return "Justo ahora";  
   }  
      else if($minutes <=60)  
      {  
     if($minutes==1)  
           {  
       return "Hace un minuto";  
     }  
     else  
           {  
       return "Hace $minutes minutos";  
     }  
   }  
      else if($hours <=24)  
      {  
     if($hours==1)  
           {  
       return "Hace una hora";  
     }  
           else  
           {  
       return "Hace $hours horas";  
     }  
   }  
      else if($days <= 7)  
      {  
     if($days==1)  
           {  
       return "Ayer";  
     }  
           else  
           {  
       return "Hace $days días";  
     }  
   }  
      else if($weeks <= 4.3) //4.3 == 52/12  
      {  
     if($weeks==1)  
           {  
       return "Hace una semana";  
     }  
           else  
           {  
       return "Hace $weeks semanas";  
     }  
   }  
       else if($months <=12)  
      {  
     if($months==1)  
           {  
       return "Hace un mes";  
     }  
           else  
           {  
       return "Hace $months meses";  
     }  
   }  
      else  
      {  
     if($years==1)  
           {  
       return "Hace un año";  
     }  
           else  
           {  
       return "Hace $years años";  
     }  
   }  
 }


function diffBetweenTimes($inDate1, $inDate2){
	$date1 = new DateTime( $inDate1 );
	$date2 = new DateTime( $inDate2 );

	$diff = $date2->diff($date1);

	$dayLabel = "";
	$hasDays = $diff->format('%a');
	if ($hasDays > 1){
	  $dayLabel = " días ";
	}
	elseif($hasDays=="1"){
		$dayLabel = "día ";
	}
	$timeElapsed = ($dayLabel!="") ? $hasDays . " " . $dayLabel : "";
	$timeElapsed .= $diff->format('%H:%I:%S');
	
	return $timeElapsed;
}
function deadline($timestamp)  {  
      $deadLine = strtotime($timestamp);  
      $current_time = time();  
      $time_difference = $deadLine - $current_time;  
      $seconds = $time_difference;  
      $minutes      = round($seconds / 60 );           // value 60 is seconds  
      $hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
      $days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
      $weeks          = round($seconds / 604800);          // 7*24*60*60;  
      $months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
      $years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
       if($seconds <= 60)  
      {  
     return "en menos de un minuto";  
   }  
      else  if($minutes <=60)  
      {  
     if($minutes==1)  
           {  
       return "en un minuto";  
     }  
     else  
           {  
       return "en $minutes minutos";  
     }  
   }  
      else if($hours <=24)  
      {  
     if($hours==1)  
           {  
       return "en una hora";  
     }  
           else  
           {  
       return "en $hours horas";  
     }  
   }  
      else if($days <= 7)  
      {  
     if($days==1)  
           {  
       return "Mañana";  
     }  
           else  
           {  
       return "en $days días";  
     }  
   }  
      else if($weeks <= 4.3) //4.3 == 52/12  
      {  
     if($weeks==1)  
           {  
       return "en una semana";  
     }  
           else  
           {  
       return "en $weeks semanas";  
     }  
   }  
       else if($months <=12)  
      {  
     if($months==1)  
           {  
       return "en un mes";  
     }  
           else  
           {  
       return "en $months meses";  
     }  
   }  
      else  
      {  
     if($years==1)  
           {  
       return "en un año";  
     }  
           else  
           {  
       return "en $years años";  
     }  
   }
}

?>