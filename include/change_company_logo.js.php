<script>
    var logoSel = "<?= $logoSel ?>";
    var previewSel = "<?= $previewSel ?>";
    var hiden_sel = "<?= $hiden_sel ?>";
    var no_upload = "<?= $no_upload ?>" ? JSON.parse("<?= $no_upload ?>") : false;
    function dataURItoBlob(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            $('#bsModalCropper').remove();
            var tmppath = URL.createObjectURL(input.files[0]);
            var $el = $('<div></div>');
            $('form').append($el);
            var a = $el.bsModalCropper({
                id: 'bsModalCropper',
                title: 'Crop logo',
                src: tmppath,
                // Cropper.js options
                cropper: {
                    rotatable: true,
                    aspectRatio: 16 / 9,
                    ready() {
                        var t = this;
                        window.setAspect = function(aspect) {
                            t.cropper.setAspectRatio(aspect)
                        }
                    }
                },
                // On cropper
                onCropper: function (dataURL) {
                    $(logoSel).attr('src', dataURL);
                    $(input).val();
                    if (!no_upload) {
                        const fd = new FormData();
                        fd.append('CompanyPhoto', dataURItoBlob(dataURL), 'photo.jpeg');
                        fd.append('id', $('#id').val());
                        $.ajax({
                            url: './php/upload_company_img.php',
                            type: 'POST',
                            data: fd,
                            processData: false, // tell jQuery not to process the data
                            contentType: false, // tell jQuery not to set contentType
                            success: function(data) {
                                toastr.success('logo was updated', 'success');
                            }
                        });
                    } else {
                        if (hiden_sel) {
                            var el = document.querySelector(hiden_sel);
                            el.value = dataURL;
                            console.log(el);
                            //$(hiden_sel).val(dataURItoBlob(dataURL));
                        }
                    }
                }
            });

            $el.click();

            $('#bsModalCropper .modal-body').prepend(
                $(
                    '<div style="margin-bottom: 15px">' +
                    '<button class="btn btn-primary mr-2" type="button" onclick="setAspect(1)"><i class="fal fa-square"></i> Square</button>' +
                    '<button class="btn btn-danger" type="button" onclick="setAspect(16 / 9)"><i class="fal fa-rectangle-wide"></i> Rectangle</button>' +
                    '</div>'
                )
            )
        }
    }

    $(logoSel).click(function() {
        $(previewSel).click();
    });

    $(previewSel).change(function() {
        console.log('PASA', this)
        readURL(this);
    });
</script>