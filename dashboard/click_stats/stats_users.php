<div class="row">
    <div class="col-xl-12">
        <m-contact-list _nghost-c14="">
            <div  class="m-portlet m-portlet--mobile">
                <div  class="m-portlet__head">
                    <div  class="m-portlet__head-caption">
                            <div  class="m-portlet__head-title">
                                <h3  class="m-portlet__head-text" translate="DASHBOARD.CONTACT_LIST">Stats By User</h3>
                            </div>
                    </div>
                </div>
            <div  class="m-portlet__body">
            <div  class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer" id="m_table_1_wrapper">
            <div  class="row">
            <div  class="col-sm-12"> 
            <?php
                    $sql__="SELECT COUNT(st.id) AS cant,  concat('srvs/media/images/profiles/', u.`avatar`) as image, p.name as position, u.id as id, u.name AS name, u.`last_name` AS lastname, br.name as branches, bu.name bussiness
                    FROM `it_click_stats` AS st
                    INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
                    INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
                    INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
                    INNER JOIN `it_positions` AS p ON u.`it_positions_id` = p.`id`
                    WHERE bu.`is_partner` = '". 
                        filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
                        "'  $sql_filter
                    GROUP BY u.id
                    ORDER BY cant DESC"; 
                        
                    $micon->consulta($sql__);
                    $labels = '';
                    $colors = "";
                    $data = "";
                    $data_link = array();
                    $data_mail = array();
                    $data_tel = array();
                    $data_sk = array();
                    $data_aa = array();
                    $data_vcard = array();
                    $data_store = array();

                    while($graph=$micon->campoconsultaA()){
                        $data_[$graph["id"]]["link"]= get_cant_clicks($graph["id"], 'link', $sql_filter );
                        $data_[$graph["id"]]["mail"]= get_cant_clicks($graph["id"], 'mailto',$sql_filter );
                        $data_[$graph["id"]]["tel"]= get_cant_clicks($graph["id"], 'tel', $sql_filter );
                        $data_[$graph["id"]]["sk"]= get_cant_clicks($graph["id"], 'sk', $sql_filter );
                        $data_[$graph["id"]]["aa"]= get_cant_clicks($graph["id"], 'aa', $sql_filter );
                        $data_[$graph["id"]]["vcard"]= get_cant_clicks($graph["id"], 'vcard', $sql_filter );
                        $data_[$graph["id"]]["store"]= get_cant_clicks($graph["id"], 'store', $sql_filter );
                        $data_[$graph["id"]]["data"]= $graph;
                    }
                   
            ?>
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_20"  >
            <thead >
            <tr>
                <th style="text-align: left"  rowspan="2">Name</th>
                <th style=" text-align: left"   rowspan="2">Position</th>
                <th style=" text-align: center"   rowspan="2">Company</th>
                <th style=" text-align: center"   rowspan="2">Branch</th>
                <th style=" text-align: center"   colspan="7">Stats</th>
                <th style=" text-align: center" aria-sort="ascending" class="sorting_asc"  rowspan="2">Total Stats</th>
            </tr>
            <tr>
                <th style="text-align: center; font-weight:bold;color:#01cece" >Link</th>
                <th style=" text-align: center; font-weight:bold;color:#8f8ffc" >Email</th>
                <th style=" text-align: center; font-weight:bold;color:#f8a2f8" >Telephone</th>
                <th style=" text-align: center; font-weight:bold;color:#f8a5a5 " >Sales Kit</th>
                <th style=" text-align: center; font-weight:bold;color: #cdcd00" >Agent Agreement</th>
                <th style=" text-align: center; font-weight:bold;color:#75ee75" >VCard</th>
                <th style=" text-align: center; font-weight:bold;color:#a2a1a1" >Store  </th>

                
            </tr>
            </thead>
                <tbody >
                <?php 
                 foreach($data_ AS $item){
                                ?>
                <tr>
                    <td>
                        <div  class="m-card-user m-card-user--sm">
                        <div  class="m-card-user__pic"><!---->
                        <img  alt="photo" class="m--img-rounded m--marginless ng-star-inserted" src="<?php echo $item["data"]["image"]?>"><!----></div>
                        <div  class="m-card-user__details"><span  class="m-card-user__name"><?php echo  $item["data"]["name"].' '. $item["data"]["lastname"]?></span></div></div>
                    </td>   
                    <td> <?php echo $item["data"]["position"]?> </td>
                    <td> <?php echo $item["data"]["bussiness"]?> </td>
                    <td> <?php echo $item["data"]["branches"]?> </td>
                    <td style="text-align: right; font-weight:bold;color:#01cece"> <?php echo  round(($item["link"]*100)/$item["data"]["cant"] )?>%</td>
                    <td style="text-align: right; font-weight:bold;color:#8f8ffc"> <?php echo  round(($item["mail"]*100)/$item["data"]["cant"])?>%</td>
                    <td style="text-align: right; font-weight:bold;color:#f8a2f8"> <?php echo  round(($item["tel"]*100)/$item["data"]["cant"])?>%</td>
                    <td style="text-align: right; font-weight:bold;color:#f8a5a5 "> <?php echo  round(($item["sk"]*100)/$item["data"]["cant"])?>%</td>
                    <td style="text-align: right; font-weight:bold;color: #cdcd00"> <?php echo  round(($item["aa"]*100)/$item["data"]["cant"])?>%</td>
                    <td style="text-align: right; font-weight:bold;color:#75ee75"> <?php echo  round(($item["vcard"]*100)/$item["data"]["cant"])?>%</td>
                    <td style="text-align: right; font-weight:bold;color:#a2a1a1"> <?php echo  round(($item["store"]*100)/$item["data"]["cant"])?>%</td>
                    <td style="text-align:right"> <?php echo $item["data"]["cant"]?> </td>

               </tr>
                <?php 
                        }?>
                </tbody>
            </table>

            				
            
            </div>
            </div>
            </div>
            </div>
            </div>
        </m-contact-list>
    </div>
</div>

