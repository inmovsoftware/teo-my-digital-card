<?php 
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
?>
<div class="m-portlet">
						
							<div class="m-portlet__body  m-portlet__body--no-padding">

							

								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-xl-8">
									<m-bar-chart _nghost-c12="">

										<div _ngcontent-c12="" class="m-widget14">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
														
													</div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0">
														
													</div>
												</div>
											</div>
											<div _ngcontent-c12="" class="m-widget14__header">
												<h3 _ngcontent-c12="" class="m-widget14__title" translate="DASHBOARD.CARDS">Click Stats</h3>
												<span _ngcontent-c12="" class="m-widget14__desc" translate="DASHBOARD.LABELS.CARDS">Top 10 Users</span>
											</div>

							<?php
						if(!function_exists('random_color_part')){
							function random_color_part() {
								return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
							}
						}

						if(!function_exists('random_color')){
						function random_color() {
							return random_color_part() . random_color_part() . random_color_part();
							}
						}

						

							$sql__="SELECT COUNT(st.id) AS cant, u.id as id, u.name AS name, u.`last_name` AS lastname
							FROM `it_click_stats` AS st
							INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
							INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
							INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
							WHERE bu.`is_partner` = '". 
								filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
								"'  $sql_filter
							GROUP BY u.id
							ORDER BY cant DESC
							LIMIT 10"; 
								
							$micon->consulta($sql__);
							$labels = '';
							$colors = "";
							$data = "";
							$data_link = array();
							$data_mail = array();
							$data_tel = array();
							$data_sk = array();
							$data_aa = array();
							$data_vcard = array();
							$data_store = array();
							while($graph=$micon->campoconsultaA()){
								$labels .= "'".$graph["name"].' '.$graph["lastname"]."',";
								$colors .= "'#8CB500',";
								array_push($data_link, get_cant_clicks($graph["id"], 'link', $sql_filter ));
								array_push($data_mail, get_cant_clicks($graph["id"], 'mailto',$sql_filter ));
								array_push($data_tel, get_cant_clicks($graph["id"], 'tel', $sql_filter ));
								array_push($data_sk, get_cant_clicks($graph["id"], 'sk', $sql_filter ));
								array_push($data_aa, get_cant_clicks($graph["id"], 'aa', $sql_filter ));
								array_push($data_vcard, get_cant_clicks($graph["id"], 'vcard', $sql_filter ));
								array_push($data_store, get_cant_clicks($graph["id"], 'store', $sql_filter ));
							}

							rtrim($labels,',');
							rtrim($colors,',');
							rtrim($data,',');
							
						
							?>
								<canvas id="myChart" styles="width:100%;"></canvas>
											<script>
											$(document).ready(function(){

												var ctx = document.getElementById("myChart").getContext('2d');
											var myChart = new Chart(ctx, {
												type: 'bar',
												data: {
													labels: [<?php echo $labels ?>],
													datasets: [

														{
															label: 'Link',
															backgroundColor: '#ccffff',
															data: ['<?php 
																	echo implode("', '", $data_link);
																	?>'																
															]
														},
														{
															label: 'Email',
															backgroundColor: '#ccccff',
															data: ['<?php 
																	echo implode("', '", $data_mail);
																	?>'																
															]
														},
														{
															label: 'Telephone',
															backgroundColor: '#ffccff',
															data: ['<?php 
																	echo implode("', '", $data_tel);
																	?>'																
															]
														},
														{
															label: 'Sales Kit',
															backgroundColor: '#ffcccc',
															data: ['<?php 
																	echo implode("', '", $data_sk);
																	?>'																
															]
														},
														{
															label: 'Agent Agreement',
															backgroundColor: '#ffffcc',
															data: ['<?php 
																	echo implode("', '", $data_aa);
																	?>'																
															]
														},
														{
															label: 'VCard',
															backgroundColor: '#ccffcc',
															data: ['<?php 
																	echo implode("', '", $data_vcard);
																	?>'																
															]
														},
														{
															label: 'Store',
															backgroundColor: '#cccccc',
															data: ['<?php 
																	echo implode("', '", $data_store);
																	?>'																
															]
														}


													]
													
												},
												options: {
												tooltips: {
													mode: 'index',
													intersect: false
												},
												responsive: true,
												scales: {
													xAxes: [{
														stacked: true,
													}],
													yAxes: [{
														stacked: true
													}]
												}
											}
											});

											});
										
											</script>

										</div>
										</m-bar-chart>
									</div>




									<div class="col-xl-4">
											
										<m-stat _nghost-c13="">
											<div _ngcontent-c13="" class="m-widget1">
												<div _ngcontent-c13="" class="m-widget1__item">
													<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
														<div _ngcontent-c13="" class="col">
															<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.USERS">Link</h3>
															<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.USERS">Links clicked</span>
														</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span style="color: #59fefe !important" _ngcontent-c13="" class="m-widget1__number m--font-brand">
																<?php
											
																	$sql__="SELECT COUNT(st.id) AS cant, st.destination_type AS type
																	FROM `it_click_stats` AS st
																	INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
																	INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
																	INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
																	WHERE st.destination_type ='link' AND  bu.`is_partner` = '". 
																		filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
																		"'  $sql_filter
																	GROUP BY type
																	"; 
																	$micon->consulta($sql__);
																	$graph = $micon->campoconsultaA();
																	
																	echo (!empty($graph["cant"]))?$graph["cant"]:'0';?>
																</span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Email</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.NEWS">Mails Send</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span  style="color: #8f8ffc !important" _ngcontent-c13="" class="m-widget1__number m--font-brand">
																<?php
											
																	$sql__="SELECT COUNT(st.id) AS cant, st.destination_type AS type
																	FROM `it_click_stats` AS st
																	INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
																	INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
																	INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
																	WHERE st.destination_type ='mailto' AND  bu.`is_partner` = '". 
																		filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
																		"'  $sql_filter
																	GROUP BY type
																	"; 
																	$micon->consulta($sql__);
																	$graph = $micon->campoconsultaA();
																	
																	echo (!empty($graph["cant"]))?$graph["cant"]:'0';?>
																</span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Telephone</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.LIKE">Telephone Clicked</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span  style="color: #f8a2f8 !important" _ngcontent-c13="" class="m-widget1__number m--font-danger"> 
																<?php
											
																	$sql__="SELECT COUNT(st.id) AS cant, st.destination_type AS type
																	FROM `it_click_stats` AS st
																	INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
																	INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
																	INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
																	WHERE st.destination_type ='tel' AND  bu.`is_partner` = '". 
																		filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
																		"'  $sql_filter
																	GROUP BY type
																	"; 
																	$micon->consulta($sql__);
																	$graph = $micon->campoconsultaA();
																	
																	echo (!empty($graph["cant"]))?$graph["cant"]:'0';?>
																
																</span>
															</div>
														</div>
													</div>

													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Sales Kit</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.COMMENTS">Sales Kit clicked</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span  style="color: #f8a5a5 !important" _ngcontent-c13="" class="m-widget1__number m--font-success">
																<?php
											
																	$sql__="SELECT COUNT(st.id) AS cant, st.destination_type AS type
																	FROM `it_click_stats` AS st
																	INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
																	INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
																	INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
																	WHERE st.destination_type ='sk' AND  bu.`is_partner` = '". 
																		filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
																		"'  $sql_filter
																	GROUP BY type
																	"; 
																	$micon->consulta($sql__);
																	$graph = $micon->campoconsultaA();
																	
																	echo $graph["cant"] ?>
																
																</span>
															</div>
														</div>
													</div>




													
													
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Agent Agreement</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.COMMENTS">Agent Agreement clicked</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span  style="color: #cdcd00 !important"  _ngcontent-c13="" class="m-widget1__number m--font-success">
																<?php
											
																	$sql__="SELECT COUNT(st.id) AS cant, st.destination_type AS type
																	FROM `it_click_stats` AS st
																	INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
																	INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
																	INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
																	WHERE st.destination_type ='aa' AND  bu.`is_partner` = '". 
																		filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
																		"'  $sql_filter
																	GROUP BY type
																	"; 
																	$micon->consulta($sql__);
																	$graph = $micon->campoconsultaA();
																	
																	echo (!empty($graph["cant"]))?$graph["cant"]:'0';?>
																
																</span>
															</div>
														</div>
													</div>

													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">VCard</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.COMMENTS">VCard downloaded</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span   style="color: #75ee75 !important" _ngcontent-c13="" class="m-widget1__number m--font-brand"> 
																<?php
											
																	$sql__="SELECT COUNT(st.id) AS cant, st.destination_type AS type
																	FROM `it_click_stats` AS st
																	INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
																	INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
																	INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
																	WHERE st.destination_type ='vcard' AND  bu.`is_partner` = '". 
																		filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
																		"'  $sql_filter
																	GROUP BY type
																	"; 
																	$micon->consulta($sql__);
																	$graph = $micon->campoconsultaA();
																	
																	echo (!empty($graph["cant"]))?$graph["cant"]:'0'; ?>
																
																</span>
															</div>
														</div>
													</div>

													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Store</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.COMMENTS">Store clicked</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span  style="color: #a2a1a1 !important" _ngcontent-c13="" class="m-widget1__number m--font-danger">
																<?php
											
																	$sql__="SELECT COUNT(st.id) AS cant, st.destination_type AS type
																	FROM `it_click_stats` AS st
																	INNER JOIN `it_users` AS u  ON u.id = st.`it_user_id`
																	INNER JOIN `it_branches` AS br ON br.`id` = u.`it_branches_id`
																	INNER JOIN `it_business` AS bu ON bu.`id` = br.`it_business_id`
																	WHERE st.destination_type ='store' AND  bu.`is_partner` = '". 
																		filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
																		"'  $sql_filter
																	GROUP BY type
																	"; 
																	$micon->consulta($sql__);
																	$graph = $micon->campoconsultaA();
																	
																	echo (!empty($graph["cant"]))?$graph["cant"]:'0';?>
																
																</span>
															</div>
														</div>
													</div>

													
												</div>
											</m-stat>
									
									
											</div>

								</div>

							</div>
						
						</div>