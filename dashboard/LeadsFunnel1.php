<div class="m-portlet">
						
							<div class="m-portlet__body  m-portlet__body--no-padding">

							

								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-xl-8">
									<m-bar-chart _nghost-c12="">

										<div _ngcontent-c12="" class="m-widget14">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
														
													</div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0">
														
													</div>
												</div>
											</div>
											<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">
													
													<div class="col-lg-12">
													<div class="m-card-user m-card-user--sm">
					                                    <div class="m-card-user__pic">
					                                        <img src="http://teo.mydigitalcard.us/srvs/media/images/profiles/<?=$DtaUser[avatar];?>" class="m--img-rounded m--marginless" alt="photo" >
					                                    </div>
					                                    <div class="m-card-user__details">
					                                        <span class="m-card-user__name  m--font-info"><b><?=$DtaUser[name].' '.$DtaUser[last_name];?></b></span>
					                                       <span class="m-card-user__email"><?=$DtaUser[positions];?></span>
					                                    </div>
								                    </div>
								                </div>
												</div>
											</div>
										</div>
											<div _ngcontent-c12="" class="m-widget14__header">
												 

												<h3 _ngcontent-c12="" class="m-widget14__title" translate="DASHBOARD.CARDS">Leads</h3>
												<span _ngcontent-c12="" class="m-widget14__desc" translate="DASHBOARD.LABELS.CARDS">Funnel leads</span>
											</div>


									<div id="embudo" style="height:300px; width: 60%"></div>


										</div>
										</m-bar-chart>
									</div>
									<div class="col-xl-4">
										<m-stat _nghost-c13="">
											<div _ngcontent-c13="" class="m-widget1">
												<div _ngcontent-c13="" class="m-widget1__item">
													<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
														<div _ngcontent-c13="" class="col">
															<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.USERS">Leads</h3>
															<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.USERS">Number Leads</span>
														</div>
															<!---->
										<?php

											$sql__="SELECT SUM(cant) AS cant, description FROM vs_leaddash where  it_business_id = '".$_SESSION['companyID']."'";
											if($_GET[id]!=''){
												$sql__ .=" and  vs_leaddash.id_user = $_GET[id]";
											} 
											$sql__ .= " group by description";
											//echo $sql__;
											$micon->consulta($sql__);
												while($graph = $micon->campoconsultaA()){
													$labels .= "'".$graph["description"]."',";
													$colors .= "'#8CB500',";
													switch ($graph[description]) {
				    									case "ltotal":
				    										$ltotal = $graph[cant];
				    									break;
				    									case "lcontact":
				    										$lcontact = $graph[cant];
				    									break;
				    									case "lprocess":
				    										$lprocess = $graph[cant];
				    									break;
				    									case "partner":	
				    										$partner = $graph[cant];
				    									break;
				    								}	
												}
											?>
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-brand"><?php echo $ltotal; ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Contact Leads</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.NEWS">Contact total</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-brand"><?php echo $lcontact; ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Leads in process</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.LIKE">Process total</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-danger"><?php echo $lprocess; ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Partner</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.COMMENTS">Partner total</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-success"><?php echo $partner; ?></span>
															</div>
														</div>
													</div>
												</div>
											</m-stat>
									
									
											</div>

								</div>

							</div>
						
						</div>