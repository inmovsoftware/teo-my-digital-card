
<div class="m-portlet">
<div class="m-portlet__body m-portlet__body--no-padding">
<div class="row m-row--no-padding m-row--col-separator-xl">
<div class="col-xl-12">
<m-map-chart _nghost-c15="">
<div _ngcontent-c15="" class="m-widget14">
    <div _ngcontent-c15="" class="m-widget14__header">
    <h3 _ngcontent-c15="" class="m-widget14__title" translate="DASHBOARD.MAP">Map</h3>
    <span _ngcontent-c15="" class="m-widget14__desc" translate="DASHBOARD.LABELS.MAP">Cards delivered on maps</span>
    </div>
        <agm-map _ngcontent-c15="" _nghost-c18="" class="sebm-google-map-container">
       
        <div id="map" style="width: 100%; height: 400px;" >
        
        </div>

<script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM-x4fGWDESn1d2wjRqh_qhrPCfXZtSlY&language=en&callback=initMap">
    </script>

    <?php

$sql3__="SELECT CONCAT(u.`name`,' ', u.`last_name` ) AS username,
 DATE_FORMAT(c.`registre_time`, '%Y-%m-%d') AS datecard,  c.`latitude` AS lat, c.`longitude` AS lon
FROM `it_cards` AS c
INNER JOIN `it_users` AS u ON c.`it_users_id` = u.`id`
INNER JOIN `it_branches` AS b ON u.`it_branches_id` = b.`id`
INNER JOIN `it_business` AS bu ON b.`it_business_id` = bu.`id`
INNER JOIN `it_positions` AS p ON u.`it_positions_id` = p.`id`
WHERE  `registre_time` BETWEEN '$_POST[fec_desde]' AND '$_POST[fec_hasta]'  and bu.`is_partner` = '". 
filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
"' $sql_filter
"; 
    
//echo $sql3__;
	
$micon->consulta($sql3__);

$data = '';
$count = 1;
while($graph= $micon->campoconsultaA()){
    $data .= "['".$graph["username"]." - ".$graph["datecard"]."', ".$graph["lat"].", ".$graph["lon"].", ".$count."],";
    $count++;
        }

     rtrim($data,',');
?>



        <script>
     

         function initMap() {
   
        var locations = [
            <?=$data ?>
            ];

            window.map = new google.maps.Map(document.getElementById('map'), {
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            var infowindow = new google.maps.InfoWindow();

            var bounds = new google.maps.LatLngBounds();

            var marker, i;

            for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
                });

                bounds.extend(marker.position);

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
            
           
            map.fitBounds(bounds);

            var listener = google.maps.event.addListener(map, "idle", function () {
                map.setZoom(10);
                google.maps.event.removeListener(listener);
            });
                    
        }
  
        
      </script>  



        </agm-map>
</div>
</m-map-chart>
</div>
</div>
</div>
</div>
