<div class="m-portlet">
						
							<div class="m-portlet__body  m-portlet__body--no-padding">

							

								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-xl-8">
									<m-bar-chart _nghost-c12="">

										<div _ngcontent-c12="" class="m-widget14">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
														
													</div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0">
														
													</div>
												</div>
											</div>
											<div _ngcontent-c12="" class="m-widget14__header">
												<h3 _ngcontent-c12="" class="m-widget14__title" translate="DASHBOARD.CARDS">Cards</h3>
												<span _ngcontent-c12="" class="m-widget14__desc" translate="DASHBOARD.LABELS.CARDS">Card send by user</span>
											</div>
											<div  class="m-widget14__header">
											   
											</div>

							<?php
						if(!function_exists('random_color_part')){
							function random_color_part() {
								return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
							}
						}

						if(!function_exists('random_color')){
						function random_color() {
							return random_color_part() . random_color_part() . random_color_part();
							}
						}

							$sql__="SELECT COUNT(c.id) AS cant, u.`name` AS name, u.`last_name` AS lastname FROM `it_users` AS u
								 INNER JOIN `it_branches` AS b ON u.`it_branches_id` = b.`id`
								INNER JOIN `it_business` AS bu ON b.`it_business_id` = bu.`id`
								LEFT JOIN `it_cards` AS c ON c.`it_users_id` = u.`id`
								WHERE bu.`is_partner` = '". 
								filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
								"'  $sql_filter
								GROUP BY name, lastname
								ORDER BY cant DESC
								LIMIT 10"; 
								
							$micon->consulta($sql__);
							$labels = '';
							$colors = "";
							$data = "";
							while($graph=$micon->campoconsultaA()){
								$labels .= "'".$graph["name"].' '.$graph["lastname"]."',";
								$colors .= "'#8CB500',";
								$data .= $graph["cant"].",";
							}
							rtrim($labels,',');
							rtrim($colors,',');
							rtrim($data,',');
							?>
									<canvas id="myChart" styles="width:100%;"></canvas>
											<script>
											$(document).ready(function(){

												var ctx = document.getElementById("myChart").getContext('2d');
											var myChart = new Chart(ctx, {
												type: 'bar',
												data: {
													labels: [<?php echo $labels ?>],
													datasets: [{
														label: '# of cards sent',
														data: [<?php echo $data ?>],
														backgroundColor: [
															<?php echo $colors ?>
														],
														borderColor: [
															<?php echo $colors ?>
														],
														borderWidth: 1
													}]
												},
												options: {
													scales: {
														yAxes: [{
															ticks: {
																beginAtZero:true
															}
														}]
													}
												}
											});

											});
										
											</script>

										</div>
										</m-bar-chart>
									</div>




									<div class="col-xl-4">
											<?php
											$sql__="SELECT b.* FROM `v_business_conformation` AS b
													INNER JOIN `it_business` AS bu ON bu.`id` = b.id
													WHERE bu.`is_partner` = '". 
													filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
													"' $sql_filter"; 
											$micon->consulta($sql__);
											$graph = $micon->campoconsultaA();
											?>	
										<m-stat _nghost-c13="">
											<div _ngcontent-c13="" class="m-widget1">
												<div _ngcontent-c13="" class="m-widget1__item">
													<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
														<div _ngcontent-c13="" class="col">
															<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.USERS">Users</h3>
															<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.USERS">With card</span>
														</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-brand"><?php echo $graph["users_total"] ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">News</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.NEWS">News total</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-brand"><?php echo $graph["news_total"] ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Post</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.LIKE">Post total</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-danger"><?php echo $graph["posts_total"] ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Branch</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.COMMENTS">Branch total</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-success"><?php echo $graph["branch_total"] ?></span>
															</div>
														</div>
													</div>
												</div>
											</m-stat>
									
									
											</div>

								</div>

							</div>
						
						</div>