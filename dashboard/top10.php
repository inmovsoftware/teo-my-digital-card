<div class="row">
    <div class="col-xl-12">
        <m-contact-list _nghost-c14="">
            <div  class="m-portlet m-portlet--mobile">
                <div  class="m-portlet__head">
                    <div  class="m-portlet__head-caption">
                            <div  class="m-portlet__head-title">
                                <h3  class="m-portlet__head-text" translate="DASHBOARD.CONTACT_LIST">Top 10 Contact List</h3>
                            </div>
                    </div>
                </div>
            <div  class="m-portlet__body">
            <div  class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer" id="m_table_1_wrapper">
            <div  class="row">
            <div  class="col-sm-12">
            
            <?php

                    $sql2__="SELECT COUNT(c.id) AS cant, concat('srvs/media/images/profiles/', u.`avatar`) as image, p.name as position,   u.`name` as name, u.`last_name` as lastname FROM `it_users` AS u
                    INNER JOIN `it_branches` AS b ON u.`it_branches_id` = b.`id`
                   INNER JOIN `it_business` AS bu ON b.`it_business_id` = bu.`id`
                   LEFT JOIN `it_cards` AS c ON c.`it_users_id` = u.`id`
                   INNER JOIN `it_positions` AS p ON u.`it_positions_id` = p.`id`
                   WHERE  `registre_time` BETWEEN '$_POST[fec_desde]' AND '$_POST[fec_hasta]'  and bu.`is_partner` = '". 
				   filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
			       "' $sql_filter
                   GROUP BY u.`name`, u.`last_name`
                   ORDER BY cant DESC limit 10"; 
                        
                   
                    ?>


            
            
            <table  class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="m_table_1" role="grid" style="width: 100%; margin-top: 0px !important">
            
            <thead >
            
            <tr  role="row">
                <th  aria-label="Agent: activate to sort column descending" aria-sort="ascending" colspan="1" rowspan="1" style="text-align: left" translate="CONTACTS.USER">User</th>
                <th  aria-label="CompanyEmail: activate to sort column ascending" colspan="1" rowspan="1" style=" text-align: left" translate="CONTACTS.POSITION">Position</th>
                <th  aria-label="CompanyAgent: activate to sort column ascending" colspan="1" rowspan="1" style=" text-align: center" translate="CONTACTS.QUANTITY">Quantity</th>
                <?php /* <th  aria-label="Actions" class="sorting_disabled" colspan="1" rowspan="1" style=" text-align: center" translate="GENERAL.ACTIONS">Actions</th> */?>
            </tr>
            </thead>
                
                <tbody >
                <?php 
                 $micon->consulta($sql2__);
                 while($graph=$micon->campoconsultaA()){
                                ?>
                <tr  class="odd ng-star-inserted" role="row" id="4">
                    <td  class="sorting_1" tabindex="0">
                        <div  class="m-card-user m-card-user--sm">
                        <div  class="m-card-user__pic"><!---->
                        <img  alt="photo" class="m--img-rounded m--marginless ng-star-inserted" src="<?php echo $graph["image"]?>"><!----></div>
                        <div  class="m-card-user__details"><span  class="m-card-user__name"><?php echo $graph["name"].' '.$graph["lastname"]?></span></div></div>
                    </td>
                    <td  class="ng-star-inserted"> <?php echo $graph["position"]?> </td>
                    <td  style="text-align: center"><?php echo $graph["cant"] ?></td>
                    <?php /* <td  style="text-align: center"><a  href="/contacts-details/4"><i  class="far fa-eye action"></i></a></td> */?>
                </tr>
                 <?php }?>
                </tbody>
            </table>
            
            </div>
            </div>
            </div>
            </div>
            </div>
        </m-contact-list>
    </div>
</div>