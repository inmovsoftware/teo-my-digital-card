<div class="m-portlet">
						
							<div class="m-portlet__body  m-portlet__body--no-padding">

							

								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-xl-8">
									<m-bar-chart _nghost-c12="">

										<div _ngcontent-c12="" class="m-widget14">
											<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
												<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
														
													</div>
												</div>
												<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
													<div style="position:absolute;width:200%;height:200%;left:0; top:0">
														
													</div>
												</div>
											</div>
											<div _ngcontent-c12="" class="m-widget14__header">
												<h3 _ngcontent-c12="" class="m-widget14__title" translate="DASHBOARD.CARDS">News</h3>
												<span _ngcontent-c12="" class="m-widget14__desc" translate="DASHBOARD.LABELS.CARDS">For month</span>
											</div>


									<?php

							$sql__=" SELECT COUNT(*) AS cant, SUM(IF(it_news.status='A',1,0)) AS activite,
									CASE WHEN MONTH(publication_date) = 1 THEN 'January'
									WHEN MONTH(publication_date) = 2 THEN 'February'
									WHEN MONTH(publication_date) = 3 THEN 'March'
									WHEN MONTH(publication_date) = 4 THEN 'April'
									WHEN MONTH(publication_date) = 5 THEN 'May'
									WHEN MONTH(publication_date) = 6 THEN 'June'
									WHEN MONTH(publication_date) = 7 THEN 'July'
									WHEN MONTH(publication_date) = 8 THEN 'August'
									WHEN MONTH(publication_date) = 9 THEN 'September'
									WHEN MONTH(publication_date) = 10 THEN 'October'
									WHEN MONTH(publication_date) = 11 THEN 'November'
									WHEN MONTH(publication_date) = 12 THEN 'December'  END AS mes
									FROM it_news 
									WHERE id_business_id ='".$_SESSION['companyID']."' AND YEAR(publication_date) = YEAR(NOW())
									GROUP BY MONTH(publication_date) "; 
								//echo $sql__;
							$micon->consulta($sql__);
							$labels = '';
							$colors = "";
							$data = "";
						while($graph=$micon->campoconsultaA()){
								$labels .= "'".$graph["mes"]."',";
								$colors .= "'#8CB500',";
								$data .= $graph["cant"].",";
							}
							rtrim($labels,',');
							rtrim($colors,',');
							rtrim($data,',');
							?>
									<canvas id="myChart" styles="width:100%;"></canvas>
											<script>
											$(document).ready(function(){

												var ctx = document.getElementById("myChart").getContext('2d');
											var myChart = new Chart(ctx, {
												type: 'bar',
												data: {
													labels: [<?php echo $labels ?>],
													datasets: [{
														label: '# News ',
														data: [<?php echo $data ?>],
														backgroundColor: [
															<?php echo $colors ?>
														],
														borderColor: [
															<?php echo $colors ?>
														],
														borderWidth: 1
													}]
												},
												options: {
													scales: {
														yAxes: [{
															ticks: {
																beginAtZero:true
															}
														}]
													}
												}
											});

											});
										
											</script>


										</div>
										</m-bar-chart>
									</div>
									<div class="col-xl-4">
										<m-stat _nghost-c13="">
											<div _ngcontent-c13="" class="m-widget1">
												<div _ngcontent-c13="" class="m-widget1__item">
													<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
														<div _ngcontent-c13="" class="col">
															<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.USERS">Total News</h3>
															<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.USERS">All published</span>
														</div>
															<!---->
															 



										
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-brand"><?php echo $Dtotal; ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Active News</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.NEWS">Activated</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-brand"><?php echo $Dactive; ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Likes</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.LIKE">All registered</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-danger"><?php echo $Dlikes; ?></span>
															</div>
														</div>
													</div>
													<div _ngcontent-c13="" class="m-widget1__item">
														<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
															<div _ngcontent-c13="" class="col">
																<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.NEWS">Views</h3>
																<span _ngcontent-c13="" class="m-widget1__desc" translate="DASHBOARD.LABELS.COMMENTS">All registered</span>
															</div>
															<!---->
															<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
																<span _ngcontent-c13="" class="m-widget1__number m--font-success"><?php echo $Dviews; ?></span>
															</div>
														</div>
													</div>
												</div>
											</m-stat>
									
									
											</div>

								</div>

							</div>
						
						</div>