<div class="row">
	<div class="col-xl-6">
		<div class="m-portlet m-portlet--rounded-force m-portlet--full-height" mportlet="">
			<div class="m-portlet__head m-portlet__head--fit">
				<div class="m-portlet__head-progress"><!----></div>
				<div class="m-portlet__head-caption"><!---->
                    <div _ngcontent-c12="" class="m-widget14__header">
                            <h3 _ngcontent-c12="" class="m-widget14__title" translate="DASHBOARD.CARDS">Top 5 Contact Company</h3>
                            <span _ngcontent-c12="" class="m-widget14__desc" translate="DASHBOARD.LABELS.CARDS">Cards sent by users</span>
                    </div>
				</div>
				<div class="m-portlet__head-tools" style="display: none;"></div>
			</div>
			<m-stat _nghost-c13="">
				<div _ngcontent-c13="" class="m-widget1">

<?php
    if(!function_exists('random_color_part')){
        function random_color_part() {
            return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
        }
    }

    if(!function_exists('random_color')){
    function random_color() {
        return random_color_part() . random_color_part() . random_color_part();
        }
    }



    $sql__="SELECT COUNT(c.id) AS cant, c.`business` AS business FROM `it_users` AS u
            INNER JOIN `it_branches` AS b ON u.`it_branches_id` = b.`id`
            INNER JOIN `it_business` AS bu ON b.`it_business_id` = bu.`id`
            INNER JOIN `it_cards` AS c ON c.`it_users_id` = u.`id`
            WHERE  `registre_time` BETWEEN '$_POST[fec_desde]' AND '$_POST[fec_hasta]'  and bu.`is_partner` = '". 
			filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
			"' $sql_filter
            GROUP BY business
            ORDER BY cant DESC
            LIMIT 5"; 
                            
    $micon->consulta($sql__);

    while($graph=$micon->campoconsultaA()){?>
    
    <div _ngcontent-c13="" class="m-widget1__item">
						<div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
							<div _ngcontent-c13="" class="col">
								<h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.USERS"> <?php 
                                      if(empty($graph["business"])){
                                            echo "<span style='font-style: italic; color: #6c6c6c;'>Not specified</span>";
                                      }else{  
                                            echo $graph["business"];  
                                            }?></h3>
							</div>
							<!---->
							<div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
								<span _ngcontent-c13="" class="m-widget1__number m--font-brand" style="color: #<?php echo random_color()?> !important"><?php echo $graph["cant"]?></span>
							</div>
						</div>
	</div>
    
    <?php
    }
    ?>
				</div>
			</m-stat>
		</div>
	</div>




		<div class="col-xl-6">
			<div class="m-portlet m-portlet--rounded-force m-portlet--full-height" mportlet="">
				<div class="m-portlet__head m-portlet__head--fit">
					<div class="m-portlet__head-progress"><!----></div>
					<div class="m-portlet__head-caption"><!---->
                        <div _ngcontent-c12="" class="m-widget14__header">
                            <h3 _ngcontent-c12="" class="m-widget14__title" translate="DASHBOARD.CARDS">Top 5 Contact Position</h3>
                            <span _ngcontent-c12="" class="m-widget14__desc" translate="DASHBOARD.LABELS.CARDS">Cards sent by users to position</span>
                        </div>
					</div>
					<div class="m-portlet__head-tools" style="display: none;"></div>
				</div>



				<m-stat _nghost-c13="">
					<div _ngcontent-c13="" class="m-widget1">
            <?php

                $sql__="SELECT COUNT(c.id) AS cant, c.`position` AS position_name FROM `it_users` AS u
                        INNER JOIN `it_branches` AS b ON u.`it_branches_id` = b.`id`
                        INNER JOIN `it_business` AS bu ON b.`it_business_id` = bu.`id`
                        INNER JOIN `it_cards` AS c ON c.`it_users_id` = u.`id`
                        WHERE  `registre_time` BETWEEN '$_POST[fec_desde]' AND '$_POST[fec_hasta]'  and bu.`is_partner` = '". 
						filter_var($_SESSION["is_partner"], FILTER_SANITIZE_STRING).
			            "' $sql_filter
                        GROUP BY position_name
                        ORDER BY cant DESC
                        LIMIT 5"; 
                                        
                $micon->consulta($sql__);

                while($graph=$micon->campoconsultaA()){?>
                
                <div _ngcontent-c13="" class="m-widget1__item">
                                    <div _ngcontent-c13="" class="row m-row--no-padding align-items-center">
                                        <div _ngcontent-c13="" class="col">
                                            <h3 _ngcontent-c13="" class="m-widget1__title" translate="DASHBOARD.USERS">
                                   <?php 
                                      if(empty($graph["position_name"])){
                                            echo "<span style='font-style: italic; color: #6c6c6c;'>Not specified</span>";
                                      }else{  
                                            echo $graph["position_name"];  
                                            }?>
                                            </h3>
                                        </div>
                                        <!---->
                                        <div _ngcontent-c13="" class="col m--align-right ng-star-inserted">
                                            <span _ngcontent-c13="" class="m-widget1__number m--font-brand"  style="color: #<?php echo random_color()?> !important"><?php echo $graph["cant"]?></span>
                                        </div>
                                    </div>
                </div>
                
                <?php
                }
                ?>



					</div>
				</m-stat>

			</div>		
		</div>	
</div>
