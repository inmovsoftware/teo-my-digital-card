var DropzoneDemo = {
	init: function () {
		Dropzone.options.mDropzoneOne = {
			paramName: "file",
			maxFiles: 1,
			maxFilesize: 5,
			addRemoveLinks: !0,
			accept: function (e, o) {
				"justinbieber.jpg" == e.name ? o("Naha, you don't.") : o()
			}
		},Dropzone.options.mDropzoneLeads = { 
			paramName: "file",
			acceptedFiles: 'text/csv,,application/xls,application/excel,application/vnd.ms-excel,application/vnd.ms-excel; charset=binary,application/msexcel,application/x-excel,application/x-msexcel,application/x-ms-excel,application/x-dos_ms_excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			maxFiles: 1,
			maxFilesize: 5,
			addRemoveLinks: !0,
			accept: function (e, o) {
				"justinbieber.jpg" == e.name ? o("Naha, you don't.") : o()
			}
		}, Dropzone.options.mDropzoneTwo = {
			paramName: "file",
			acceptedFiles: "image/*",
			uploadMultiple: true, //
			maxFiles: 30,
			maxFilesize: 30,
			addRemoveLinks: !0,
			accept: function (e, o) {
				"justinbieber.jpg" == e.name ? o("Naha, you don't.") : o()
			}
		}, Dropzone.options.mDropzoneThree = {
			paramName: "file",
			maxFiles: 10,
			maxFilesize: 10,
			addRemoveLinks: !0,
			acceptedFiles: "image/*,application/pdf,.psd",
			accept: function (e, o) {
				"justinbieber.jpg" == e.name ? o("Naha, you don't.") : o()
			}
		}
	}
};
DropzoneDemo.init();