<?php require_once('layout/before_content.php') ?>
<?php
   $sql = "select
      it_users.id as it_users_id from it_groups_users
      LEFT JOIN it_user_group ON it_user_group.it_groups_users_id = it_groups_users.id
      LEFT JOIN it_users ON it_users.id = it_user_group.it_users_id
      where it_groups_users.id = " . $_REQUEST['id'];

    $micon->consulta($sql);
    $selected = [];
    while($member = $micon->campoconsultaA()) {
        array_push($selected, $member['it_users_id']);
    }
    $sql2 = "
        select * from it_users where id in (select id from VS_USERLOGIN where companyId = " . $_SESSION['companyID'] . ")";
    $micon1->consulta($sql2);
    $users = [];
    while($user = $micon1->campoconsultaA()) {
        array_push($users, $user);
    }

?>

<div _ngcontent-c33="">
   <m-card _ngcontent-c33="" _nghost-c21="">
    <form method="post" action="php/UserGroup_assign.php">
      <div _ngcontent-c21="" class="m-portlet m-portlet--tab">
         <div _ngcontent-c21="" class="m-portlet__head" style="display: flex;">
            <div _ngcontent-c21="" class="m-portlet__head-caption ng-star-inserted">
               <div _ngcontent-c21="" class="m-portlet__head-title">
                  <h3 _ngcontent-c21="" class="m-portlet__head-text">
                     <div _ngcontent-c21="">
                        <div _ngcontent-c33="" class="w-100" header="">
                           <div _ngcontent-c33="" class="row">
                              <div _ngcontent-c33="" class="col">
                                 <div _ngcontent-c33="" translate="USERS.SELECT_GROUP_MEMBER">Select group members</div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </h3>
               </div>
            </div>
            <div _ngcontent-c21="" class="m-portlet__head-tools ng-star-inserted">
               <div _ngcontent-c33="" actions="">
                  <div _ngcontent-c33="" class="row">
                     <div _ngcontent-c33="" class="col text-right">
                        <button _ngcontent-c33="" class="btn btn-secondary mr-2" role="button" routerlink="/user-group" tabindex="0"><i _ngcontent-c33="" class="btn-icon fal fa-times"></i><span _ngcontent-c33="" translate="GENERAL.CANCEL">Cancel</span></button>
                        <button _ngcontent-c33="" class="btn btn-danger">
                           <!----><!----><span _ngcontent-c33="" class="ng-star-inserted"><i _ngcontent-c33="" class="fal fa-save btn-icon"></i><span _ngcontent-c33="" translate="GENERAL.SAVE">Save</span></span>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div _ngcontent-c21="" class="m-portlet__body">
            <div _ngcontent-c21="">
               <div _ngcontent-c33="" content="">
                  <div _ngcontent-c33="" novalidate="" class="ng-untouched ng-pristine ng-valid">
                     <div _ngcontent-c33="" class="m-alert m-alert--outline alert alert-info alert-dismissible fade show mb-4" role="alert">To assign a user to group, please select it from the list on the left side. To unassign use the list on the right side. You can assign or deallocate the arrows in bulk. (&lt;&lt; &gt;&gt;)</div>
                     <div _ngcontent-c33="" class="row">
                        <div _ngcontent-c33="" class="select-label col">No assigned users</div>
                        <div _ngcontent-c33="" class="select-label col">Assigned users</div>
                     </div>
                     <input name="id" type="hidden" value="<?= $_REQUEST['id'] ?>" />
                     <select
                        multiple="multiple"
                        id="groups_select"
                        name="users[]"
                    >
                        <?php foreach ($users as $user) {?>
                            <option value="<?= $user['id'] ?>" <? if (in_array($user['id'], $selected)) {?>selected="selected"<? } ?>>
                                <?= $user['name'] ?> <?= $user['last_name'] ?>
                            </option>
                        <?php } ?>
                    </select>
                    </div>
               </div>
            </div>
         </div>
      </div>
    </form>
   </m-card>
</div>
<?php require_once('layout/after_content.php') ?>
<script>
    $(function() {
        $('#groups_select').bootstrapDualListbox()
    })
</script>