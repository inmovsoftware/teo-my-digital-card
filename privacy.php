<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>TEO MDC - Privacy Policy</title>
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:locale" content="en_EN"/>
    <meta property="og:image" content="http://teo.mydigitalcard.us/img/icons/android-icon-192x192.png">  
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="TEO MDC - Privacy Policy"/>
    <meta property="og:description" content="Privacy Policy"/>
    <meta property="og:url" content="http://teo.mydigitalcard.us/"/>
    <meta property="og:site_name" content="TEO MDC: log company, Directory, Company social fee and Digital business cards"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi"/>

    <meta name="theme-color" content="#007F7F">

  <meta name="description" content="TEO MDC Home Page">
  <meta name="author" content="Inmov SAS">
  <link rel="apple-touch-icon" sizes="57x57" href="img/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png">
    <link rel="manifest" href="img/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#007F7F">
    <meta name="msapplication-TileImage" content="img/icons/ms-icon-144x144.png">
    

   <!--  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"/> -->
    <style type="text/css">
        
        body {
            background: url(img/mdc_back_teoPanel2.jpg) no-repeat top center;
            background-size: cover;
            

        }
        .container{
           

            color: white
        }
        #image-container {
          position: relative;
  
          
        }
        .footer {
            position: absolute;
            bottom: 10px;
        }


    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>


<body>

<div class="container">





  <div class=" align-items-center justify-content-center h-100" style="display: flex;">
    <div class="col col-lg-8 pt-5">


        <h2>Privacy Policy</h2>
<p>Your privacy is important to us. It is Inmov SAS' policy to respect your privacy regarding any information we may collect from you across our website and App, and other sites we own and operate.</p>
<p>We only ask for personal information when we truly need it to provide a service to you. We collect it by fair and lawful means, with your knowledge and consent. We also let you know why we’re collecting it and how it will be used.</p>
<p>We only retain collected information for as long as necessary to provide you with your requested service. What data we store, we’ll protect within commercially acceptable means to prevent loss and theft, as well as unauthorised access, disclosure, copying, use or modification.</p>
<p>We don’t share any personally identifying information publicly or with third-parties, except when required to by law.</p>
<p>Our website and App may link to external sites that are not operated by us. Please be aware that we have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.</p>
<p>You are free to refuse our request for your personal information, with the understanding that we may be unable to provide you with some of your desired services.</p>
<p>Your continued use of our  website and App will be regarded as acceptance of our practices around privacy and personal information. If you have any questions about how we handle user data and personal information, feel free to contact us <abbr title="Developer Team Mail">dev@inmov.com</abbr>.</p>
<p>This policy is effective as of January 14, 2018.</p>

<p class="text-center">
<img src="img/mdclogo.svg" class="w-25 " > 
</p>

    </div>

    
   </div>




</div>


  <!-- <script src="js/scripts.js"></script> -->
</body>
</html>