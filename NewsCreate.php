<?
require('include/mysql_class.php');
include('include/seguridad.php');

$page_actual = explode('?',$_SERVER['REQUEST_URI']);
$page_actual = $page_actual[0];

$archivo_actual = basename($_SERVER['PHP_SELF']);
$sql="SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS options, path, it_modules.id
	FROM it_modules
	INNER JOIN it_options ON it_modules.id = it_options.it_modules_id
	WHERE `path`='$archivo_actual'
	ORDER BY it_modules.order, it_options.order"; 
    
$micon->consulta($sql);$migas=$micon->campoconsultaA();
$opName=$migas[OptionsName];
//echo $sql;
if($migas[OptionsLevel]==2){
	$opId	=$migas[id];
	$opName=$migas[options];
	$sql="SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS options, path, it_modules.id
	FROM it_modules
	INNER JOIN it_options ON it_modules.id = it_options.it_modules_id
		WHERE `it_options`.id='$opId'"; 
	$micon->consulta($sql);$migas=$micon->campoconsultaA();	
}
$page_title = "$migas[module]";
if ($_GET[edit]=='true'){
	$Action = 'Update';

	$sql = "SELECT * FROM it_news WHERE id = '$_GET[id]'";
	$micon->consulta($sql);
	$DtaNews =  $micon->campoconsultaA();
}else{
	$Action = 'Insert';
}

?>
<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title><?=$migas[module];?> | <?=$migas[options];?></title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />


		<!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--begin::Page Vendors Styles -->
		<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">

						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="index.html" class="m-brand__logo-wrapper">
										<img alt="" src="assets/demo/media/img/logo/logo_default_dark.png" />
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">

									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>

									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
								
							</div>

							<!-- END: Horizontal Menu -->

							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click"
										 m-dropdown-persistent="1">
											<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
												<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
												<span class="m-nav__link-icon"><i class="flaticon-alarm"></i></span>
											</a>
											<? require('php/notifications.php');?>
										</li>
										<li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-text">
													<img class="m-topbar__language-selected-img" src="assets/app/media/img/flags/020-flag.svg">
												</span>
											</a>
											<? require('php/language.php');?>
										</li>
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
										 m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="srvs/media/images/profiles/<?=$_SESSION['UserAvatar'];?>" style="width: 41px; height: 41px" class="m--img-rounded m--marginless" alt="" />
												</span>
												<span class="m-topbar__username m--hide">Nick</span>
											</a>
											<? require('php/profile.php');?>
										</li>
									</ul>
								</div>
							</div>

							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>

			<!-- END: Header -->

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

					<!-- BEGIN: Aside Menu -->
					<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							
						<?
						$OpenStatus = '';
						$sql  = "SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS OPTIONS, path, it_modules.id as ModId
								FROM it_profile_option
								INNER JOIN it_options ON it_profile_option.it_options_id = it_options.id
								INNER JOIN it_modules ON it_modules.id = it_options.it_modules_id
								WHERE it_profile_option.it_profiles_id = '".$_SESSION['UserProfile']."' and it_options.status='A'
								group by it_modules.name
								ORDER BY it_modules.order, it_options.order";
						$micon->consulta($sql);
						while($DtaModule = $micon->campoconsultaA()){	  
							if($_GET[open] == $DtaModule[ModId]){
								$OpenStatus = ' m-menu__item--expanded m-menu__item--open';
							}else{
								$OpenStatus = '';
							}
						?>
							<li class="m-menu__item  m-menu__item--submenu <?=$OpenStatus;?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon <?=$DtaModule[icon];?>"></i><span class="m-menu__link-text"><?=$DtaModule[module];?></span><i
									 class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									
									<ul class="m-menu__subnav">
										<?
										$active = '';
										$sql = "SELECT it_options.name AS options, path
												FROM it_profile_option
												INNER JOIN it_options ON it_profile_option.it_options_id = it_options.id
												WHERE it_modules_id = '".$DtaModule[ModId]."' and it_profile_option.it_profiles_id = '".$_SESSION['UserProfile']."' AND it_options.status='A'
												ORDER BY  it_options.order";
										$micon1->consulta($sql);	
										while($Dtaoption = $micon1->campoconsultaA()){
											$Url = '/'.$Dtaoption[path];
											if($page_actual == $Url){
												$active = 'm-menu__item--active';
											}else{
												$active = '';
											}
										?>
										<li class="m-menu__item <?=$active;?>" aria-haspopup="true"><a href="<?=$Dtaoption[path].'?open='.$DtaModule[ModId];?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?=$Dtaoption[options]?></span></a></li>
									<? } ?>
									</ul>

								</div> 
							</li>
						<?
						}	
						?>	
						</ul>
					</div>

					<!-- END: Aside Menu -->
				</div>

				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader "> 
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									<i class="fal  <?=$migas[icon]?>"></i> <?=$migas[module]?></h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text"><?=$migas[options]?></span>
										</a> 
									</li>
								</ul>
							</div>

							<!--estacio para indacadores -->
						
							<!--fin estacio para indacadores -->
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">
						<form class="m-form m-form--state m-form--fit m-form--label-align-right" method="post" id="FormNews" action="php/NewsCreate.php" enctype="multipart/form-data">
						<!--Begin::Section-->
								<div class="m-portlet m-portlet--last m-portlet--head-md m-portlet--responsive-mobile" id="main_portlet">
										<div class="m-portlet__head">
											<div class="m-portlet__head-progress">

												<!-- here can place a progress bar-->
											</div>
											<div class="m-portlet__head-wrapper">
												<div class="m-portlet__head-caption">
													<div class="m-portlet__head-title">
														<h3 class="m-portlet__head-text">
															Create News
														</h3>
													</div>
												</div>
												<div class="m-portlet__head-tools">
													<a href="#" onclick="window.history.back();" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
														<span>
															<i class="la la-arrow-left"></i>
															<span>Back</span>
														</span>
													</a>
													<div class="btn-group">
														<button type="submit" class="btn btn-danger  m-btn m-btn--icon m-btn--wide m-btn--md">
															<span>
																<i class="la la-check"></i>
																<span>Save</span>
															</span>
														</button>
													</div>
												</div>
											</div>
										</div>
										<div class="m-portlet__body">
											

											<div class="row">

												<div class="col-12">
													<div class="tab-content">
															<div class="form-group m-form__group row">
																<div class="col-lg-12">
																	<label>Title of the news:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<input type="text" class="form-control m-input" placeholder="Title of the news" name="InputTitle" value="<?=$DtaNews[title];?>">
																		<input type="hidden" name="action" value="<?=$Action;?>">
																		<input type="hidden" name="id" value="<?=$_GET[id];?>">
																		<input type="hidden" name="companyID" value="<?=$_SESSION['companyID'];?>">
																		<input type="hidden" name="UserId" value="<?=$_SESSION['UserID'];?>">
																		<input type="hidden" name="open" value="<?=$_GET[open];?>">
																		<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fal  fal fa-signature"></i></span></span>
																	</div>
																</div>
															</div>
															<div class="form-group m-form__group row">
																<div class="col-lg-6">
																	<label>Introductory text:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<input type="text" class="form-control m-input" placeholder="Introductory text" name="InputIntroductory" value="<?=$DtaNews[intro];?>">
																		<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fal  fal fa-file-alt"></i></span></span>
																	</div>
																</div>
																<div class="col-lg-6">
																	<label>Upload cover photo:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<input type="file" class="form-control m-input" name="InputPhoto">
																		<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fal fa-upload"></i></span></span>
																	</div>
																</div>
															</div>
															<div class="form-group m-form__group row">
																<div class="col-lg-6">
																	<label>Publication date:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<? $trpstartdate1 = substr_replace($DtaNews[publication_date],T,11,0); 
																			$InputDatePub = preg_replace('/\s+/', '', $trpstartdate1);?>
																		<input type="datetime-local" class="form-control m-input" placeholder="Publication date" name="InputDatePub" value="<?=$InputDatePub;?>">
																		<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-calendar-check"></i></span></span>
																	</div>
																</div>
																<div class="col-lg-6">
																	<label class="">End date of the publication:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<? $trpstartdate2 = substr_replace($DtaNews[close_date],T,11,0); 
																			$InputDateEnd = preg_replace('/\s+/', '', $trpstartdate2);?>
																		<input type="datetime-local" class="form-control m-input" placeholder="End date of the publication" name="InputDateEnd" value="<?=$InputDateEnd;?>">
																		<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fal  fal fa-calendar-times"></i></span></span>
																	</div>
																</div>
															</div>
															<div class="form-group m-form__group row">
																<div class="col-lg-6">
																	<label class="">Branch:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<select class="form-control " name="InputBranch[]" id="m_bach" multiple="multiple">
																			<option value="">---</option>
																			<?
																			$sql = "SELECT id,name FROM it_branches WHERE it_business_id='".$_SESSION['companyID']."'";
																			$micon1->consulta($sql);
																			while($DtaBranch = $micon1->campoconsultaA()){	
																				$sql = "SELECT it_branch_id
																						FROM it_new_branch 
																						WHERE it_news_id ='".$DtaNews[id]."' AND it_branch_id='".$DtaBranch[id]."'";
																				$micon2->consulta($sql);
																				$DtaBranchSelected = $micon2->campoconsultaA();
																				if($DtaBranchSelected[it_branch_id]==$DtaBranch[id]){
																					$selectedB = 'selected';
																				}else{
																					$selectedB = '';
																				}		
																			?>
																			<option value="<?=$DtaBranch[id];?>" <?=$selectedB;?>> <?=$DtaBranch[name];?></option>
																			<? } 
																				
																			?>
																		</select>
																	</div>
																</div>
																<div class="col-lg-6">
																	<label class="">Category:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<select class="form-control m-select2 " id="m_select2_3" name="InputCategory"  >
																			<option value=""></option>
																			<?
																			
																			
																			$sql = "SELECT id,name,color FROM it_categories_news WHERE it_business_id='".$_SESSION[companyID]."'";	
																			$micon1->consulta($sql);
																			while($DtaCategory = $micon1->campoconsultaA()){	
																				if($DtaNews[it_categories_news_id] == $DtaCategory[id]){
																					$selected = 'selected';
																				}else{
																					$selected = '';
																				}
																			?>
																			<option value="<?=$DtaCategory[id];?> " <?=$selected;?> > <?=$DtaCategory[name];?></option>
																			<? } 
																				
																			?>
																		</select>
																	</div>
																</div>
																
															</div>
															<div class="form-group m-form__group row">	
																<div class="col-lg-6">
																	<label class="">Position:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<select class="form-control " name="InputPosition[]" id="m_select2_2" multiple="multiple">
																			<option value="">All</option>
																			<?
																			$sql = "SELECT id,name FROM it_positions WHERE it_business_id='".$_SESSION['companyID']."'";	
																			$micon1->consulta($sql);
																			while($DtaPositions = $micon1->campoconsultaA()){	
																				$sql = "SELECT it_position_id
																						FROM it_new_position 
																						WHERE it_news_id ='".$DtaNews[id]."' AND it_position_id='".$DtaPositions[id]."'";
																				$micon2->consulta($sql);
																				$DtaPosiSelected = $micon2->campoconsultaA();
																				if($DtaPosiSelected[it_position_id]==$DtaPositions[id]){
																					$selectedP = 'selected';
																				}else{
																					$selectedP = '';
																				}	
																			?>
																			<option value="<?=$DtaPositions[id];?>" <?=$selectedP;?>> <?=$DtaPositions[name];?></option>
																			<? } 
																				
																			?>
																		</select>
																	</div>
																</div>
																<div class="col-lg-6">
																	<label class="">Group to publish:</label>
																	<div class="m-input-icon m-input-icon--right">
																		<select class="form-control" name="InputGroup[]"  multiple="multiple" id="m_Group">
																			<option value="">All</option>
																			<?
																			$sql = "SELECT id,name FROM it_groups_users WHERE it_business_id='".$_SESSION['companyID']."'";	
																			$micon1->consulta($sql);
																			while($DtaGroup = $micon1->campoconsultaA()){	
																				$sql = "SELECT it_groups_users_id
																						FROM it_new_groupuser 
																						WHERE it_news_id ='".$DtaNews[id]."' AND it_groups_users_id='".$DtaGroup[id]."'";
																				$micon2->consulta($sql);
																				$DtaGroupSelected = $micon2->campoconsultaA();
																				if($DtaGroupSelected[it_groups_users_id]==$DtaGroup[id]){
																					$selectedG = 'selected';
																				}else{
																					$selectedG = '';
																				}
																			?>
																			<option value="<?=$DtaGroup[id];?>" <?=$selectedG;?>> <?=$DtaGroup[name];?></option>
																			<? } 
																				
																			?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="form-group m-form__group row">
																<div class="col-lg-12">
																	<label class="label">Text :</label>
																	<textarea class="summernote" id="m_summernote_1" name="InputText"><?=$DtaNews[text];?></textarea>

																</div>
															</div>	
														</div>
													</div>	
												</div> 
												
											</div>
											
										
										
								</div>

						</form>
					</div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			<footer class="m-grid__item		m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								<?=date('Y')?> &copy; Inmov S.A.S. 
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<!-- lado derecho del footer -->
						</div>
					</div>
				</div>
			</footer>

			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->



		<!-- end::Quick Sidebar -->

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->

	

		<!--begin:: Global Mandatory Vendors -->
		<script src="vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		<script src="assets/demo/default/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>
		<script src="assets/demo/default/custom/crud/forms/widgets/summernote.js" type="text/javascript"></script>
		<script src="assets/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>

				<script type="text/javascript">
		var FormControls = function () {
	    //== Private functions
	    

	    var demo3 = function () {
	        $( "#FormNews" ).validate({
	            // define validation rules
	            rules: {
	                //=== Client Information(step 3)
	                //== Billing Information
	                InputTitle: {
	                    required: true
	                },
	                InputIntroductory: {
	                    required: true,
	                },
	                <? if($Action=='Insert'){?> 
	                InputPhoto: {
	                    required: true,
	                },
	               <? }?>
	                InputDatePub: {
	                    required: true,
	                },
	                inputType: {
	                    required: true,
	                },
	                InputText: {
	                    required: true,
	                }

	            },
	            
	            //display error alert on form submit  
	            invalidHandler: function(event, validator) {
	                mUtil.scrollTo("m_form_3", -200); 

	                swal({
	                    "title": "", 
	                    "text": "There are some errors in your submission. Please correct them.", 
	                    "type": "error",
	                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide",
	                    "onClose": function(e) {
	                        console.log('on close event fired!');
	                    }
	                });

	                event.preventDefault();
	            },

	            submitHandler: function (form) {
	                form[0].submit(); // submit the form
	                swal({
	                    "title": "", 
	                    "text": "Form validation passed. All good!", 
	                    "type": "success",
	                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
	                });

	                return false;
	            }
	        });       
	    }

	    return {
	        // public functions
	        init: function() {
	            demo3(); 
	        }
	    };
	}();

	jQuery(document).ready(function() {    
	    FormControls.init();
	});
		</script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>