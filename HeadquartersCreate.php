<?
require('include/mysql_class.php');
include('include/seguridad.php');

$page_actual = explode('?',$_SERVER['REQUEST_URI']);
$page_actual = $page_actual[0];

$archivo_actual = basename($_SERVER['PHP_SELF']);
$sql="SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS options, path, it_modules.id
	FROM it_modules
	INNER JOIN it_options ON it_modules.id = it_options.it_modules_id
	WHERE `path`='$archivo_actual'
	ORDER BY it_modules.order, it_options.order"; 
    
$micon->consulta($sql);$migas=$micon->campoconsultaA();
$opName=$migas[OptionsName];
//echo $sql;
if($migas[OptionsLevel]==2){
	$opId	=$migas[id];
	$opName=$migas[options];
	$sql="SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS options, path, it_modules.id
	FROM it_modules
	INNER JOIN it_options ON it_modules.id = it_options.it_modules_id
		WHERE `it_options`.id='$opId'"; 
	$micon->consulta($sql);$migas=$micon->campoconsultaA();	
}
$page_title = "$migas[module]";

?>
<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title><?=$migas[module];?> | <?=$migas[options];?></title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<style>
			/* Always set the map height explicitly to define the size of the div
			* element that contains the map. */
			#map {
			 height: 400px;
			width:100%;
			}
			/* Optional: Makes the sample page fill the window. */
			html, body {
			height: 100%;
			margin: 0;
			padding: 0;
			}
			#description {
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			}

			#infowindow-content .title {
			font-weight: bold;
			}

			#infowindow-content {
			display: none;
			}

			#map #infowindow-content {
			display: inline;
			}

			.pac-card {
			margin: 10px 10px 0 0;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
			background-color: #fff;
			font-family: Roboto;
			}

			#pac-container {
			padding-bottom: 12px;
			margin-right: 12px;
			}

			.pac-controls {
			display: inline-block;
			padding: 5px 11px;
			}

			.pac-controls label {
			font-family: Roboto;
			font-size: 13px;
			font-weight: 300;
			}

			#pac-input {
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			width: 400px;
            margin-top: 8px;
            height: 45px;
            border: 1px solid #45ccb1
			}

			#pac-input:focus {
			border-color: #4d90fe;
			}

			#title {
			color: #fff;
			background-color: #4d90fe;
			font-size: 25px;
			font-weight: 500;
			padding: 6px 12px;
			}
			#target {
			width: 345px;
			}
            img.logo-img {
                margin: 0 auto;
                margin-bottom: 16px !important;
            }
            .input-disabled{
                background-color: #57596217;
                border-color: #dddee0 !important;
            }
		</style>

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--begin::Page Vendors Styles -->
		<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">

						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="index.html" class="m-brand__logo-wrapper">
										<img alt="" src="assets/demo/media/img/logo/logo_default_dark.png" />
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">

									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>

									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
								
							</div>

							<!-- END: Horizontal Menu -->

							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click"
										 m-dropdown-persistent="1">
											<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
												<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
												<span class="m-nav__link-icon"><i class="flaticon-alarm"></i></span>
											</a>
											<? require('php/notifications.php');?>
										</li>
										<li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-text">
													<img class="m-topbar__language-selected-img" src="assets/app/media/img/flags/020-flag.svg">
												</span>
											</a>
											<? require('php/language.php');?>
										</li>
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
										 m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="srvs/media/images/profiles/<?=$_SESSION['UserAvatar'];?>" style="width: 41px; height: 41px" class="m--img-rounded m--marginless" alt="" />
												</span>
												<span class="m-topbar__username m--hide">Nick</span>
											</a>
											<? require('php/profile.php');?>
										</li>
									</ul>
								</div>
							</div>

							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>

			<!-- END: Header -->

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

					<!-- BEGIN: Aside Menu -->
					<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							
						<?
						$OpenStatus = '';
						$sql  = "SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS OPTIONS, path, it_modules.id as ModId
								FROM it_profile_option
								INNER JOIN it_options ON it_profile_option.it_options_id = it_options.id
								INNER JOIN it_modules ON it_modules.id = it_options.it_modules_id
								WHERE it_profile_option.it_profiles_id = '".$_SESSION['UserProfile']."' and it_options.status='A'
								group by it_modules.name
								ORDER BY it_modules.order, it_options.order";
						$micon->consulta($sql);
						while($DtaModule = $micon->campoconsultaA()){	  
							if($_GET[open] == $DtaModule[ModId]){
								$OpenStatus = ' m-menu__item--expanded m-menu__item--open';
							}else{
								$OpenStatus = '';
							}
						?>
							<li class="m-menu__item  m-menu__item--submenu <?=$OpenStatus;?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon <?=$DtaModule[icon];?>"></i><span class="m-menu__link-text"><?=$DtaModule[module];?></span><i
									 class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									
									<ul class="m-menu__subnav">
										<?
										$active = '';
										$sql = "SELECT it_options.name AS options, path
												FROM it_profile_option
												INNER JOIN it_options ON it_profile_option.it_options_id = it_options.id
												WHERE it_modules_id = '".$DtaModule[ModId]."' and it_profile_option.it_profiles_id = '".$_SESSION['UserProfile']."' AND it_options.status='A'
												ORDER BY  it_options.order";
										$micon1->consulta($sql);	
										while($Dtaoption = $micon1->campoconsultaA()){
											$Url = '/'.$Dtaoption[path];
											if($page_actual == $Url){
												$active = 'm-menu__item--active';
											}else{
												$active = '';
											}
										?>
										<li class="m-menu__item <?=$active;?>" aria-haspopup="true"><a href="<?=$Dtaoption[path].'?open='.$DtaModule[ModId];?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?=$Dtaoption[options]?></span></a></li>
									<? } ?>
									</ul>

								</div> 
							</li>
						<?
						}	
						?>	
						</ul>
					</div>

					<!-- END: Aside Menu -->
				</div>

				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader "> 
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									<i class="fal  <?=$migas[icon]?>"></i> <?=$migas[module]?></h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text"><?=$migas[options]?></span>
										</a> 
									</li>
								</ul>
							</div>

							<!--estacio para indacadores -->
						
							<!--fin estacio para indacadores -->
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">

						<!--Begin::Section-->
						<div class="m-portlet m-portlet--last m-portlet--head-md m-portlet--responsive-mobile" id="main_portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-progress">

									<!-- here can place a progress bar-->
								</div>
								<div class="m-portlet__head-wrapper">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text">
												New Branches/Regions
											</h3>
										</div>
									</div>
									
									<div class="m-portlet__head-tools">
										<a href="HeadquartersList.php?open=<?=$_GET[open];?>" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
											<span>
												<i class="la la-arrow-left"></i>
												<span>Back</span>
											</span>
										</a>
										<div class="btn-group">
											<input type="submit" form="FormCreate" class="btn btn-danger  m-btn m-btn--icon m-btn--wide m-btn--md"
											  value="save">
										</div>
									</div>
								</div>
							</div>
							<div class="m-portlet__body">
							
							
								<form enctype="multipart/form-data" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
										id="FormCreate" action="php/HeadquartersSave.php" method="POST" >
										
									<!-- Hidden Fields --->
									<input type="hidden" name="action" id="action" value="Insert">
									<input type="hidden" name="open" id="open" value="<?=$_GET[open];?>">

									<div class="row">
										<div class="col-2 border-right">
											<?
											$sql="SELECT `it_business`.`logo`, `it_business`.`id`, `it_business`.`name`
													FROM `it_business` 											
													WHERE `it_business`.`id`='".$_SESSION['companyID']."'";
											$micon->consulta($sql);
											$row=$micon->campoconsultaA();
										
											$foto =  $row['logo'];
											$directorio = './srvs/media/images/business/'; 
											$logo=$directorio.$foto;
											?>
											<div class="row">
												<img src="<?=$logo;?>"  height="150" width="170" class="mx-auto" >
											</div>
		
											<?$miBusinessID=$_SESSION['companyID'];include_once('php/business_conformation_view.php');?>
										</div>
										<div class="col-10">
											<ul class="nav nav-tabs  m-tabs-line m-tabs-line--success" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#company_1" role="tab">Basic Info</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#company_2" role="tab">Social network</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#company_3" role="tab">Location</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="company_1" role="tabpanel">
													<div class="form-group m-form__group row">
													
														<div class="col-lg-6 m-form__group-sub">
															<label>Company :</label>
															<div class="m-input-icon m-input-icon--right">
																<select class="form-control"  name="CompanyId" id="CompanyId">
																	<?
																	$sql0="SELECT `id`,
																			CASE WHEN `is_partner`='true' 
																			   THEN CONCAT(`name`,' ','(Partner)')
																			   ELSE CONCAT(`name`,' ','(Owner)')
																			END `name`
																			FROM `it_business`
																			WHERE `status`='A' ";
																	$sql1="";		
																	if($_SESSION['is_partner']=='true'){
																		$sql1=" AND `id`=".$_SESSION['companyID'] ;
																	}
																	$sql=$sql0.$sql1;
																	$micon->consulta($sql);
																	while($row=$micon->campoconsultaA()){
																		$sel="";
																		if($row[id]==$_SESSION['companyID']){$sel="selected";}
																		echo "<option value='".$row[id]."' ".$sel." >".$row[name]."</option>";
																	}
																	?>
																</select>
															</div>
														</div>
														<div class="col-lg-6 m-form__group-sub">
															<label>Branches/Regions Name:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="HeadquatersName" id="HeadquatersName"  placeholder="Branches/Regions Name">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
													</div>
													
													<div class="form-group m-form__group row">
														<div class="col-lg-6 m-form__group-sub">
															<label>Language:</label>
															<div class="m-input-icon m-input-icon--right">
																<select class="form-control"  name="CompanyLanguage" id="CompanyLanguage">
																	<option value='0'>Select Language</option>
																	<option value='EN'>English</option>
																	<option value='ES'>Español</option>
																</select>
															</div>
														</div>
														
														<div class="col-lg-6">
															<label >Phone:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="number" class="form-control m-input" name="CompanyPhone" id="CompanyPhone"  placeholder="Phone" >
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fal  fal fa-phone"></i></span></span>
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane" id="company_2" role="tabpanel">
																									
													<?
													// Toma id de Branch 
													$miBranchID= $_SESSION['UserBranch'];
													include('php/business_social_update.php');
													?>
													
												</div>
												<div class="tab-pane " id="company_3" role="tabpanel">
                                                    <div class="alert alert-success" role="alert">
                                                        <strong>Remember: </strong> To complete the information of the fields at the bottom, you can specify the name of your company in the Search field or simply by dragging the map's PIN
                                                    </div>
                                                    <div class="form-group m-form__group row" style="padding: 0;     padding-bottom: 15px;">
                                                        <div class="col-lg-12">
                                                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                                            <div id="map" height="800px" width="100%"></div>
                                                        </div>
                                                    </div>
												<div class="form-group m-form__group row">
												
														<div class="col-lg-5 m-form__group-sub">
															<label>Company Address:</label>
															<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control m-input" name="CompanyAddress" id="CompanyAddress" readonly >
																<span class="m-input-icon__icon m-input-icon__icon--right"></span>
															</div>
														</div>
														<div class="col-lg-3 m-form__group-sub">
															<label>Country:</label>
															<div class="m-input-icon m-input-icon--right input-disabled">
																<input type="text" class="form-control m-input" name="CompanyCountry" id="CompanyCountry"  readonly >
																<input type="Hidden" name="CompanyCountryID" id="CompanyCountryID" readonly>
																<span class="m-input-icon__icon m-input-icon__icon--right"></span>
															</div>
														</div>
														<div class="col-lg-2 m-form__group-sub">
															<label>State:</label>
															<div class="m-input-icon m-input-icon--right input-disabled">
																<input type="text" class="form-control m-input" name="CompanyState" id="CompanyState" readonly >
																<input type="Hidden" name="CompanyStateID" id="CompanyStateID" readonly>
																<span class="m-input-icon__icon m-input-icon__icon--right"></span>
															</div>
														</div>
														<div class="col-lg-2 m-form__group-sub">
															<label>City:</label>
															<div class="m-input-icon m-input-icon--right input-disabled">
																<input type="text" class="form-control m-input" name="CompanyCity" id="CompanyCity" readonly >
																<input type="Hidden" name="CompanyCityID" id="CompanyCityID" readonly>
																<span class="m-input-icon__icon m-input-icon__icon--right"></span>
															</div>
														</div>
													</div>
													
													<div class="form-group m-form__group row">
														<div class="col-lg-4">
															<label>Latitude:</label>
															<div class="m-input-icon m-input-icon--right input-disabled">
																<input type="text" class="form-control m-input" name="CompanyLatitude" id="CompanyLatitude"  readonly value="<?=$DatPartner['latitude']?>">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
														<div class="col-lg-4">
															<label class="">Longitude:</label>
															<div class="m-input-icon m-input-icon--right input-disabled">
																<input type="text" class="form-control m-input" name="CompanyLongitude" id="CompanyLongitude" readonly value="<?=$DatPartner['longitude']?>">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
														<div class="col-lg-4">
															<label class="">Place ID:</label>
															<div class="m-input-icon m-input-icon--right input-disabled">
																<input type="text" class="form-control m-input" name="CompanyPlaceID" id="CompanyPlaceID" readonly value="<?=$DatPartner['placeid']?>">
																<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-map-marked-alt"></i></span></span>
															</div>
														</div>
													</div>
												
												</div>
											</div>	
										</div> 
										
									</div>
									
								</form>
							</div>
						</div>
					
					</div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			<footer class="m-grid__item		m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								<?=date('Y')?> &copy; Inmov S.A.S. 
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<!-- lado derecho del footer -->
						</div>
					</div>
				</div>
			</footer>

			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->



		<!-- end::Quick Sidebar -->

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->

	

		<!--begin:: Global Mandatory Vendors -->
		<script src="vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		<script src="components/PartnerList.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
		
		<script type="text/javascript">
		
		$.validator.addMethod("valueNotEquals", function(value, element, arg){
			return arg !== value;
		}, "Value Invalid");

			var FormControls = function () {
				//== Private functions
				

				var demo3 = function () {
					$( "#FormCreate" ).validate({
						// define validation rules
						rules: {
							//=== Client Information(step 3)
							//== Billing Information
							CompanyId: {
								required: true
							},
							HeadquatersName: {
								required: true
							},
							CompanyLanguage: {
								required: true,
								valueNotEquals: "0"
							},
							CompanyAddress: {
								required: true
							},
							CompanyCountry: {
								required: true
							},
							CompanyState: {
								required: true
							},
							CompanyCity: {
								required: true
							},
							CompanyPhone: {
								required: true
							}
						},
						
						//display error alert on form submit  
						invalidHandler: function(event, validator) {
							mUtil.scrollTo("m_form_3", -200); 

							swal({
								"title": "", 
								"text": "There are some errors in your submission. Please correct them.", 
								"type": "error",
								"confirmButtonClass": "btn btn-secondary m-btn m-btn--wide",
								"onClose": function(e) {
									console.log('on close event fired!');
								}
							});

							event.preventDefault();
						},

						submitHandler: function (form) {
							
							form[0].submit(); // submit the form
							
							swal({
								"title": "", 
								"text": "Form validation passed. All good!", 
								"type": "success",
								"confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
							});

							return false;
						}
					});       
				}

				return {
					// public functions
					init: function() {
						demo3(); 
					}
				};
			}();

			jQuery(document).ready(function() {    
				FormControls.init();
			});
			
		</script>
		
		 
    <script>
	
	var MiMarker=0;
	var infowindow;
    var messagewindow;
	
	var miLat= 4.58684750000000;
	var milng= -74.08153519999996;
	var MiTitulo= "Your Site";

	function initAutocomplete() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: miLat, lng: milng},
			zoom: 13,
			mapTypeId: 'roadmap'
		});
		
		var geocoder = new google.maps.Geocoder;
		
 		if(MiMarker==0){
			MiMarker=1;
			var marker=new google.maps.Marker({
				map: map,
				title: MiTitulo,
                draggable: true,
				position: {lat: miLat, lng: milng}
			});

            // Try HTML5 geolocation. | Current Lcoation
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    infoWindow.setPosition(pos);
                    infoWindow.setContent('Location found.');
                    infoWindow.open(map);
                    map.setCenter(pos);
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

            google.maps.event.addListener(marker, 'dragend', function(event){

                latlng = marker.getPosition();
                //console.log(latlng.lat());
                document.getElementById('CompanyLatitude').value = latlng.lat();
                document.getElementById('CompanyLongitude').value = latlng.lng();

                Migeocode(geocoder, map, latlng) ;
            });

			var latlng = marker.getPosition();

			document.getElementById('CompanyLatitude').value = latlng.lat();
			document.getElementById('CompanyLongitude').value = latlng.lng();

			Migeocode(geocoder, map, latlng) ;
		}
		
		// Create the search box and link it to the UI element.
		
		var input = document.getElementById('pac-input');
	
		var searchBox = new google.maps.places.SearchBox(input);
		
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});

		var markers = [];
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			
			var places = searchBox.getPlaces();

			if (places.length == 0) {
				return;
			}

			// Clear out the old markers.
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];
			
			
			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();
			places.forEach(function(place){
				
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};

			 	// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  draggable: true,
				  position: place.geometry.location
				})); 
				
				// micodigo
				
				var marker=markers[markers.length-1];
				
				var latlng = {
				
					lat: place.geometry.location.lat(),
					lng: place.geometry.location.lng()
				};
		  
				//var geocoder = new google.maps.Geocoder;
				
				document.getElementById('CompanyLatitude').value = place.geometry.location.lat();
				document.getElementById('CompanyLongitude').value = place.geometry.location.lng();
				
                Migeocode(geocoder, map, latlng) ;


				google.maps.event.addListener(marker, 'dragend', function(event){
					
					latlng = marker.getPosition();
					//console.log(latlng.lat());
					document.getElementById('CompanyLatitude').value = latlng.lat();
					document.getElementById('CompanyLongitude').value = latlng.lng();
					
					Migeocode(geocoder, map, latlng) ;
				});
				
				// end micodigo
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				 
				} else {
				  bounds.extend(place.geometry.location);
				}
					
			});
			map.fitBounds(bounds);
		});
	}	

	function Migeocode(geocoder, map, latlng) {	
		geocoder.geocode({
			'location': latlng
			}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					if (results[0]) {
                        map.setCenter(latlng);
						map.setZoom(18);
						console.log(results);
						//console.log(latlng);
						var placeID = results[0].place_id;
						document.getElementById('CompanyPlaceID').value=placeID;

						var cant=results[0].address_components.length;

						for(var j=0; j<cant; j++) {
							miObj=results[0].address_components[j];
							
							Object.keys(miObj).forEach(function (key) {
								
								if(key=='long_name'){
									long_name=miObj[key];
								}
								if(key=='short_name'){
									short_name =miObj[key];
								}						
								if(key=='types'){
									Miarray=miObj[key];
									if(Miarray[0]=='country'){
										document.getElementById('CompanyCountry').value=long_name;
										document.getElementById('CompanyCountryID').value=short_name;
									}
									if(Miarray[0]=='administrative_area_level_1'){
										document.getElementById('CompanyState').value=long_name;
										document.getElementById('CompanyStateID').value=short_name;
									}
									if(Miarray[0]=='administrative_area_level_2'){
										document.getElementById('CompanyCity').value=long_name;
										document.getElementById('CompanyCityID').value=short_name;
									}
								}
							})
						}
					}
					document.getElementById('CompanyAddress').value=results[1].formatted_address;
					
				}
				
			}
		);	
	};
	
	$('#pac-input').keydown(function (e) {
		if (e.keyCode == 13 || e.keyCode == 9) {
			$(e.target).blur();
		};
	})
    </script>
    <script async defer
  	   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM-x4fGWDESn1d2wjRqh_qhrPCfXZtSlY&libraries=places,geometry&callback=initAutocomplete&language=EN">
    </script>


	</body>

	<!-- end::Body -->
</html>