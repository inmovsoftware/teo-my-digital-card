<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>TEO MDC - Privacy Policy</title>
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:locale" content="en_EN"/>
    <meta property="og:image" content="http://teo.mydigitalcard.us/img/icons/android-icon-192x192.png">  
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="TEO MDC - Privacy Policy"/>
    <meta property="og:description" content="Privacy Policy"/>
    <meta property="og:url" content="http://teo.mydigitalcard.us/"/>
    <meta property="og:site_name" content="TEO MDC: log company, Directory, Company social fee and Digital business cards"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi"/>


    <meta name="theme-color" content="#007F7F">

  <meta name="description" content="TEO MDC Home Page">
  <meta name="author" content="Inmov SAS">
  <link rel="apple-touch-icon" sizes="57x57" href="img/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png">
    <link rel="manifest" href="img/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#007F7F">
    <meta name="msapplication-TileImage" content="img/icons/ms-icon-144x144.png">
    

   <!--  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"/> -->
   <script src="js/hello.all.js"></script>
    <style type="text/css">
        
        body {
            background: url(img/mdc_back_teoPanel2.jpg) no-repeat top center fixed;
		    -webkit-background-size: cover;
		    -moz-background-size: cover;
		    -o-background-size: cover;
		    background-size: cover;
            

        }
        .container{
           

            color: white
        }
        #image-container {
          position: relative;
  
          
        }
        .footer {
            position: absolute;
            bottom: 10px;
        }
        html{
    background:#eee;
    height:100%
}
body{
    text-align:center;
    display:-webkit-flex;
    display:flex;
    height:100%;
    -webkit-flex-flow:column wrap;
    flex-flow:column wrap;
    -webkit-justify-content:center;
    justify-content:center;
    margin:0;
    padding:10px
}
.loading{
    font-size:50px;
    height:1em;
    -webkit-animation:fadeout .5s 5s;
    animation:fadeout .5s 5s;
    -webkit-animation-fill-mode:forwards;
    animation-fill-mode:forwards
}
.loading span{
    -webkit-animation:fadein .5s .5s alternate infinite;
    animation:fadein .5s .5s alternate infinite
}
.loading span:nth-child(2){
    -webkit-animation-delay:.75s;
    animation-delay:.75s
}
.loading span:nth-child(3){
    -webkit-animation-delay:1s;
    animation-delay:1s
}
p,h2{
    text-align:center;
    font-family:helvetica;
    opacity:0;
    -webkit-animation:fadein .5s 3s;
    animation:fadein .5s 3s;
    -webkit-animation-fill-mode:forwards;
    animation-fill-mode:forwards
}
p{
    font-size:smaller;
    word-break:break-word;
    -webkit-animation-delay:5s;
    animation-delay:5s
}
.overflow{
    background:#000;
    color:#fff;
    max-width:100%;
    text-align:left
}
@-webkit-keyframes fadein{
    from{
        opacity:0;
        -webkit-transform:scale(.2)
    }
    to{
        opacity:1;
        -webkit-transform:scale(1)
    }
}
@-webkit-keyframes fadeout{
    to{
        opacity:0;
        height:0
    }
}
@keyframes fadein{
    from{
        opacity:0;
        transform:scale(.2)
    }
    to{
        opacity:1;
        transform:scale(1)
    }
}
@keyframes fadeout{
    to{
        opacity:0;
        height:0
    }
}



    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>


<body>

<div class="container">



	

  <div class=" align-items-center justify-content-center h-100" style="display: flex;">

    <div class="col col-lg-8 pt-5">
		
		<div class="loading text-center"><span>&bull;</span><span>&bull;</span><span>&bull;</span></div>
        <h2 class="text-center">Please close this window to continue.</h2>



<script>
		window.onbeforeunload = function(){
			document.getElementsByTagName('h2')[0].innerHTML="Redirecting, please wait";
		}
	</script>

    </div>

    
   </div>




</div>



</body>
</html>