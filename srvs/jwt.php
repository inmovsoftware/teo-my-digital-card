<?php

// require the autoloader that composer created
require_once 'vendor/autoload.php';

// use the firebase JWT Library previous installed using composer
use Firebase\JWT\JWT;
//use Twilio\Rest\Client;

// create a class for easy code structure and use
class userAuth {

	// create a dummy user for the tutorial
	// create an empty id variable to hold the user id
	/*function __construct() {
       print "En el constructor BaseClass\n";
   }*/
	//	private $id;
	//private $usrID;
	private $uData;
	// key for JWT signing and validation, shouldn't be changed
	private $key = "e34de89659579b538704acf9612dae72";

	//private $user = array();

	/*function userAuth($usrID = "", $password = "") {
		$this->user = array(
			"usrID" => $usrID,
			"password" => $password
		);
		
	}*/


	// Checks if the user exists in the database
	/*private function validUser( $usrID, $password ) {
		
		if ( $usrID == $this->user[ 'usrID' ] && $password == $this->user[ 'password' ] ) {
			//echo $this->user;
			// Add user usrID and id to empty usrID and id variable and return true
			//$this->id = $this->user[ 'id' ];

			$this->usrID = $this->user[ 'usrID' ];

			return true;
		} else {

			return false;
		}
	}*/
	// Generates and signs a JWT for User
	private
	function genJWT() {
		// Make an array for the JWT Payload
		$payload = array(
			//"id" => $this->id,
			"data" => $this->uData,
			"exp" => time() + ( 60 * 60 * 24 * 60 ) //un mes 2592000 segundos
		);

		// encode the payload using our secretkey and return the token
		return JWT::encode( $payload, $this->key );
	}
	// sends signed token in usrID to user if the user exists
	public
	function getUserToken( $arrPrivData ) {
		//echo $usrID . " - " . $password;
		//echo "te entró";
		//if ( $this->validUser( $usrID, $password ) ) {
		//echo "te entró";
		// $this->usrID = $usrID;
		$this->uData = $arrPrivData;
		$token = $this->genJWT();

		return $token;

		//	} else {

		//		return "notoken";
		//	}

	}
	// Validates a given JWT from the user usrID
	private	function validJWT( $token ) {
		$res = array( false, '' );
		// using a try and catch to verify
		try {
			//$decoded = JWT::decode($token, $this->key, array('HS256'));
			$decoded = JWT::decode( $token, $this->key, array( 'HS256' ) );
		} catch ( Exception $e ) {
			return $res;
		}
		$res[ '0' ] = true;
		$res[ '1' ] = ( array )$decoded;

		return $res;
	}


	public	function validUID( $token ) {
		// checks if an usrID is valid
		$tokenVal = $this->validJWT( $token );
		//	print_r($tokenVal);
		// check if the first array value is true
		if ( $tokenVal[ '0' ] ) {
			// create user session and all that good stuff
			return "true";
		} else {
			return "false";
		}
	}

	public 	function getUsrObj( $token ) {
		//echo $usrID . " - " . $password;
		//echo "te entró";
		//if ( $this->validUser( $usrID, $password ) ) {
		//echo "te entró";
		// $this->usrID = $usrID;
		$decoded = JWT::decode( $token, $this->key, array( 'HS256' ) );
		//$this->uData = $arrPrivData;
		//$token = $this->genJWT();

		return $decoded;

		//	} else {

		//		return "notoken";
		//	}

	}

}

?>