<?php 
define('PW_AUTH', 'erXC6OicwB9ecvjdax0LMKTECxtMQ0qNfHpnG5R0JJdzsVjh0mbMhqA9hO31LsUc4uRUluJntqSAI71KAyN2');
define('PW_APPLICATION', '73584-6447D');
/*define('PW_AUTH', 'z0PUKSCb0xuxdxjCy9Uf28UNH7EMZNhYUhCB4hw4EErLKGEtKlEPEpNkElnryU8dcH6tdfhO1FKOlZriq0mN');
define('PW_APPLICATION', '7733F-12AF2');*/
define('PW_DEBUG', false);
 
//$devices[] = '';
function pwCall($method, $data) {
	$url     = 'https://cp.pushwoosh.com/json/1.3/' . $method;
	$data1   = array('request' => $data);
	$request = json_encode($data1);
	
 
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
 
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
 
    if (defined('PW_DEBUG') && PW_DEBUG) {
      print "[PW] request: $request\n";
      print "[PW] response: $response\n";
      print '[PW] info: ' . print_r($info, false);
    }
}
 //array_push($devices, 'd8df24cb20febb3f');
 //array_push($devices, '4bc23e1bd4676ba4');
 //array_push($devices, 'F26CEA52-7C6C-4416-9F0C-D10BEEB9A976');

function sendPushOnSimpleInteraction($oName, $dUUID, $pushType, $UserAvatar, $idPost, $comment) {
	$devs = array();
	//echo "ddd ->".$dUUID;
//	echo "funcion=" . $oName ." ". $dUUID;
//	array_push($devs, 'd8df24cb20febb3f');
	array_push($devs, $dUUID );
	
	if ($pushType=="like"){
		//$pushTitle = "Like"
		$pushContentES = '👍 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' liked a post from you.';
     //   $pushContentEN = '👍 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' like your post.';
	}
	elseif($pushType=="comment"){
		//$pushTitle = "Nuevo comen"
		$pushContentES = '💬 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' commented: "'.$comment.'" in a post from you.';
     //   $pushContentEN = '💬 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' commented on your post.';
	}
	
//	print_r($devs);
	pwCall('createMessage', array(
		'application' => PW_APPLICATION,
		'auth' => PW_AUTH,
		'notifications' => array(
				array(
					'send_date' => 'now',
					//'content' =>  $pushContent,
                    "content" => $pushContentES,
					'devices' => $devs,
					
				//IOS	
					'ios_sound' =>'www/res/like.mp3',
					'ios_badges' => +1,
					"ios_subtitle" => "Community",
					
				
				//ANDROID
					'android_header' => "Community",
					'android_badges' => +1,
					'android_sound' =>'like',
					'android_custom_icon' => $UserAvatar,
                    "android_led"    => "#FF0000",
					
				//	'rich_media' => 'A0221-4ACB6',
					'data' => array(
                        'page' => 'community',
                        'post' => $idPost
                    )					
				//	'platforms'=>array(3)
				)
			)
		)

	);
}

function sendPushOnOtherComments($oName, $devs, $UserAvatar, $idPost, $comment ) {
	
//	$devs = array();
	//echo "ddd ->".$dUUID;
//	echo "funcion=" . $oName ." ". $dUUID;
//	array_push($devs, 'd8df24cb20febb3f');
	//array_push($devs, $dUUID );
	//print_r($devs);
	
	$pushTitle = "Community";
	
		//$pushTitle = "Nuevo comen"
	$pushContentES = '💬 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' wrote: "'.$comment.'" in the same Post that you commented.';
   // $pushContentEN = '💬 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' also commented on the same post as you.';

	//echo $pushContent;

	pwCall('createMessage', array(
		'application' => PW_APPLICATION,
		'auth' => PW_AUTH,
		'notifications' => array(
				array(
					'send_date' => 'now',
					//'content' =>  $pushContent,
                    "content" => $pushContentES,
					'devices' => $devs,
					
				//IOS	
					'ios_sound' =>'www/res/like.mp3',
					'ios_badges' => +1,
					"ios_subtitle" => $pushTitle,
					
				
				//ANDROID
					'android_header' => $pushTitle,
					'android_badges' => +1,
					'android_sound' =>'like',
					'android_custom_icon' => $UserAvatar,
                    "android_led"    => "#FF0000",
					
				//	'rich_media' => 'A0221-4ACB6',
					'data' => array(
                        'page' => 'community',
                        'post' => $idPost
                    )					
				//	'platforms'=>array(3)
				)
			)
		)

	);
}

function sendHBdayPush($they, $hbdaysUsers ) {
	//echo $msgPush;
	$devs = array();
	//echo "ddd ->".$dUUID;
//	echo "funcion=" . $oName ." ". $dUUID;
	array_push($devs, '4bc23e1bd4676ba4');
	//array_push($devs, $dUUID );
	//print_r($devs);
	
	$pushTitle = $they;
	
		//$pushTitle = "Nuevo comen"
	$pushContentES = "🎂 " . $hbdaysUsers . ", expresa tus buenos deseos en la Comunidad";
   // $pushContentEN = "🎂 " . $hbdaysUsers . ", expresses your good wishes in the Community";

	//echo $pushContent;
    $banner = "http://app.mydigital.cards/srvs/media/images/community/Birthday-Cake.jpg";
	pwCall('createMessage', array(
		'application' => PW_APPLICATION, 
		'auth' => PW_AUTH,
		'notifications' => array(
				array(
					'send_date' => 'now', 
					//'content' =>  $pushContent,
                     "content" => $pushContentES,
					//'devices' => $devs, 
                    'wns_ttl' => 60 * 60 * 12,
					
				//IOS	
					'ios_sound' =>'www/res/hbday.mp3',
					'ios_badges' => +1,
					"ios_subtitle" => $pushTitle,
                    "ios_root_params" => [  // Optional � root level parameters to the aps dictionary
					  "aps" => [
						"content-available"=> "1",
						"mutable-content"=>1 //required for iOS 10 Media attachments
							],
					  "attachment"=>$banner  // iOS 10 media attachment URL
					],
					
				
				//ANDROID
					'android_header' => $pushTitle,
					'android_badges' => +1,
					'android_sound' =>'hbday',
                    'android_banner' => $banner ,
                    "android_led"    => "#FF0000",
					//'android_custom_icon' => $UserAvatar,
					
				//	'rich_media' => 'A0221-4ACB6',
					'data' => array(
                        'page' => 'community',
                        'post' => 0
                    )					
				//	'platforms'=>array(3)
				)
			)
		)

	);
}
       //sendPushToAll($userName, $devs ,$taComment ,$UserAvatar,$banner,$lPostId);
function sendPushToAll($oName, $devs, $taPost, $UserAvatar, $banner, $idPost ) { 

	$pushTitle = "Message to all";
	
	$pushContentES = '📢 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' wrote: ' . $taPost;
//	$pushContentEN = '📢 ' . mb_convert_case( trim( $oName ), MB_CASE_TITLE, "UTF-8") .' wrote: ' . $taPost;
	
//	print_r($devs);
	pwCall('createMessage', array(
		'application' => PW_APPLICATION, 
		'auth' => PW_AUTH,
		'notifications' => array(
				array(
					'send_date' => 'now',
					 "content" => $pushContentES,
					'devices' => $devs,
					
				//IOS	
					'ios_sound' =>'www/res/like.mp3',
					'ios_badges' => +1,
					"ios_subtitle" => $pushTitle,
					 "ios_root_params" => [  // Optional � root level parameters to the aps dictionary
					  "aps" => [
						"content-available"=> "1",
						"mutable-content"=>1 //required for iOS 10 Media attachments
							],
					  "attachment"=>$banner  // iOS 10 media attachment URL
					],
					
				
				//ANDROID
					'android_header' => $pushTitle,
					'android_badges' => +1,
					'android_sound' =>'like',
					'android_custom_icon' => $UserAvatar,
					'android_banner' => $banner ,
					"android_led"    => "#FF0000", 
				//	'rich_media' => 'A0221-4ACB6',
					'data' => array(
                        'page' => 'community',
                        'post' => $idPost
                    )					
				//	'platforms'=>array(3)
				)
			)
		)

	);
}
/*
  function Sustituto_Cadena($rb){ 
        ## Sustituyo caracteres en la cadena final
        $rb = str_replace("á", "&aacute;", $rb);
        $rb = str_replace("é", "&eacute;", $rb);
        $rb = str_replace("®", "&reg;", $rb);
        $rb = str_replace("í", "&iacute;", $rb);
        $rb = str_replace("�", "&iacute;", $rb);
        $rb = str_replace("ó", "&oacute;", $rb);
        $rb = str_replace("ú", "&uacute;", $rb);
        $rb = str_replace("n~", "&ntilde;", $rb);
        $rb = str_replace("º", "&ordm;", $rb);
        $rb = str_replace("ª", "&ordf;", $rb);
        $rb = str_replace("Ã¡", "&aacute;", $rb);
        $rb = str_replace("ñ", "&ntilde;", $rb);
        $rb = str_replace("Ñ", "&Ntilde;", $rb);
        $rb = str_replace("Ã±", "&ntilde;", $rb);
        $rb = str_replace("n~", "&ntilde;", $rb);
        $rb = str_replace("Ú", "&Uacute;", $rb);
        return $rb;
    }  
*/
?>
