<?
header( "Access-Control-Allow-Origin: *" );
require( 'include/mysql_class.php' );

$response = array( "result" => false );
//data: {"token":token,"idPost":queryPage.idPost,"uID":uID} ,
$token 		= filter_var( $_REQUEST[ "token" ],FILTER_SANITIZE_STRING );
$idPost 	= filter_var( $_REQUEST[ "idPost" ],FILTER_SANITIZE_STRING );
//$uID 		= filter_var( $_REQUEST[ "uID" ] ,FILTER_SANITIZE_STRING);
$taComment 	= filter_var( $_REQUEST[ "taComment" ],FILTER_SANITIZE_STRING );

$taComment = trim($taComment);

require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {

	$objU      = $auth->getUsrObj( $token )->data; 
	$uID       = $objU->uID;
	
	/*$sqlComment = " INSERT INTO `communityComments` (`isUser`,`idCommunity`,`communityComments`) VALUES ('$uID','$idPost','$taComment') ; ";*/

	$timestamp = date( 'Y-m-d H:i:s' );
	$sqlComment = "INSERT INTO `it_ratings` (`item_type`,`item_id`, `it_users_id`, `type`, `date`,`comment` ) VALUES ('P','$idPost', '$uID', 'C', '$timestamp','$taComment');";
	//echo $sqlLike;
	$micon->query( $sqlComment);
	//$dtaScheduleDetDetail = $micon->campoconsultaA();	
    
    $sqlUInfo = " SELECT name, avatar FROM `it_users` WHERE `it_users`.id = '$uID'  ";
    $micon->query( $sqlUInfo);
    $dtaUInfo  = $micon->fetchArray();
   // $fullNameArr = explode(" ",$dtaUInfo[userName]);
    $userName   = $dtaUInfo[name];
    $UserAvatar = ($dtaUInfo[avatar]!="nouser.svg") ? URL . "media/images/profiles/" . $dtaUInfo[ avatar ] : URL . "media/images/" . $dtaUInfo[ avatar ] ;
	
	//recolect data for push
	$sqlPush = " SELECT `name`, `avatar`,
	`it_users`.`id`, 
	(SELECT `uuid` FROM `it_users` INNER JOIN `it_posts` ON `it_posts`.`it_users_id` = `it_users`.`id` WHERE `it_posts`.`id`='$idPost') AS  OwnerUUID,
	(SELECT `it_users`.`id` FROM `it_users` INNER JOIN `it_posts` ON `it_posts`.`it_users_id` = it_users.`id` WHERE `it_posts`.`id`='$idPost') AS  postOwner 
	FROM
		`it_users`
	   WHERE it_users.`id` ='$uID' 
	   HAVING id  <> postOwner;   ";
	
	require_once( 'sendPush.php' );
	$micon->query( $sqlPush );
	$cntPush = $micon->numRows();
	//echo "cntPush "  . $cntPush;
	//$dtaPush = $micon->campoconsultaA();
	//$postOwner = $dtaPush[postOwner];
	if($cntPush > 0)	{

		$dtaPush = $micon->fetchArray();
		$postOwner = $dtaPush[postOwner];
		
		
		
		$hid  = $dtaPush[OwnerUUID]; 
		
		$UserAvatar = ($dtaPush[avatar]!="nouser.svg") ? URL . "media/images/profiles/" . $dtaPush[ avatar ] : URL . "media/images/" . $dtaPush[ avatar ] ;
		//($arrUserApp[ UserMobile ]!="") ? $arrUserApp[ UserMobile ] : ""
		if ($hid!=""){
			sendPushOnSimpleInteraction($userName , $hid, "comment", $UserAvatar,$idPost,$taComment);
		}
		
	}
	
	
	
	
	//recolect data for push
	$sqlPush = " SELECT `it_users`.`id`,`uuid`
FROM
    `it_users`
    INNER JOIN `it_ratings`
        ON (`it_users`.`id` = `it_ratings`.`it_users_id`)
  WHERE `it_users`.id <> '$uID' AND `it_ratings`.`it_users_id`<>'$postOwner' AND `it_ratings`.`item_id`='$idPost' AND `it_ratings`.`type`='C' AND `it_ratings`.`item_type`='P'  AND `uuid` IS NOT NULL AND `uuid`  <> ''
  GROUP BY `it_users`.id ;  ";
	//echo $sqlPush;
	$micon->query( $sqlPush );
	$cntPush = $micon->numRows();
	if($cntPush > 0)	{
		/*
		$sqlUsr = "  SELECT UserName FROM `USER` WHERE UserLogin ='$uID'  ";
		$micon2->consulta( $sqlUsr );
		$dtaUsr = $micon2->campoconsultaA();
		$userName = $dtaUsr[UserName];*/
		
					
		$devs = array();
		while ($dtaPush = $micon->fetchArray()){

			$hid  = $dtaPush[uuid];
			array_push($devs, $hid );
		}
		//print_r($devs);
		sendPushOnOtherComments($userName, $devs ,$UserAvatar,$idPost, $taComment );
		
	}
	
	
	$response = array(
		"result" => true
	);

}

echo json_encode( $response );
?>