<?
header( "Access-Control-Allow-Origin: *" );
require( 'include/mysql_class.php' );
require( 'include/utils.php' );

$response = array( "result" => false ); 

$token 			= filter_var( $_REQUEST[ "token" ] ,FILTER_SANITIZE_STRING);
$gmt 			= filter_var( $_REQUEST[ "gmt" ],FILTER_SANITIZE_STRING );
$idPost 		= filter_var( $_REQUEST[ "idPost" ] ,FILTER_SANITIZE_STRING);


require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {

	$objU      = $auth->getUsrObj( $token )->data;
	$uID       = $objU->uID;

	/*$sqlHits = "update `communityPosts` set hits = hits+1 where `idCommunity`='$idPost'";
	$micon->consulta( $sqlHits );*/ 
	
	//$sqlComments = " SELECT * FROM `places` ORDER BY placeName asc ";
	$sqlComments = " SELECT *,it_ratings.`id` AS idCommunityComments,  
`it_users`.`id` as idUser,
CONVERT_TZ(`it_ratings`.`date`,'-05:00','$gmt') AS commentDate
FROM
   `it_ratings`
    INNER JOIN `it_users`
        ON (`it_ratings`.`it_users_id` = `it_users`.`id`)
     WHERE `it_ratings`.`item_id`='$idPost' AND `item_type`='P' AND `type`='C'
        ORDER BY `date` ASC;   ";
	//echo $sqlComments;
	$micon->query( $sqlComments );
	//$dataArr = array();
	$dataComments = array(); 
	//$today = date("Y-m-d");//2017-10-17
	//$today = "2017-10-19";
	$qtyComms = false;
	while ( $dtaCommunity = $micon->fetchArray() ) {
		$qtyComms = true;
		$objComments				= new stdClass();
		$objComments->Id 			= $dtaCommunity[ idCommunityComments ];
        $objComments->uId 			= $dtaCommunity[ idUser ];
		$objComments->Comment		= $dtaCommunity[ comment ]; 
		//$objComments->Pic			= ( $dtaCommunity[ communityPic ] != "" ) ? $dtaCommunity[ communityPic ] : "";
		$objComments->uAvatar		= ( $dtaCommunity[ avatar] != "nouser.svg" ) ? URL . "media/images/profiles/" . $dtaCommunity[ avatar ] : "img/nouser.svg"  ;
		$objComments->uName			= mb_convert_case( rtrim( $dtaCommunity[ name ] ), MB_CASE_TITLE, "UTF-8") . " " . mb_convert_case( trim( $dtaCommunity[ last_name ] ), MB_CASE_TITLE, "UTF-8");
	//	$objComments->Owner		= $dtaCommunity[ userName ] ;
		//$objComments->Content		= $dtaCommunity[ contentNews ];
		//$objComments->Date 			= friendlyDate($dtaCommunity[ communityCommentsTimeStamp ],true,true,true);	
		$objComments->TimeAgo 		= time_ago($dtaCommunity[ commentDate ]);	
		
		$objComments->Sent  = false;
		if ($dtaCommunity[ idUser ] == $uID){
            $objComments->Sent  = true;
        }

		
	
		//array_push( $dataArr, $objComments );		
		array_push( $dataComments, $objComments );
		//objComments" => [ "region" => $dataArr ],		
	}
	
	
	
	

	$response = array(
		"result" 				 =>  true ,
		"objPostComments" 		 =>  $dataComments ,
		"hasComments" 			 =>  $qtyComms ,
        
	);

}

echo json_encode( $response );
?>