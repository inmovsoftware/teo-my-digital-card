<?
require( 'include/mysql_class.php' );

$response = array( "result" => false );

//$uID 		= mysql_real_escape_string( $_REQUEST[ "uID" ] );
$token 		= filter_var( $_REQUEST[ "token" ] ,FILTER_SANITIZE_STRING);
$plat 		= $_REQUEST[ "plat" ];
$appVersion = $_REQUEST[ "appVersion" ];
$model 		= $_REQUEST[ "model" ];
$uuid       = $_REQUEST[ "uuid" ];
$ver        = $_REQUEST[ "ver" ]; 

require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {
	$objU      = $auth->getUsrObj( $token )->data;  
	$uID       = $objU->uID;
	//print_r($objU);

	$sql = "SELECT
    *
FROM
    VS_USERLOGIN
WHERE `id`='$uID' and `status` ='A' ";
	$micon->query( $sql );
	//echo $sql;
	//echo $micon->numFields();
	if ( $micon->numRows() ) {

		$timeStamp = date( "Y-m-d H:i:s" );
		$sql = " UPDATE `it_users` SET `last_open` = '" . $timeStamp . "', model='" . $model . "', app_version='" . $appVersion . "',platform='" . $plat . "', version='" . $ver . "', uuid='" . $uuid . "' WHERE `id`='$uID' ";
		$miconInset->query( $sql );

		$sql = "SELECT extra, link_update,notes_release FROM `it_appinfo` where type ='" . $plat . "'";
		$miconInset->query( $sql );
		$DtaVersion = $miconInset->fetchArray();
		$cur = ( str_replace( ".", "", $appVersion ) );
		$last = ( str_replace( ".", "", $DtaVersion[ extra ] ) );
		$outdated = false;

		if ( $last > $cur and $plat!="Web") {
			$outdated = true;
		}

		$arrCustomer    = $micon->fetchArray();
		//$_GET["action"] = "genToken"; 	
		$arrPrivData    = array(
			"uID"               => $arrCustomer[ id ],
			"uName"             => $arrCustomer[ name ] ,
			"uLName"            => $arrCustomer[ last_name ] ,
			"uMobile"           => $arrCustomer[ phone ],
			"uPrefix"           => ($arrCustomer[ country_phone ] != "") ? $arrCustomer[ country_phone ]  : "1",
			"uMail"             => $arrCustomer[ email ],
			
			"uPosition"         => $arrCustomer[ it_positions_id ],
			"canChangePosition" => ($arrChangePosition[value] === 'true') ? true : false ,
			"companyId" 		=> $arrCustomer[companyId],
			"branchId" 			=> $arrCustomer[it_branches_id],
			"is_partner" 		=> ($arrCustomer[is_partner] == "true") ? true : false,

		);



		//require_once('handler.php'); 
		$tokenResponse = $auth->getUserToken( $arrPrivData );
		$response = array(
			"result"               => true,
			"outdated"             => $outdated,
			"urlUpdate"            => $DtaVersion[ link_update ],
			"curVersion"           => $DtaVersion[ extra ],
			"appVersion"           => $appVersion,
			"sType"                => $arrCustomer[ session_type ], 
			"notesAboutNewVersion" => ($DtaVersion[ notes_release ] != "") ? "<br><br>" . $DtaVersion[ notes_release ] : "" ,
			"token"                => $tokenResponse

		);
		// $response 	= array( "result" => true );
	}


}

echo json_encode( $response );
?>