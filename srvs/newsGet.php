<?
require( 'include/mysql_class.php' );
require( 'include/utils.php' );


$response = array( "result" => false );

$token    = filter_var(  $_GET[ "token" ] ,FILTER_SANITIZE_STRING); 
$gmt 	  = filter_var( $_REQUEST[ "gmt" ] ,FILTER_SANITIZE_STRING );
$catShow  = filter_var(  $_GET[ "catShow" ] ,FILTER_VALIDATE_INT);  

require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {

	$objU      = $auth->getUsrObj( $token )->data;
	$uID       = $objU->uID;
	$uPosition = $objU->uPosition;
	$companyId = $objU->companyId;
	$branchId  = $objU->branchId;

	/////GET USER AND COMPANY PROPERTIES 
	$sqlUsrCompany = " SELECT
	    `it_business`.`is_partner`
	    , `it_business`.`id` as bID
	    , `it_business`.`name`
	FROM
	    `it_branches`
	    INNER JOIN `it_business` 
	        ON (`it_branches`.`it_business_id` = `it_business`.`id`)
	    INNER JOIN `it_users` 
	        ON (`it_users`.`it_branches_id` = `it_branches`.`id`)
	WHERE (`it_users`.`id` =$uID);";

	//echo 	$sqlUsrCompany;

	$micon->query( $sqlUsrCompany );
	$arrCustomer 	= $micon->fetchArray();


	$clauseForPartner = "";
	if ($arrCustomer[is_partner] == "false"){

		$clauseForPartner = " and (it_news.`it_categories_news_id` <> 1 ) ";
	}

/*
#NOT SHOW NEWS FOR TEO / JUST FOR PARTNERS
	#and (it_news.`it_categories_news_id` <> 1 )
*/
	$sqlCatList = "	 

		SELECT
	    `it_categories_news`.`name`  AS NewsCateName
	    , `it_categories_news`.`color`  AS NewsCateColor
	    , `it_categories_news`.`id`  AS NewsCateId
	   ,COUNT(`it_news`.`id`) qtyNews
	    
	    , (SELECT GROUP_CONCAT(it_new_position.it_position_id) FROM `it_new_position` WHERE `it_position_id`=$uPosition AND it_new_position.`it_news_id` = it_news.`id` )  forPosition
	    , (SELECT GROUP_CONCAT(it_new_position.it_position_id) FROM `it_new_position` WHERE it_new_position.`it_news_id` = it_news.`id`) AS allPos
	    
	    , (SELECT GROUP_CONCAT(`it_new_branch`.`it_branch_id`) FROM `it_new_branch`   WHERE `it_new_branch`.`it_branch_id`=$branchId AND `it_new_branch`.`it_news_id` = it_news.`id` ) AS forBranch
	    , (SELECT GROUP_CONCAT(`it_new_branch`.it_branch_id) FROM `it_new_branch`   WHERE  `it_new_branch`.`it_news_id` = it_news.`id` ) AS allBranches
	    
	    , (SELECT GROUP_CONCAT(it_user_group.`it_groups_users_id`) FROM `it_user_group`  WHERE `it_user_group`.`it_users_id`=$uID     ) AS myGroup   
	    ,(SELECT GROUP_CONCAT(it_new_groupuser.`it_groups_users_id`) FROM `it_new_groupuser`  WHERE `it_new_groupuser`.`it_news_id`=it_news.`id`   ) AS forGroups 	   

	FROM
	    `it_news`
	    INNER JOIN `it_categories_news` 
	        ON (`it_news`.`it_categories_news_id` = `it_categories_news`.`id`)
	WHERE (`it_categories_news`.`status` = 'A' AND `it_news`.`status`='A' )
	AND CONVERT_TZ(publication_date,'-05:00','$gmt') <= NOW() AND ( CONVERT_TZ(close_date >= NOW(),'-05:00','$gmt') OR ISNULL(close_date) OR close_date='' )
	
	$clauseForPartner 

	AND (`id_business_id` = '$companyId' OR `it_categories_news`.`id` = 1)
	GROUP BY `it_categories_news_id` 
	HAVING qtyNews > 0 	
	AND (( (ISNULL(allPos))  AND (ISNULL(allBranches))  AND (ISNULL(forGroups)) )  OR ((FIND_IN_SET( forPosition, allPos )) OR  (FIND_IN_SET( forBranch, allBranches ))  OR (FIND_IN_SET( myGroup, forGroups ))  ))
	ORDER BY `it_categories_news`.`name` ASC 
";
	
	//echo $sqlCatList;
	$micon->query( $sqlCatList ); 
//	$arrNewsForPartner 	= $micon->fetchArray();



	//$micon2->consulta( $sqlCats );
	$dataCatNews = array();
	while ( $dtaCatsNews = $micon->fetchArray() ) {
		$objCatsNews				= new stdClass();
		$objCatsNews->Id 			= $dtaCatsNews[ NewsCateId ];
		$objCatsNews->cName 		= $dtaCatsNews[ NewsCateName ];		 
		$objCatsNews->cColor 		= "#" .$dtaCatsNews[ NewsCateColor ];
		$objCatsNews->qtyNews		= $dtaCatsNews[ qtyNews ];
		
		array_push( $dataCatNews, $objCatsNews );
	}
	
	//$sqlNews = " SELECT * FROM `places` ORDER BY placeName asc ";
	$clauseForCat ="";
	if ($catShow > 0){
		$clauseForCat = " and `it_categories_news`.`id`='$catShow' ";
	}
	$sqlNews = " SELECT
	    `it_categories_news`.`name`  as NewsCateName
	   , `it_categories_news`.`color` as NewsCateColor
	   , `it_news`.`id` as idNews
	   , `it_news`.`title`
	   , `it_news`.`intro`
	   , `it_news`.`photo`
	   , CONVERT_TZ(`it_news`.creation_date,'-05:00','$gmt') as newsCreationDate
	   , (SELECT COUNT(*) FROM `it_ratings` WHERE `item_id`=`it_news`.`id` AND `type`='L' AND `item_type`='N' ) AS likes
	   , (SELECT `id` FROM `it_ratings` WHERE `item_id`=`it_news`.`id` AND `it_users_id`='$uID' AND `type`='L' AND `item_type`='N' ) AS me
	   , (SELECT COUNT(*) FROM `it_ratings` WHERE `item_id`=`it_news`.`id` AND `type`='R'  AND `item_type`='N' ) AS hits
	, (SELECT GROUP_CONCAT(it_new_position.it_position_id) FROM `it_new_position` WHERE `it_position_id`=$uPosition AND it_new_position.`it_news_id` = it_news.`id` )  forPosition
	    , (SELECT GROUP_CONCAT(it_new_position.it_position_id) FROM `it_new_position` WHERE it_new_position.`it_news_id` = it_news.`id`) AS allPos
	    
	    , (SELECT GROUP_CONCAT(`it_new_branch`.`it_branch_id`) FROM `it_new_branch`   WHERE `it_new_branch`.`it_branch_id`=$branchId AND `it_new_branch`.`it_news_id` = it_news.`id` ) AS forBranch
	    , (SELECT GROUP_CONCAT(`it_new_branch`.it_branch_id) FROM `it_new_branch`   WHERE  `it_new_branch`.`it_news_id` = it_news.`id` ) AS allBranches
	    
	    , (SELECT GROUP_CONCAT(it_user_group.`it_groups_users_id`) FROM `it_user_group`  WHERE `it_user_group`.`it_users_id`=$uID     ) AS myGroup   
	    ,(SELECT GROUP_CONCAT(it_new_groupuser.`it_groups_users_id`) FROM `it_new_groupuser`  WHERE `it_new_groupuser`.`it_news_id`=it_news.`id`   ) AS forGroups 	   

	FROM
	    `it_news`
	    INNER JOIN `it_categories_news` 
	        ON (`it_news`.`it_categories_news_id` = `it_categories_news`.`id`)
	WHERE (`it_categories_news`.`status` = 'A' AND `it_news`.`status`='A' )
	AND CONVERT_TZ(publication_date,'-05:00','$gmt') <= NOW() AND ( CONVERT_TZ(close_date >= NOW(),'-05:00','$gmt') OR ISNULL(close_date) OR close_date='' )


	$clauseForCat

	AND (`id_business_id` = '$companyId ' or `it_categories_news`.`id` = 1)
	
	$clauseForPartner 

	
	HAVING  (( (ISNULL(allPos))  AND (ISNULL(allBranches))  AND (ISNULL(forGroups)) )  OR ((FIND_IN_SET( forPosition, allPos )) OR  (FIND_IN_SET( forBranch, allBranches ))  OR (FIND_IN_SET( myGroup, forGroups ))  ))
	ORDER BY `it_news`.`creation_date` desc  limit 10";
	//echo $sqlNews;
	$micon->query( $sqlNews );
	//$dataArr = array();
	$dataNews   = array();
	//$today    = date("Y-m-d");//2017-10-17
	//$today    = "2017-10-19";
	$hasContent = false;
	$iContent   = 0;
	while ( $dtaNews = $micon->fetchArray() ) {
		//$hasContent = true;
		$iContent++;
		$objNews				= new stdClass();
		$objNews->Id 			= $dtaNews[ idNews ];
		$objNews->Title			= $dtaNews[ title ];
		$objNews->Intro			= $dtaNews[ intro ];
		//$objNews->Content		= $dtaNews[ contentNews ];
		$objNews->Date 			= friendlyDate($dtaNews[ newsCreationDate ],true,true,true);	
		$objNews->Img 			= ( $dtaNews[ photo ] != "" ) ? URL . "/media/images/news/" . $dtaNews[ photo ] : "";
		$objNews->Views 		= ( $dtaNews[ hits ] != "" ) ? $dtaNews[ hits ] : "0";
		$objNews->Likes 		= ( $dtaNews[ likes ] != "" ) ? $dtaNews[ likes ] : "0";
		$objNews->Category 		=  $dtaNews[ NewsCateName ];
		$objNews->CateColor 	=  "#" . $dtaNews[ NewsCateColor ]; 
		
		$objNews->Liked 	= false;
		if ($dtaNews[ me ] != ""){
			$objNews->Liked = true;
		}
		/*
		$objNews->hasGallery 	= false;
		if ($dtaNews[ idGallery ] != ""){
			$objNews->hasGallery = true;
		}*/
		
	
		//array_push( $dataArr, $objNews );		
		array_push( $dataNews, $objNews );
		//objNews" => [ "region" => $dataArr ],		
	}
	
	
	if ($iContent>=1){
		$hasContent = true;
	}
	

	$response = array(
		"result"      =>  true ,
		"objNews"     =>  $dataNews ,
		"objCatsNews" =>  $dataCatNews  ,
		"hasContent"  => $hasContent,
	);

}

echo json_encode( $response );
?>