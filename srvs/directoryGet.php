<?
header( "Access-Control-Allow-Origin: *" );
require( 'include/mysql_class.php' );


$response = array( "result" => false );
$token    = filter_var( $_REQUEST[ "token" ], FILTER_SANITIZE_STRING );
$dType    = filter_var( $_REQUEST[ "dType" ], FILTER_SANITIZE_STRING );

require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {

	$objU      = $auth->getUsrObj( $token )->data;
	$uID       = $objU->uID;
	$companyId = $objU->companyId;
	
	if ($dType != "L"){
		$clauseDirectory = " AND `it_business`.`id` = '$companyId' ";
		if ($dType == "P"){
			$clauseDirectory = " AND  `it_business`.is_partner='true' ";
		}

		$sqlDirectory = " SELECT
			 `it_users`.`id` AS uId
		    , `it_users`.`name` as fName
		    , `it_users`.`last_name` as lName
		    , `it_users`.`avatar`
		    , `it_users`.`country_phone`
		    , `it_users`.`phone`
		    , `it_users`.`email`
		    , `it_business`.`id` AS bsId
		    , `it_branches`.`id` AS brId  
		    , `it_business`.`name` AS bsName  
		    , `it_branches`.`name` AS brName  
		    , `it_positions`.`name` as pName
		    
		FROM
		    `it_branches`
		    INNER JOIN `it_business` 
		        ON (`it_branches`.`it_business_id` = `it_business`.`id`)
		    INNER JOIN `it_users` 
		        ON (`it_users`.`it_branches_id` = `it_branches`.`id`)
		    left JOIN `it_groups_users` 
		        ON (`it_groups_users`.`it_business_id` = `it_business`.`id` and `it_groups_users`.`show_in_directory` ='yes' )
		    INNER JOIN `it_positions` 
		        ON (`it_positions`.`it_business_id` = `it_business`.`id`) AND (`it_users`.`it_positions_id` = `it_positions`.`id`)
		WHERE `it_users`.`status` ='A'
		    $clauseDirectory
		    GROUP BY `it_users`.`id`
		    ORDER BY `it_users`.`name` "; 
		   
	}
	else{
			$sqlDirectory = " SELECT
		    `it_leads`.`id` as uId
		    , `it_leads`.`name` as fName
		    , `it_leads`.`position` as pName
		    , `it_leads`.`business` AS bsName
		    , `it_leads`.`country_phone`
		    , `it_leads`.`phone`
		    , `it_leads`.`email`
		FROM
		    `it_leads`
		    INNER JOIN `it_users` 
		        ON (`it_leads`.`it_users_id` = `it_users`.`id`)
		WHERE (`it_leads`.`it_users_id` = $uID)  AND `it_leads`.`status` ='A'
		ORDER BY  `it_leads`.`name` ";
		
	}


	
	$micon->query( $sqlDirectory ); 

	//echo $sqlDirectory ;
	$dataDirectory = array();
	$hasPeople = false;
	while ( $dtaDirectory = $micon->fetchArray() ) {
			$hasPeople = true;
			$objDirectory           = new stdClass();
			$objDirectory->Id       = $dtaDirectory[ uId ];
			$objDirectory->Name     = mb_convert_case($dtaDirectory[ fName ], MB_CASE_TITLE, "UTF-8") ;
			$objDirectory->LName    = ( $dtaDirectory[ lName] != "" ) ?  mb_convert_case($dtaDirectory[ lName ], MB_CASE_TITLE, "UTF-8") : "";
			$objDirectory->Position = ( $dtaDirectory[ pName] != "" ) ? $dtaDirectory[ pName ] : "";	 
			$objDirectory->Email    = ( $dtaDirectory[ email] != "" ) ? $dtaDirectory[ email ] : "";	 
			$objDirectory->PxPhone  = ( $dtaDirectory[ country_phone] != "" ) ? str_replace("+","",$dtaDirectory[ country_phone ])  : "";	 
			$objDirectory->Phone    = ( $dtaDirectory[ phone] != "" ) ? $dtaDirectory[ phone ]  : "";	 


			$objDirectory->Avatar   = "nouser.svg";
			
			$hasAvatar = false;
			if ($dType != "L"){
				$hasAvatar = true;

				$objDirectory->Avatar   = ( $dtaDirectory[ avatar] != "nouser.svg" ) ? URL . "/media/images/profiles/" . $dtaDirectory[ avatar ] : "nouser.svg" ;


				//if ($dType == "P"){
				//	$objDirectory->bsName   = ( $dtaDirectory[ bsName ]!="" ) ?  $dtaDirectory[ bsName ]  : "";	
				
				//} 
				//if ($dType == "C"){ 
					$objDirectory->bsName   = ( $dtaDirectory[ brName ]!="" ) ?  $dtaDirectory[ bsName ]  . " · " . $dtaDirectory[ brName ]  : "";	
					if ( $uID == 2){
						//$objDirectory->bsName  = "";
					}
					
				//}

			}
			else{
				$objDirectory->bsName   = ( $dtaDirectory[ bsName ]!="" ) ?  $dtaDirectory[ bsName ]  : "";	
			}
			$objDirectory->hasAvatar = $hasAvatar;	
				
		array_push( $dataDirectory, $objDirectory );
			
	}

	$tr = false;
	if ( $uID == 2){
		//$tr = true;
	}

	$response = array(
		"result"       =>  true ,
		"objDirectory" =>  $dataDirectory ,
		"dType"        =>  $dType , 
		"hasPeople"    =>  $hasPeople ,
	
		"triquinuela"  =>  $tr
	);

}
echo json_encode( $response );
?>