<?
header( "Access-Control-Allow-Origin: *" );
require( 'include/mysql_class.php' );
require( 'include/utils.php' );


$response = array( "result" => false );
//data: {"token":token,"idNews":queryPage.idNews,"uID":uID} ,
//filter_var(  $_GET[ "token" ] ,FILTER_SANITIZE_STRING); 
$token 		= filter_var( $_REQUEST[ "token" ]  ,FILTER_SANITIZE_STRING);
$idNews 	= filter_var( $_REQUEST[ "idNews" ] ,FILTER_VALIDATE_INT);
$gmt 	    = filter_var( $_REQUEST[ "gmt" ] ,FILTER_SANITIZE_STRING );

require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {
//update `news` set hits = hits+1 where `idNews`='$idNews'
	$objU      = $auth->getUsrObj( $token )->data;
	$uID       = $objU->uID;

	$timestamp = date( 'Y-m-d H:i:s' );
	$sqlHits = "INSERT INTO `it_ratings` (`item_type`,`item_id`, `it_users_id`, `type`, `date`) VALUES ('N','$idNews', '$uID', 'R', '$timestamp');";
	$micon->query( $sqlHits );

	//echo $sqlHits;
	
	$sqlNewsDet = "SELECT
	    `it_categories_news`.`name`  as NewsCateName
	   , `it_categories_news`.`color` as NewsCateColor
	   , `it_news`.`id` as idNews
	   , `it_news`.`title`
	   , `it_news`.`intro`
	   , `it_news`.`photo`
	   , `it_news`.`text`
	   , it_photos_news_id
	   , CONVERT_TZ(`it_news`.created_at,'-05:00','$gmt') as newsCreationDate
	   , (SELECT COUNT(*) FROM `it_ratings` WHERE `item_id`=`it_news`.`id` AND `type`='L' AND `item_type`='N' ) AS likes
	   , (SELECT `id` FROM `it_ratings` WHERE `item_id`=`it_news`.`id` AND `it_users_id`='$uID' AND `type`='L' AND `item_type`='N' ) AS me
	   , (SELECT COUNT(*) FROM `it_ratings` WHERE `item_id`=`it_news`.`id` AND `type`='R'  AND `item_type`='N' ) AS hits
	FROM
	    `it_news`
	    INNER JOIN `it_categories_news` 
	        ON (`it_news`.`it_categories_news_id` = `it_categories_news`.`id`)
	WHERE (`it_categories_news`.`status` = 'A' and `it_news`.`status`='A' )
	AND CONVERT_TZ(publication_date,'-05:00','$gmt') <= NOW() AND ( CONVERT_TZ(close_date >= NOW(),'-05:00','$gmt') OR ISNULL(close_date) OR close_date='' )
	AND ( `it_news`.`id` = $idNews)
	"  
		;
//		echo $sqlNewsDet;
	
	$micon->query( $sqlNewsDet );
	$dtaNewsDetail = $micon->fetchArray(); 
	$dataArr = array();
//	$dataDays = array();
	//$today = date( "Y-m-d" ); //2017-10-17
	//$today = "2017-10-19";

	$dataDet = array();

	$objNewsDetail = new stdClass();
	//$objNewsDetail->Id = $dtaNewsDetail[ idschedule ];
	$objNewsDetail->Id 		=  $dtaNewsDetail[ idNews ];
	$objNewsDetail->Title 	= ( $dtaNewsDetail[ title ] != "" ) ? $dtaNewsDetail[ title ] : "";
	$objNewsDetail->Content	= ( $dtaNewsDetail[ text ] != "" ) ? convert2ExternalUrl( $dtaNewsDetail[ text ]) : "";
//	$objNewsDetail->Speaker = ( $dtaNewsDetail[ presentationModerator ] != "" ) ? $dtaNewsDetail[ presentationModerator ] : "";
	$objNewsDetail->Date 	= ( $dtaNewsDetail[ newsCreationDate ] != "" ) ? friendlyDate($dtaNewsDetail[ newsCreationDate ] ,true,true,true): "";
	//$objNewsDetail->Type 	= ( $dtaNewsDetail[ scheduleType ] != "" ) ? $dtaNewsDetail[ scheduleType ] : "";
	$objNewsDetail->Img 	= ( $dtaNewsDetail[ photo ] != "" ) ? URL . "/media/images/news/" . $dtaNewsDetail[ photo ] : "";
	//$objNewsDetail->Type 	= $dtaNewsDetail[ NewsType ] ;
	$objNewsDetail->Type 	= "txt" ;
	
	$objNewsDetail->Category 	= $dtaNewsDetail[ NewsCateName ];		
    $objNewsDetail->CateColor	= "#" . $dtaNewsDetail[ NewsCateColor ];
	
	$objNewsDetail->Liked 	= false;
	if ($dtaNewsDetail[ me ] != ""){
		$objNewsDetail->Liked = true;
	}
	
	
	$objNewsDetail->hasGallery 	= false;
	if ($dtaNewsDetail[ it_photos_news_id ] != ""){ 
		$objNewsDetail->hasGallery = true;
		$objNewsDetail->idGallery  = $dtaNewsDetail[ it_photos_news_id ];
	}
	
	$objNewsDetail->Likes = ( $dtaNewsDetail[ likes ] != "" ) ? $dtaNewsDetail[ likes ] : "0";
	$objNewsDetail->Views = ( $dtaNewsDetail[ hits ] != "" ) ? $dtaNewsDetail[ hits ] : "0";
	
	array_push( $dataDet, $objNewsDetail );


	//$objScheduleDet->Detail = [ "detail" => $dataDet ];
	//array_push( $dataArr, $objScheduleDet );		
//	array_push( $dataDays, $objScheduleDet );
	//objScheduleDet" => [ "region" => $dataArr ],		


	$response = array(
		"result"				 => true,
		"objNewsDetail"  => $dataDet ,
	);

}

echo json_encode( $response );
?>