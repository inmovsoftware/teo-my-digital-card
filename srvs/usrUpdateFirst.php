<?
require( 'include/mysql_class.php' );


$response 		= array( "result" => false );
$txtUName 		= filter_var( $_POST[ "txtUName" ],FILTER_SANITIZE_STRING );
$txtULName 		= filter_var( $_POST[ "txtULName" ] ,FILTER_SANITIZE_STRING);
$txtPhone 		= filter_var( $_POST[ "txtUPhone" ],FILTER_SANITIZE_STRING );
$txtBDay 		= filter_var( $_POST[ "txtBDay" ] ,FILTER_SANITIZE_STRING);
$txtNewPassword = filter_var( $_POST[ "txtNewPassword" ],FILTER_SANITIZE_STRING );
$hidIdPosition  = filter_var( $_POST[ "hidIdPosition" ],FILTER_SANITIZE_STRING );
$hidCountryCode = filter_var( $_POST[ "hidCountryCode" ],FILTER_SANITIZE_STRING );

$token 			= filter_var( $_POST[ "token" ],FILTER_SANITIZE_STRING );

require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {
	

	$objU = $auth->getUsrObj( $token )->data;
	$uID  = $objU->uID;





	$sql = "SELECT * from `VS_USERLOGIN` WHERE id='" . $uID . "'";
	$micon->query( $sql );


	if ( $micon->numRows() ) {

		$timestampLogin = date( 'Y-m-d H:i:s' );

        
        
        $udpPassword = "";
        if ($txtNewPassword!=""){
			$udpPassword = ", password='".  password_hash($txtNewPassword , PASSWORD_DEFAULT, ['cost' => 12])    ."', date_password_change='". $timestampLogin ."' ";
		}
        

		$sql = "UPDATE `it_users` SET `updated_at`='" . $timestampLogin . "', `name`='" . trim($txtUName) . "', `phone`='" . $txtPhone . "', `last_name`='" . trim($txtULName) . "',  `update_data`='1', `birthday`='" . $txtBDay . "', `it_positions_id`='" . $hidIdPosition ."', `country_phone`='" . $hidCountryCode ."' $udpPassword WHERE `id`='" . $uID . "'";
		$micon->query( $sql );
        

		$sql = "SELECT * from `VS_USERLOGIN` WHERE id='" . $uID . "'";
		$micon->query( $sql );
		$arrCustomer 	= $micon->fetchArray();

		$arrPrivData = array(
			"uID"               => $arrCustomer[ id ],
			"uName"             => $arrCustomer[ name ] ,
			"uLName"            => $arrCustomer[ last_name ] ,
			"uMobile"           => $arrCustomer[ phone ],
			"uPrefix"           => $arrCustomer[ country_phone ],
			"uMail"             => $arrCustomer[ email ],
			"sType"             => $arrCustomer[ session_type ],
			"uPosition"         => $arrCustomer[ it_positions_id ],
			"canChangePosition" => ($arrChangePosition[value] === 'true') ? true : false ,
			"companyId" 		=> $arrCustomer[companyId],
			"branchId" 			=> $arrCustomer[it_branches_id],
			"is_partner" 		=> ($arrCustomer[is_partner] == "true") ? true : false,
		);
					
		
		
		
		$_GET["action"] = "genToken";
		require('handler.php');
		
		$response = array(
			"uBDay"             => ($arrCustomer[ birthday ] != "") ? $arrCustomer[ birthday ]  : "",
			"uStatus"           => $arrCustomer[ status ],
			"uAvatar"           => $arrCustomer[ avatar ],		
			"updated"           => $arrCustomer[ update_data ],		
			"posName"           => $arrCustomer[ posName ],		
			"api"				=> $arrCustomer[ session_type ],
			"token"             => $tokenResponse,

			"result"            => true,
		);
		
	}

}

echo json_encode( $response );
?>