<?
require( 'include/mysql_class.php' );


$response 	= array( "result" => false );
//filter_var($email, FILTER_SANITIZE_EMAIL);
$email 		=  filter_var( $_GET[ "txtEMail" ],FILTER_SANITIZE_EMAIL ) ;
//$uPass 		=  md5(  filter_var($_GET[ "txtPassword" ],FILTER_SANITIZE_STRING) ); 
$uPass 		=  filter_var($_GET[ "txtPassword" ],FILTER_SANITIZE_STRING);  
$platform 	=  filter_var( $_GET[ "plat" ] ,FILTER_SANITIZE_STRING);
$model 		=  filter_var( $_GET[ "model" ] ,FILTER_SANITIZE_STRING);
$version 	=  filter_var( $_GET[ "ver" ],FILTER_SANITIZE_STRING) ;
$uuid 		=  filter_var( $_GET[ "uuid" ] ,FILTER_SANITIZE_STRING);
$appVersion =  filter_var( $_GET[ "appVersion" ] ,FILTER_SANITIZE_STRING);
//$pcc 		=  filter_var( $_GET[ "pcc" ] ,FILTER_SANITIZE_STRING);


$sql = "  SELECT
    *
FROM
    VS_USERLOGIN
WHERE `email`='$email'  ";
    //echo $sql;
$micon->query( $sql );
$arrCustomer 	= $micon->fetchArray();

//echo $arrCustomer[password] ;
if ( $micon->numRows() > 0 ) {
	if ( $arrCustomer[session_type] == "Manual" ){

			if ( password_verify( $uPass , $arrCustomer[password] )      ) {
				
				$timestampLogin = date( 'Y-m-d H:i:s' );
				//$arrCustomer 	= $micon->fetchArray();


				$sqlCompanyConfig = " SELECT `name`, `value` 
				FROM `it_parameters` 
				WHERE `it_business_id` in ( SELECT it_business_id FROM `it_branches` WHERE `id` = '". $arrCustomer[it_branches_id] ."'  ) AND `name` = 'change_positions'";
				$micon->query( $sqlCompanyConfig );
				$arrChangePosition 	= $micon->fetchArray();



				
				$arrPrivData = array(
					"uID"               => $arrCustomer[ id ],
					"uName"             => mb_convert_case( trim( $arrCustomer[ name ] ), MB_CASE_TITLE, "UTF-8") ,
					"uLName"            => mb_convert_case( trim( $arrCustomer[ last_name ] ), MB_CASE_TITLE, "UTF-8"),
					"uMobile"           => $arrCustomer[ phone ],


					"uPrefix"           => ($arrCustomer[ country_phone ] != "") ? $arrCustomer[ country_phone ]  : "1",


					"uMail"             => $arrCustomer[ email ],
					"uPosition"         => $arrCustomer[ it_positions_id ],
					"canChangePosition" => ($arrChangePosition[value] === 'true') ? true : false ,
					"companyId" 		=> $arrCustomer[companyId],
					"branchId" 			=> $arrCustomer[it_branches_id],
					"is_partner" 		=> ($arrCustomer[is_partner] == "true") ? true : false,

				);

				require_once('handler.php');
				
			   
			    if ( ($arrCustomer[ birthday ]=="")  or  ($arrCustomer[ birthday ]=="0000-00-00")  )  {
			    	 $today = date('Y-m-d');
					 $dminus13 = strtotime ( '-13 year' , strtotime ( $today ) ) ;
					 $bDay = date ( 'Y-m-d' , $dminus13 );		 

			    }
			    else{
			    	$date1 = new DateTime('now');
					$date2 = new DateTime( $arrCustomer[ birthday ]  ); 
			    	if ($date1->diff($date2)->y >= 13 ){
			    		 $bDay = $arrCustomer[ birthday ] ;
					}
					else{
			    		 $bDay = "1990-01-01";
					}

			    } 

				$response = array(
					"uBDay"             => $bDay,
					"uStatus"           => $arrCustomer[ status ],
					"uAvatar"           => $arrCustomer[ avatar ],		
					"updated"           => $arrCustomer[ update_data ],		
					"posName"           => $arrCustomer[ posName ],		
					"api"				=> $arrCustomer[ session_type ],
					"token"             => $tokenResponse,
					"sType"             => $arrCustomer[ session_type ],
					"result"            => true,
				);





				$sql = "update it_users set last_login='" . $timestampLogin . "',last_open='" . $timestampLogin . "', model='" . $model . "', platform='" . $platform . "', version='" . $version . "' ,uuid='" . $uuid . "'  , app_version='" . $appVersion . "', first_login = IF(first_login IS NULL, '$timestampLogin',first_login) where id='" . $arrCustomer[ id ]. "'";
				$micon->query( $sql );

			}
	}
	else {
		$response = array(
			"uStatus"           => "M",
			"result"            => true,
			"sType"				=> $arrCustomer[session_type] ,

		);

	}
}
else{
	$response 	= array( "result" => false );
}
echo json_encode( $response );
?>