<?
header( "Access-Control-Allow-Origin: *" );
require( 'include/mysql_class.php' );
require( 'include/utils.php' );


$response = array( "result" => false );
 
$token 			= filter_var( $_REQUEST[ "token" ],FILTER_SANITIZE_STRING );
$gmt 			= filter_var( $_REQUEST[ "gmt" ] ,FILTER_SANITIZE_STRING);
$pID 			= filter_var( $_REQUEST[ "pID" ] ,FILTER_SANITIZE_STRING);

//print_r($_REQUEST);
require_once( 'handler.php' );
if ( $tokenResponse == "true" ) {

	$objU      = $auth->getUsrObj( $token )->data;
	$uID       = $objU->uID;
	$companyId = $objU->companyId;
	
	//$sqlCommunity = " SELECT * FROM `places` ORDER BY placeName asc ";
	$sqlCommunity = " SELECT `it_users`.`name`, `it_users`.`last_name`,`it_users`.`id` as uID , `it_users`.`avatar`, 
`it_posts`.*, 
CONVERT_TZ(`it_posts`.`date`,'-05:00','$gmt') AS postDate,

(SELECT COUNT(`id`) FROM `it_ratings` WHERE `it_ratings`.`item_id`=it_posts.`id` AND `type` = 'L' AND `item_type`='P') AS likes,
(SELECT COUNT(`id`) FROM `it_ratings` WHERE `it_ratings`.`item_id`=it_posts.`id` AND `type` = 'C' AND `item_type`='P') AS comments
, (SELECT `it_ratings`.`id` FROM `it_ratings` WHERE   `it_ratings`.`item_id`=it_posts.`id` AND `type` = 'L' AND `item_type`='P' AND  `it_ratings`.`it_users_id`='$uID' limit 1) AS me
, IF(STRCMP(`it_posts`.`it_users_id`,'$uID'),FALSE,TRUE) AS iOwner
,(SELECT `it_users`.`can_notify` FROM `it_users` WHERE `it_users`.`id`='$uID') AS UserNotification
FROM
    `it_users` 
    INNER JOIN `it_posts`
        ON (`it_users`.`id` = `it_posts`.`it_users_id` AND `it_posts`.`status` = 'A')  
		
	WHERE `it_business_id` = '$companyId' and `it_posts`.id = $pID
      ORDER BY  `date` DESC, `it_posts`.`id`  DESC";
	
  //  echo $sqlCommunity;
	$micon->query( $sqlCommunity );
	//$dataArr = array();
	$dataCommunity = array();
	//$today = date("Y-m-d");//2017-10-17
	//$today = "2017-10-19";
	$hasPost = false;
	//$canNotify 	= false;
	while ( $dtaCommunity = $micon->fetchArray() ) {
        
       
		$hasPost = true;
		$objCommunity				= new stdClass();
		$objCommunity->Id 			= $dtaCommunity[ id ];
		$objCommunity->uId 			= $dtaCommunity[ idUser ];
		$objCommunity->Text			= ( $dtaCommunity[ text ] != "" ) ?  $dtaCommunity[ text ]  : ""; 
		$objCommunity->Pic			= ( $dtaCommunity[ photo ] != "" ) ? URL . "media/images/community/" . $dtaCommunity[ photo ] : "";
		$objCommunity->uAvatar		= ( $dtaCommunity[ avatar] != "nouser.png" ) ? URL . "media/images/profiles/" . $dtaCommunity[ avatar ] : "nouser.png" ;
		//( $dtaCompanies[ Companylogo] != "" ) ? "http://app.mydigital.cards/srvs/media/images/companies/" . $dtaCompanies[ Companylogo] : "nologo.png";
		$objCommunity->uName		= mb_convert_case( trim( $dtaCommunity[ name ] ), MB_CASE_TITLE, "UTF-8") . " " . mb_convert_case( trim( $dtaCommunity[ last_name ] ), MB_CASE_TITLE, "UTF-8");
	//	$objCommunity->Owner		= $dtaCommunity[ userName ] ;
		//$objCommunity->Content	= $dtaCommunity[ contentNews ];
		$objCommunity->Date 		= friendlyDate($dtaCommunity[ postDate ],true,true,true);	
		$objCommunity->TimeAgo 		= time_ago($dtaCommunity[ postDate ]);	
		
		
		//$objCommunity->Img 			= ( $dtaCommunity[ srcNews ] != "" ) ? $dtaCommunity[ srcNews ] : "";
		$objCommunity->Comments 	= ( $dtaCommunity[ comments ] != "" ) ? $dtaCommunity[ comments ] : "0";
		$objCommunity->Likes 		= ( $dtaCommunity[ likes ] != "" ) ? $dtaCommunity[ likes ] : "0";
        $objCommunity->Type 		=  $dtaCommunity[ type ] ;
        
		
		$objCommunity->Liked 	= false;
		if ($dtaCommunity[ me ] != ""){
			$objCommunity->Liked = true;
		}
		
		$objCommunity->Owner 	= false;
		if ($dtaCommunity[ iOwner ] != "0"){
			$objCommunity->Owner = true;
		}
		
		
		
		array_push( $dataCommunity, $objCommunity );
				
	}
	
	
	
	

	$response = array(
		"result" 		    =>  true ,
		"objPosts" 		    =>  $dataCommunity ,
		"hasPost" 		    =>  $hasPost ,
		//"canNotify" 	    =>  $canNotify ,
	);

}
echo json_encode( $response );
?>