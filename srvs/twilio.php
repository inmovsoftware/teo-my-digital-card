<?php
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;


class AuthController extends Controller
{


    public function sms(Request $request){

        $data = $request->validate([
            "phone" => "required",
            "message" => "required"
        ]);


            $item[] =   array( "name"=> "Guest", "phone_number"=> $data["phone"]);


            $this->pushSms( $data["message"] , $item);

            return response()->json(
                [
                    'errors' => [
                        'status' => 200,
                        'messages' => [trans('auth.smssent')]
                    ]
                ],
                200
            );

    }



protected function pushSms($message,  $numbers)
    {
        $this->_notifyThroughSms($message, $numbers);

    }

private function _notifyThroughSms($e, $numbers)
    {
       // $items = $this->_notificationRecipients();
        foreach (  $numbers  as $recipient) {
            $this->_sendSms(
                $recipient["name"],
                $recipient["phone_number"],
                $e
            );
        }
    }

private function _notificationRecipients()
    {
        $adminsFile = base_path() .
            DIRECTORY_SEPARATOR .
            'config' . DIRECTORY_SEPARATOR .
            'administrators.json';
        try {
            $adminsFileContents = \File::get($adminsFile);

            return json_decode($adminsFileContents);
        } catch (FileNotFoundException $e) {
            Log::error(
                'Could not find ' .
                $adminsFile .
                ' to notify admins through SMS'
            );
            return [];
        }
    }

private function _sendSms($name, $to, $message)
    {
        $accountSid   = 'ACdc935164b11de2860ce4f34b0f77b8b3';
        $authToken    = '6929e00896ac5f699a85ac88f2687810';
        $twilioNumber = '+14697784007';

        $client = new Client($accountSid, $authToken);

        try {/*
            $validation_request = $client->validationRequests
                             ->create($to, // phoneNumber
                                      array(
                                          "friendlyName" => $name
                                      )
                             );

            Log::info('Validation Request  ' . $validation_request);
*/
            $client->messages->create(
                $to,
                [
                    "body" => $message,
                    "from" => $twilioNumber
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );
          //  Log::info('Mensaje enviado a ' . $twilioNumber);
        } catch (TwilioException $e) {
            Log::error(
                'Could not send SMS notification.' .
                ' Twilio replied with: ' . $e
            );
        }
    }
}
