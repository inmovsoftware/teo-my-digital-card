<?
require_once 'vendor/autoload.php';
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
require( 'include/mysql_class.php' );


$response = array( "result" => false  );
$email    =  filter_var($_GET["txtIDfp"],FILTER_SANITIZE_EMAIL);

$sql = "SELECT *
FROM
    `it_users`    
	WHERE email='" . $email . "'";

//echo $sql;
$micon->query( $sql );

//echo $micon->numRows();
if ( $micon->numRows() ) {
    $arrUser = $micon->fetchArray();

    if ( $arrUser[ UserStatus ] != "C" ) {

        if ( $arrUser[ session_type ] == "Manual" ){

            $code        = mt_rand( 100000, 999999 );
            $uPhone      = $arrUser[ phone ];
            $phonePrefix = $arrUser[ country_phone ];

            if ( $uPhone != "" ) {
                    $sql = "update it_users set recovery_code='" . $code . "' where email='" . $email . "'";
                    $micon->query( $sql );

                    //$time = date( "H:i:s" );
                    //Use the code: 1111 to restore your access to the Diageo Champions App
                    $msg = "Your TEO MDC code to restore your access is: " . $code;

                    


                    $twilio = new Client(SID, TOKEN);
                    $to     = "+" . trim($phonePrefix) . trim($uPhone) ;


                    try {
                        $message = $twilio->messages
                         ->create($to, // phoneNumber
                                 array(
                                   "body" => $msg,
                                   "from" => "+14697784007",
                                   //"statusCallback" => "https://postb.in/jlhRBchc"
                                )
                         );

                        //echo $message->sid . "\n";
                        //echo $message->status . "\n";

                        sleep( 2 );
                       // echo "----------------------------------------\n";
                        $messageInfo = $twilio->messages(  $message->sid )
                                        ->fetch();

                        $statusTW = $messageInfo->status;                       
                        //undelivered
                        //failed

                    } catch (TwilioException $e) {
                        //echo 'Could not send SMS notification.' . ' Twilio replied with: ';
                        //print_r($e[code]) ;    
                       // echo ($e->Status);    
                         $statusTW = "unknow";

                    }


                   
                   // require_once( 'elibom/elibom.php' );
                   // $elibom = new ElibomClient( 'fburgos@mejia.com.co', 'IsA1805fl' );
                   // $deliveryId = $elibom->sendMessage( $uPhone, $msg );


                    //sleep( 6 );

                   // $delivery = $elibom->getDelivery( $deliveryId );


                    $subPhone    = substr( $uPhone, 6 );
                    $maskedPhone = str_pad( $subPhone, 10, "*", STR_PAD_LEFT );

                    $response = array(
                        "result"         => true,
                        "mPhone"         => "+".trim($phonePrefix) . $maskedPhone,
                        "fPhone"         => $uPhone,
                        "errCode"        => 202,
                        "twilioResponse" => $statusTW,
                    );
              }
             else { ///IF HAS NOT PHONE NUMBER
                $response = array(
                        "result"  => true,
                        "errCode" => 400 ,
                    );
                }
        }
        else{  ///IF  HAS NOT SOCIAL LOGIN
            $response = array(
                "result"    => true,
                "errCode"   => 406,
                "typeLogin" => $arrUser[ session_type ] ,

            );

        } 
    }
    else { //USER suspended
        $response = array( 
            "result" => true,
            "errCode" => 401,
        );
    }
}
echo json_encode( $response );
?>