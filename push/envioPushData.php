<?
include("../include/mysql_class.php");

$campana = $_POST['campanya'];
$filtro =  $_POST['filtro'];
$mensajeInput = $_POST['text'];

$enviarPush = false;
$mensaje = "";
$dispositivos = array();
/** Test **/
$test = false;

switch($filtro){
	/** Se busca toda la base de dispositivos de Don Maximo 3.0 **/
	case 1 : 
		$sql = "SELECT user_id_dispositivo as device FROM usuario WHERE user_id_dispositivo IS NOT NULL GROUP BY user_id_dispositivo";
		$micon->consulta($sql);
		while($result = $micon->campoconsultaA()){
			array_push($dispositivos , $result['device']);
		}
		$enviarPush = true;
	break;
	/** Se busca la base de dispositivos de Don Maximo 3.0 que ejecutaron de forma correcta **/
	case 2 : 
		$sql = "SELECT user_id_dispositivo as device FROM usuario u
				INNER JOIN pdv p ON p.`usuario_user_id` = u.`user_id`
				INNER JOIN campanya_ejecucion ce ON ce.`campanya_pdv_pdv_pdv_outnum` = p.`pdv_outnum`
				WHERE user_id_dispositivo IS NOT NULL
				AND ce.`ejec_estado` = 'AC'
				AND ce.`campanya_pdv_campanya_camp_id` = '".$campana."' GROUP BY user_id_dispositivo";
		$micon->consulta($sql);
		while($result = $micon->campoconsultaA()){
			array_push($dispositivos , $result['device']);
		}
		$enviarPush = true;
	break;
	/** Se busca la base de dispositivos de Don Maximo 3.0 que no ejecutaron en la campaña **/
	case 3 : 
		$sql = "SELECT user_id_dispositivo as device FROM usuario u
			INNER JOIN pdv p ON p.`usuario_user_id` = u.`user_id`
			INNER JOIN `campanya_pdv` cp ON cp.`pdv_pdv_outnum` = p.`pdv_outnum`
			WHERE user_id_dispositivo IS NOT NULL
			AND cp.`campanya_camp_id` = '".$campana."'
			AND cp.`pdv_pdv_outnum` NOT IN (
				SELECT `campanya_pdv_pdv_pdv_outnum` FROM `campanya_ejecucion` WHERE `campanya_pdv_campanya_camp_id` = '".$campana."') GROUP BY user_id_dispositivo";
		$micon->consulta($sql);
		while($result = $micon->campoconsultaA()){
			array_push($dispositivos , $result['device']);
		}
		$enviarPush = true;
	break;
	/** Se busca la base de dispositivos de Don Maximo 3.0 que enviaron en cualquier estado la campaña **/
	case 4 : 
		$sql = "SELECT user_id_dispositivo AS device FROM usuario u
			INNER JOIN pdv p ON p.`usuario_user_id` = u.`user_id`
			INNER JOIN `campanya_ejecucion` ce ON ce.`campanya_pdv_pdv_pdv_outnum` = p.`pdv_outnum`
			WHERE user_id_dispositivo IS NOT NULL
			AND ce.`campanya_pdv_campanya_camp_id` = '".$campana."'
			GROUP BY user_id_dispositivo";
		$micon->consulta($sql);
		while($result = $micon->campoconsultaA()){
			array_push($dispositivos , $result['device']);
		}
		$enviarPush = true;
	break;
	/** Se lee los dispositivos que vienen en el archivo **/
	case 5 : 
	
	break;
	/** Se realiza el control de filtro por seguridad **/
	default:
		$enviarPush = false;
	break;
}

if(true){
	//echo "Dispositivos encontrados ".sizeof($dispositivos);
	$dispositivos = array();
	array_push($dispositivos , 'bd5d7b01902b977c');
}
/** Añadiendo a los usuarios de prueba **/

$sql = "SELECT user_id_dispositivo AS device FROM usuario u WHERE u.`user_login` IN (SELECT up.`pdv` FROM usuarios_prueba up) AND user_id_dispositivo IS NOT NULL";
$micon->consulta($sql);
while($result = $micon->campoconsultaA()){
	array_push($dispositivos , $result['device']);
}

if($enviarPush){
	if(sizeof($dispositivos) > 0){
		//print_r ($dispositivos); 
		$data = array(
		'application' => '08A7D-5EF7C',
		'auth' => "erXC6OicwB9ecvjdax0LMKTECxtMQ0qNfHpnG5R0JJdzsVjh0mbMhqA9hO31LsUc4uRUluJntqSAI71KAyN2",
		'notifications' => array(
				array(
					'send_date' => 'now',
					'content' => $mensajeInput,
					'android_badges'=>+1,
					'ios_badges'=>+1,
					'ios_title'=>"Amig@ Detallista",
					'android_header'=>"Amig@ Detallista",
					'android_vibration'=>1,
					'ios_ttl'=>172800,
					'android_gcm_ttl'=>172800
				)
			)
		);
		
		$url = "https://cp.pushwoosh.com/json/1.3/createMessage";
		$request = json_encode(['request' => $data]);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
	 
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
	 
		if ($test) {
			print "[PW] request: $request\n";
			print "[PW] response: $response\n";
			print '[PW] info: ' . print_r($info, true);
		}else{
			$mensaje = "Hemos enviado las push a ".sizeof($dispositivos)." dispositivos";
		}
		
	}else{
		$mensaje = "No se encontramos dispositivos en el filtro, intentalo nuevamente";
	}
}
else{
	$mensaje = "No se encontró filtro por favor verifique su selección";
}

header('Location: push_new.php?gwall=1&mensaje='.$mensaje);
?>