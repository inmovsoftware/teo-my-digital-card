<? 
include("../include/seguridad.php");
include("../include/mysql_class.php");

$hoy = date('Y-m-d');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Don Máximo | Inmov</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
      <!-- Toastr style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="../css/plugins/datapicker/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="../js/plugins/colorbox/colorbox.css" rel="stylesheet" />
    <link href="../css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../images/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>
    <div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<? include('../menu_left_interna.php');?>
			</div>
		</nav>
        <div id="page-wrapper" class="gray-bg">
			<div class="row border-bottom">
			 <? include('../top.php');?>
			</div>
			<div class="wrapper wrapper-content">
        		<div class="row animated fadeInRightBig">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h3>Enviar Push filtrado</h3>
							</div>
							<div class="ibox-content">
								<div class="panel blank-panel">
									<div class="panel-body">
										<div class="tab-content">
											<div id="bitacora" class="tab-pane <?=$_GET['bitacora']?> active">
											<? if($_GET['gwall']==1){?>
												<div class="alert alert-success alert-dismissable">
													<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
													<i class="ace-icon fa fa-info-circle bigger-110 success"></i> 
													 <?=$_GET['mensaje']?><br>
												</div>
											<? }?>
												<form id="add-form" action="envioPushData.php" method="post" enctype="multipart/form-data" >
													<fieldset>
														<div class="hr-line-dashed"></div>
                                                        <h3 style="color:#1c1950;">Seleccione  campaña</small></h3> 
														<div class="hr-line-dashed"></div>
                                                        <div class="row">
															<div class="col-lg-12">
																<div class="form-group">
																	<label>Campaña*</label>
																	<select name="campanya" id="campanya" class="form-control" >
																		<option value="">::</option>
																		<? 
																		$sql="SELECT c.`camp_id` AS id ,
																				c.`camp_consecutivo` AS consecutivo,
																				c.`camp_nombre` AS nombre,
																				DATE_FORMAT( c.`camp_inicio` , \"%Y-%m-%d\") AS  inicio ,
																				DATE_FORMAT( c.`camp_fin` , \"%Y-%m-%d\") AS  fin ,
																				c.`camp_estado` AS estado
																				 FROM  campanya c ORDER BY c.`camp_id`DESC LIMIT 0 , 5"; 
																		$micon->consulta($sql);
																		while($dt = $micon->campoconsultaA()){
																			$estado = "";
																			if($dt['estado'] == "P"){
																				$estado = "de Prueba";
																			}else if($dt['estado'] == "A"){
																				$estado = "Activa";
																			}else if($dt['estado'] == "F"){
																				$estado = "Finalizada";
																			}else if($dt['estado'] == "S"){
																				$estado = "Suspendida";
																			}
																		?>
																		<option value="<?=$dt['id']?>"><?="Campaña ".$estado." || Válida de ".$dt['inicio']." hasta ".$dt['fin']." || ".$dt['consecutivo']." - ".$dt['nombre']?></option>
																		<?}?>
																	</select>
																</div>
															</div>
														</div> 
														<div class="row">
															<div class="col-lg-6">
																<div class="form-group form-inline" >
																	<div class="hr-line-dashed"></div>
																	<h3 style="color:#1c1950;">Filtrar sobre la campana</small></h3> 
																	<div class="hr-line-dashed"></div>
																	<p>
																		<label>
																			<input type="radio" name="filtro" value="1" id="tipocampania1">
																			Todos los Clientes
																		</label>&nbsp;&nbsp;&nbsp;
																	</p>
																	<p> 
																		<label>
																			<input type="radio" name="filtro" value="2" id="tipocampania2">
																			Ganadores
																		</label>&nbsp;&nbsp;&nbsp;
																	</p> 																	
																	<p>     
																		<label>
																			<input type="radio" name="filtro" value="3" id="tipocampania3">
																			Clientes que no enviaron Campaña
																		</label>&nbsp;&nbsp;&nbsp;
																	</p>  
																	<p>
																		<label>
																			<input name="filtro" type="radio" value="4" id="tipocampania4">
																			Clientes que enviarón Campaña
																		</label>   
																	</p>
																	<p>
																		<label>
																			<input name="filtro" type="radio" value="5" id="tipocampania5">
																			Archivo con ID
																		</label>   
																	</p>																	
																</div>
															</div>
															<div class="col-lg-6" id="instruccion" style="display:none">
																<div class="alert alert-success alert-dismissable">
																	<h3>Especificaciones del Archivo</h3>
																	<p>El archivo debe tener la extensión <strong>XLS</strong> ó <strong>XLSX.</strong></p>
																	<p>El tamaño no puede superar los dos <strong>(2) MB.</strong></p>
																	<p>Los datos deben venir en la <strong>Hoja 1.</strong></p>
																	<p>La Celda <strong>A1</strong> debe contener Seis (6) letras A: <strong>AAAAAA.</strong></p>
																	<p>La última fila en su columna <strong>A</strong> debe contener Seis (6) letras Z: <strong>ZZZZZZ.</strong></p>
																	<p>La columna <strong>A</strong> debe contener el <strong>Id del Dispositivo.</strong></p>
																</div>
																<div class="form-group">
																  <label for="fichero">Adjunta tu archivo aquí</label>
																  <input class="form-control" id="fichero" type="file">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">													 
																<div class="form-group">
																	<div class="hr-line-dashed"></div>
																	<h3 style="color:#1c1950;">Texto a enviar en el push</small></h3> 
																	<div class="hr-line-dashed"></div>
																	Para ver la tabla de emoticones disponibles en los mensajes push <a href="https://apps.timwhitlock.info/emoji/tables/unicode" target="_blank">da click aquí.</a>Copia la primera columna llamada <b>Native</b>
																	<div class="hr-line-dashed"></div>
																	<textarea class="form-control " name="text" id="text" required></textarea>
																</div>
																<button type="submit" class="btn btn-w-m btn-primary"><i class="fa fa-paper-plane-o"></i> Enviar</button>
															</div>
															<br><br>
														</div>
													</fieldset>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>            
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Data picker -->
	<script src="../js/plugins/datapicker/bootstrap-datepicker.min.js"></script>
	<script src="../js/plugins/colorbox/jquery.colorbox-min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>
      <!-- iCheck -->
    <script src="../js/plugins/iCheck/icheck.min.js"></script>
    <script>
		jQuery(function($) {

		});
		
		$( "input[name=filtro]" ).on( "click", function() {
			if ($('#tipocampania5').is(':checked')) {
				$("#instruccion").show(1000);
			}else{
				$("#instruccion").hide(1000);
			}
		});
    </script>
     <!-- jQuery UI custom -->
	<script src="../js/jquery-ui.custom.min.js"></script>
	<script src="../js/plugins/confirm-master/jquery.confirm.js"></script>
	<script src="../js/plugins/toastr/toastr.min.js"></script>
	<!-- Data picker -->
</body>
</html>