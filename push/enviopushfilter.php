<?
require('../include/mysql_class.php');
require('pushService/services.php');
//$_POST[tipopush]
//$_POST[filtro]
$mensaje = "No se encontraron dispositivos para enviar push";
$dispositivos = array();
//Se valida el tipo de filtro para el envio de push
switch($_POST[tipopush]){
	// Envio Por filtro
	case 1 :
		$sql = "";
		//Se valida la consulta que se necesita
		// Clientes que no enviaron Campana
		if($_POST[filtro] == '1'){
			$sql = "SELECT 
				u.`user_id_dispositivo` AS iddispositivo,
				p.`pdv_outnum` AS outnum
				FROM campanya_pdv cp
				INNER JOIN pdv p ON cp.`pdv_pdv_outnum` = p.`pdv_outnum`
				INNER JOIN usuario u ON p.`usuario_user_id` = u.`user_id`
				WHERE u.`user_id_dispositivo` IS NOT NULL 
				AND p.`pdv_outnum` NOT IN (SELECT ce.`campanya_pdv_pdv_pdv_outnum` FROM campanya_ejecucion ce WHERE ce.`campanya_pdv_campanya_camp_id` = '".$_POST[campanya]."' GROUP BY ce.`campanya_pdv_pdv_pdv_outnum`)
				AND cp.`campanya_camp_id` = '".$_POST[campanya]."'";
		}
		//Todos los clientes de la Campana
		else if($_POST[filtro] == '2'){
			$sql = "SELECT 
					u.`user_id_dispositivo` AS iddispositivo,
					p.`pdv_outnum` AS outnum
					FROM campanya_pdv cp
					INNER JOIN pdv p ON cp.`pdv_pdv_outnum` = p.`pdv_outnum`
					INNER JOIN usuario u ON p.`usuario_user_id` = u.`user_id`
					WHERE u.`user_id_dispositivo` IS NOT NULL 
					AND cp.`campanya_camp_id` = '".$_POST[campanya]."'";
		}
		//Ganadores de la campana
		else if($_POST[filtro] == '3'){
			$sql = "SELECT 
					u.`user_id_dispositivo` AS iddispositivo
					FROM  usuario u 
					WHERE u.`user_id_dispositivo` IS NOT NULL 
					AND u.`user_login` IN (SELECT v.`nit` FROM v_listado_auditoria v WHERE v.`estado` = 'AC' AND v.`campana` ='".$_POST[campanya]."' )";
		}
		//Enviaron la Campana
		else if($_POST[filtro] == '2'){
			$sql = "SELECT 
						u.`user_id_dispositivo` AS iddispositivo
						FROM  usuario u 
						WHERE u.`user_id_dispositivo` IS NOT NULL 
						AND u.`user_login` IN (SELECT v.`nit` FROM v_listado_auditoria v WHERE v.`campana` ='".$_POST[campanya]."' )
						GROUP BY u.`user_id`";
		}
		//No se encontro filtro
		else{
			$mensaje = "No se encontró filtro para enviar push";
		}
		if($sql != ""){
			//echo $sql;			
			$micon->consulta($sql);
			while($dtc = $micon->campoconsultaA()){
				array_push($dispositivos , $dtc['iddispositivo']);
			}
			//print_r($dispositivos);
		}
	break;
	// Envio por archivo
	case 2:
		$file = $_FILES['file']['tmp_name'];
		$handle = fopen($file, "r");
		while(($filesop = fgetcsv($handle, 1000, ";")) !== false){
			echo "dispositivo".$filesop[0];
			array_push($dispositivos, $filesop[0]);
			
		}
	break;
}

/** VALIDA QUE EXISTAN DISPOSITIVOS A ENVIAR **/
if( sizeof($dispositivos) > 0){
	
	pwCall('createMessage', array(
		'application' => PW_APPLICATION,
		'auth' => PW_AUTH,
		'notifications' => array(
				array(
					'send_date' => 'now',
					'content' => $_POST[text],
					'devices' => $dispositivos,
					'data' => array('custom' => 'json data')
				)
			)
		)
	); 
	
	$mensaje = "Se envio el mensaje ".$_POST[text]." a ".sizeof($dispositivos). " dispositivos registrados";
}
//echo $mensaje;
header('Location: push.php?gwall=1&mensaje='.$mensaje.'');
?>