<? 
include("../include/seguridad.php");
include("../include/mysql_class.php");

$hoy = date('Y-m-d');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Salestime | Mejía</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
    
      <!-- Toastr style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">
    
    <link href="../css/plugins/datapicker/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="../js/plugins/colorbox/colorbox.css" rel="stylesheet" />
    <link href="../css/plugins/iCheck/custom.css" rel="stylesheet">
    
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../images/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    
     <!-- Data Tables -->
    <link href="../css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="../css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <? include('../menu_left_interna.php');?>
        </div>
    </nav>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
         <? include('../top.php');?>
        </div>
        <div class="wrapper wrapper-content">
        		<div class="row animated fadeInRightBig">
                <div class="col-lg-12">
                
                
                
                
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <h3>Enviar Push filtrado</h3></div>
                    <div class="ibox-content">
                      
                    	<div class="panel blank-panel">

                   
                

                        <div class="panel-body">
                            <div class="tab-content">
                                  <div id="bitacora" class="tab-pane <?=$_GET['bitacora']?> active">
                                
                                   
                                   
                                  <? if($_GET['gwall']==1){?>
						<div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								<i class="ace-icon fa fa-info-circle bigger-110 success"></i> 
                                 <?=$_GET[mensaje]?><br>
						</div>
					<? }?>
                                <form id="add-form" action="enviopushfilter.php" method="post" enctype="multipart/form-data" >
                                
                               
                                
                                
                                <fieldset>
                                
                                
                                  <div class="row">
                                
                                 <div class="col-lg-12 ">
                                                        <div class="form-group form-inline" >
                                                       <p>     
                                                         
                                                             <label>
                                                                <input name="tipopush" type="radio" id="tipopush" value="1" checked="checked" onChange="validacion()" >
                                                                Enviar Push por Filtro</label>
                                                              &nbsp;&nbsp;&nbsp;
                                                            <label>
                                                              <input type="radio" name="tipopush" value="2" id="tipopush1" onChange="validacion()">
                                                               Enviar Push por  archivo csv <small></small></label>
                                                               &nbsp;&nbsp;&nbsp;
															   <!--
                                                            <label>
                                                              <input type="radio" name="tipopush" value="3" id="tipopush1" onChange="validacion()">
                                                               Enviar Push segmentado por archivo csv <small>>>(fila 1: id, fila 2: mensaje)</small></label>
                                                            -->
                                                          </p> 
                                                        </div>
                                                     </div> 
                                                     
                                                     
                                                     </div>
                                <div class="hr-line-dashed"></div>
                                                                                                                     
                                
                                     <h3 style="color:#1c1950;"> Filtrar campaña</small></h3> 
                                <div class="hr-line-dashed"></div>
                                                                                                                            <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Campaña*</label>
                              <select name="campanya" id="campanya" class="form-control" >
                                <option value="">::</option>
                                <? $sql="SELECT * FROM campanya WHERE campanya.camp_fin >= now() "; 
								//echo $sql;
								$micon->consulta($sql);
									while($dt = $micon->campoconsultaA()){?>
                                    <option value="<?=$dt['camp_id']?>"><?=$dt['camp_id']." - ".$dt['camp_nombre']." || ".$dt['camp_inicio']."/".$dt['camp_fin']?></option>
                                    <? }?>
                                </select>
                                            </div>
                                         </div> 
                                        
                                         </div> 
                                         
                                         
                                         <div class="row">
                                                     <div class="col-lg-12 ">
                                                     <div class="form-group form-inline" >
                                                      <label>Filtro sobre Campaña*</label>
                                                       <p>     
                                                         
                                                             
                                                              
                                                            <label>
                                                              <input type="radio" name="filtro" value="1" id="tipocampania1"  onChange="mostrar()">
                                                               Clientes que no enviaron Campaña</label>
                                                                &nbsp;&nbsp;&nbsp;
                                                            <label>
                                                              <input type="radio" name="filtro" value="2" id="tipocampania2"  onChange="mostrar()">
                                                               Todos los Clientes</label>
                                                               &nbsp;&nbsp;&nbsp;
                                                            <label>
                                                              <input type="radio" name="filtro" value="3" id="tipocampania3"  onChange="mostrar()">
                                                               Ganadores</label>&nbsp;&nbsp;&nbsp;
                                                            <label>
                                                                <input name="filtro" type="radio" id="tipocampania" value="4" onChange="mostrar()"  >
                                                                Clientes que enviarón Campaña</label>   <div class="form-group" id="data_1"   style="display:none">
                                <label class="font-noraml">Fecha limite de envio Campaña:</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="fecha" class="form-control" value="<?=$hoy?>">
                                </div>
                            </div>
                            
                            
                                                          </p> 
                                                        </div>
                                                        </div>
                                                        </div>
                                         
                                         <div class="row">
                                        
                                         <div class="col-lg-12">
                                         
                                         
                                         
                                         <div class="form-group">
                                                <label>Archivo Csv*</label>
                                                <input id="file" name="file" type="file" class="form-control " accept=".csv" disabled >
                                           </div>
                                         
                                         	<div class="form-group">
                                                <label>Texto del Push*</label>
                                                <textarea class="form-control " name="text" id="text" required></textarea>
                                            </div>
                                         </div>
                                         	<div class="col-lg-6">
                                            
                                         </div>
                                           
                                         <br><br>
                                         <button type="submit" class="btn btn-w-m btn-primary"><i class="fa fa-paper-plane-o"></i> Enviar</button>
                                </fieldset>
                         </form>
                              </div>
                                
                                
                                
                                
                                
                            </div>
                        </div>

                    </div>
                    
                    
                         
                    </div>
                </div>
                
                
            </div>
            </div>
            </div>
        </div>
        
        
        
        
        
        </div>

    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Data picker -->
   <script src="../js/plugins/datapicker/bootstrap-datepicker.min.js"></script>
	<script src="../js/plugins/colorbox/jquery.colorbox-min.js"></script>
    <!-- Data Tables -->
    <script src="../js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="../js/plugins/dataTables/dataTables.responsive.js"></script>
    <script src="../js/plugins/dataTables/dataTables.tableTools.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>
      <!-- iCheck -->
    <script src="../js/plugins/iCheck/icheck.min.js"></script>
    <script>
    jQuery(function($) {

			$('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
				$('.input-daterange').datepicker({
    			format: "yyyy-mm-dd",
				autoclose: true
				});
		

			 });	
    </script>
     <!-- jQuery UI custom -->
<script src="../js/jquery-ui.custom.min.js"></script>
<script src="../js/plugins/confirm-master/jquery.confirm.js"></script>
 <script src="../js/plugins/toastr/toastr.min.js"></script>
  <!-- Data picker -->
   <script src="../js/plugins/datapicker/bootstrap-datepicker.js"></script>
 <? if($_GET['gwall']==4){ //echo "aaaaa";?>
        <script>
        $(document).ready(function() {
			
			
			
		 		setTimeout(function() {
                	toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.info('', 'PdV desvinculado con éxito');
            }, 300);});
        </script>
        <? }?>
        
        
        <script>
        $(document).ready(function() {
			
			$('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
				format: "yyyy-mm-dd",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });
			
		 		
            });
			
			
			
		function validacion() {
			
	 
			opcion = $('input:radio[name=tipopush]:checked').val();
		
			if (opcion ==1){
				
				document.getElementById("file").disabled = true;
				document.getElementById("campanya").disabled = false;
				document.getElementById("campanya").required;
				document.getElementById("file").required=false;
					document.getElementById("tipocampania1").disabled = false;
					document.getElementById("tipocampania2").disabled = false;
					document.getElementById("tipocampania3").disabled = false;
					document.getElementById("tipocampania").disabled = false;
				
				
				} else if(opcion ==2){
					document.getElementById("file").disabled = false;
					document.getElementById("campanya").required=false
					document.getElementById("file").required;
					document.getElementById("campanya").disabled = true;
					document.getElementById("tipocampania1").disabled = true;
					document.getElementById("tipocampania2").disabled = true;
					document.getElementById("tipocampania3").disabled = true;
					document.getElementById("tipocampania").disabled = true;
					
					
					
					}else{
						
						document.getElementById("file").disabled = false;
					document.getElementById("campanya").required=false;
					document.getElementById("text").required=false;
					document.getElementById("text").disabled=true;
					document.getElementById("file").required;
					document.getElementById("campanya").disabled = true;
					document.getElementById("tipocampania1").disabled = true;
					document.getElementById("tipocampania2").disabled = true;
					document.getElementById("tipocampania3").disabled = true;
					document.getElementById("tipocampania").disabled = true;
					
						
						}
			
		}
		
		
		function mostrar() {
			
			opcion = $('input:radio[name=filtro]:checked').val();
		
			if (opcion ==1){
				document.getElementById('data_1').style.display = 'block';
			}else{
				document.getElementById('data_1').style.display = 'none';
				}
				
				
				
			

			
	 
			
			
		}
		
		
		
		

	
		
		
		
	
			
        </script>
        
        
        
        
        
        
</body>
</html>