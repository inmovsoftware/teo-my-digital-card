<?php 
session_start();
require('../include/mysql_class.php');
include('../include/seguridad.php');
include('../srvs/include/utils.php');

$page         = filter_var( $_POST['page'] ,FILTER_SANITIZE_NUMBER_INT);
$pageSize        = filter_var( $_POST['pageSize'] ,FILTER_SANITIZE_NUMBER_INT);

$range_init = ($page*$pageSize)-$pageSize;

						
$sql = "SELECT CONCAT(it_users.name,' ',last_name ) AS nname, CONCAT(it_users.name,' ',last_name ) AS nameuser,  avatar, it_posts.status, photo,it_posts.text,it_posts.date, it_posts.type,
it_posts.id AS idpost, it_users.id AS iduser
FROM it_posts
INNER JOIN it_users ON it_posts.it_users_id = it_users.id
LEFT JOIN it_userblock_posts_admin ON it_posts.it_users_id = it_userblock_posts_admin.user_block_id
WHERE it_posts.status = 'A' AND ISNULL(user_block_id) ";
    if($_SESSION['is_partner'] =='true'){
                $sql .= " and it_branches.it_business_id ='".$_SESSION['companyID']."'";
    }					
    $micon2->consulta($sql); 
    $cantidad_total = $micon2->numregistros();
    $cantidad = $cantidad_total/$pageSize;
    $cantidad =  round($cantidad, 0, PHP_ROUND_HALF_UP);
    $sql .= " ORDER BY it_posts.date DESC Limit $range_init,$pageSize " ;
											
    $micon->consulta($sql); 

    if($cantidad_total > 0){
    while($DtaPost = $micon->campoconsultaA()){	
        $sql = "SELECT SUM(IF(it_ratings.type = 'C',1, 0)) AS CantComment , 
                SUM(IF(it_ratings.type = 'L',1, 0)) AS CantLike
                FROM it_ratings WHERE  item_id='".$DtaPost[idpost]."'";
        $micon1->consulta($sql);
        $Cant = $micon1->campoconsultaA();

        $sql_like = "SELECT SUM(IF(it_ratings.type='L',1,0)) AS n_like, SUM(IF(it_ratings.type='C', 1, 0)) AS n_read
        FROM it_ratings 
        INNER JOIN it_posts ON it_ratings.item_id = it_posts.id
        WHERE item_type = 'P' and it_posts.id = '".$DtaPost[id]."'";
    //	echo $sql_like;	
            $micon1->consulta($sql_like);
            $DtaLike = $micon1->campoconsultaA();
?>
    
    
    <div class="m-portlet m-portlet--bordered-semi  mypost m-portlet--rounded-force useritem<?=$DtaPost[iduser]?>" id="element<?=$DtaPost[idpost]?>" >
									
                                    <div class="m-portlet__body" style=" padding: 1.2rem 2.2rem;">
                                    


                                    <div class="m-widget19">
                                        <div class="m-widget19__content">
                                            <div class="m-widget19__header" style="margin-top: 0.75rem; margin-bottom: -0.25rem;">        
                                            
                                             
                                                    <div class="m-widget19__user-img" >
                                                 
                                                        <img class="m-widget19__img" src="srvs/media/images/profiles/<?=$DtaPost[avatar];?>" style="width: 35px; margin-top: -9px; height: 35px">
                                                            
                                                        
                                                        </div>
                                                        <div class="m-widget19__info" >

                                                        <span style="float: right;  margin-top: -11px;" > 
                                                                     <button class="btn btn-danger block-user" style="margin-right: 15px; font-size: 0.8rem; padding: .55rem 1.0rem;" data-id="<?=$DtaPost[iduser]?>" > <i class="fa fa-user-times" style="color: #FFF; margin-right: 3px;  cursor:pointer"></i> Block User</button>   
                                                                     <button class="btn btn-warning remove-post"  style="margin-right: 15px; font-size: 0.8rem; padding: .55rem 1.0rem;"  data-id="<?=$DtaPost[idpost]?>"> <i class="fal fa-ban"  style="color:  margin-right: 3px; cursor:pointer"></i> Remove Post</button> 
                                                                     <button class="btn btn-success add-comment"  style="font-size: 0.8rem; padding: .55rem 1.0rem;"  data-id="<?=$DtaPost[idpost]?>"> <i class="fal fa-comment"  style="color:  margin-right: 3px; cursor:pointer"></i> Add Comment</button> 
                                                                </span>  
                                                            <span class="m-widget19__username">
                                                                <?=$DtaPost[nameuser];?>
                                                            </span><br>
                                                                <span > <?= time_ago($DtaPost[date]);?> </span>
                                                                
                                                        </div>
                                                
                                            </div>
                                        </div>
                            
                                        <? if($DtaPost[photo]!=''){ ?>
                                        <div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px; margin-top: 0rem;">
                                            <img src="srvs/media/images/community/<?=$DtaPost[photo];?>">
                                            
                                            <div class="m-widget19__shadow"></div>
                                        </div>
                                    <? } ?>
                                        <div class="m-widget19__content">
                                            <div class="m-widget19__header" style="margin-bottom: 0rem; ">

                                                
                                            <div class="row" style="margin-bottom:20px;    display: table;"> 
                                                    <div class="col-md-3" style='display: table-cell; '>
                                                        <div style="font-size: 1rem; font-weight: 500;vertical-align: middle; line-height: 0;text-align: center;">
                                                            <span style="text-align: center; display: inline-block; margin-bottom: 1.3rem;  font-size: 1.5rem;  font-weight: 600;color: #08A6E4 !important;">
                                                                <i class="fal fa-thumbs-up cursor:pointer" style="margin-right: 7px;"></i> <?= $Cant[CantLike]; ?>
                                                            </span><br>
                                                            <span style="display: inline-block;font-size: .85rem;">Likes</span>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-3" style='display: table-cell; '>
                                                        <div style="font-size: 1rem; font-weight: 500;vertical-align: middle; line-height: 0;text-align: center;">
                                                            <span style="text-align: center; display: inline-block; margin-bottom: 1.3rem;  font-size: 1.5rem;  font-weight: 600;color: #8DB600 !important;">
                                                            <i class="fal fa-comment cursor:pointer"  style="margin-right: 7px;"></i> <?= $Cant[CantComment]; ?></span><br> 
                                                            <span style="display: inline-block;font-size: .85rem;">Comments</span>
                                                        </div>
                                                    </div>


                                            </div>


                                            <div class="m-widget19__body" >
                                            <? if($DtaPost[type]=='D') {?>
                                                <span class="text-success" style="margin-top:10px; padding: 3px 8px 3px 0px;font-weight: bold;">#Discution</span>
                                                <br>
                                            <? }else{ ?>
                                                <span class="text-danger" style="margin-top:10px;padding: 3px 8px 3px 0px;font-weight: bold;">#Question</span>
                                                <br>
                                            <? } ?>	

                                                <span style="color: #575962; font-size: 1.2em;"> <?=$DtaPost[text];?><br>
                                                </span>
                                               
                                            </div>
                                            <? 
                                            $sql = "SELECT
                                                 CONCAT(it_users.name,' ',last_name) AS nameuser ,avatar ,it_ratings.comment,it_ratings.date
                                                FROM it_ratings
                                                INNER JOIN it_posts ON it_ratings.item_id = it_posts.id
                                                INNER JOIN it_users ON   it_ratings.it_users_id = it_users.id
                                                WHERE item_type = 'P' AND it_ratings.type = 'C' AND it_posts.id = '".$DtaPost[id]."'";
                                                //echo $sql;
                                            $micon2->consulta($sql);
                                            while($DtaComment = $micon2->campoconsultaA()){ ?>
                                            <hr style="width: 100%">
                                            <div class="m-widget19__header p-0">
                                                <div class="m-widget19__user-img">
                                                    <img class="m-widget19__img" src="srvs/media/images/profiles/<?=$DtaComment[avatar];?>" style="width: 35px; height: 35px">
                                                </div>
                                                <div class="m-widget19__info">
                                                    <span class="m-widget19__username">
                                                        <?=$DtaComment[nameuser];?>
                                                    </span><br>
                                                    <span class="m-widget19__time">
                                                        <?=$DtaComment[comment];?>
                                                    </span>
                                                </div>
                                                <div style="width: 130px">
                                                     <span class="m-widget19__comment"><?=$DtaComment[date];?></span>
                                                </div>
                                            </div>
                                            <? } ?>
                                        </div>
                                        
                                    </div>
                                    </div>
                                </div>
                            </div>
    
  <?php  }
  
                                            }else{?>
                                            
                                            <div class="text-center">
											
                                                <m-empty-view _ngcontent-c23="" iconclass="fas fa-users" _nghost-c28="">
                                                    <div _ngcontent-c28="" class="text-center" style="padding: 2rem 0">
                                                        <div _ngcontent-c28="">
                                                            <i _ngcontent-c28="" style="font-size: 70px; margin-bottom: 20px; color: rgba(0,0,0,0.2)" class="fas fa-users"></i>
                                                        </div>
                                                        <div _ngcontent-c28="" style="max-width: 500px; margin: 0 auto">
                                                            <div _ngcontent-c23="" message="">
                                                                <span _ngcontent-c23="" translate="GROUPS.EMPTY_DATATABLE">
                                                                We have not found community posts.
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </m-empty-view>

                                            </div>     
        <?php
                    }