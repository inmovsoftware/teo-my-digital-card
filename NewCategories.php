<?
session_start();
require('include/mysql_class.php');
include('include/seguridad.php');


$page_actual = explode('?',$_SERVER['REQUEST_URI']);
$page_actual = $page_actual[0];

$archivo_actual = basename($_SERVER['PHP_SELF']);
$sql="SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS options, path, it_modules.id
	FROM it_modules
	INNER JOIN it_options ON it_modules.id = it_options.it_modules_id
	WHERE `path`='$archivo_actual'
	ORDER BY it_modules.order, it_options.order"; 
    
$micon->consulta($sql);$migas=$micon->campoconsultaA();
$opName=$migas[OptionsName];
//echo $sql;
if($migas[OptionsLevel]==2){
	$opId	=$migas[id];
	$opName=$migas[options];
	$sql="SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS options, path, it_modules.id
	FROM it_modules
	INNER JOIN it_options ON it_modules.id = it_options.it_modules_id
		WHERE `it_options`.id='$opId'"; 
	$micon->consulta($sql);$migas=$micon->campoconsultaA();	
}
$page_title = "$migas[module]";

/* revisar si se va a editar la categoria */
if ($_GET[edit]=='true'){
	$Action = 'Update';

	$sql = "SELECT id,name,concat('#',color) as color FROM it_categories_news WHERE id = '$_GET[id]'";
	$micon->consulta($sql);
	$DtaCategoryE =  $micon->campoconsultaA();
}else{
	$Action = 'Insert';
}

// - - - - - - - - - - - Navegador Detected - - - - - - - - - - -


/*Funcion que devuelve el Navegador Actual*/
function obtenerNavegadorWeb()
{
    $agente = $_SERVER['HTTP_USER_AGENT'];
    $navegador = 'Unknown';

    //Obtener el UserAgente
    if(preg_match('/MSIE/i',$agente) && !preg_match('/Opera/i',$agente))
    {
        $navegador = 'Internet Explorer';
        $navegador_corto = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$agente))
    {
        $navegador = 'Mozilla Firefox';
        $navegador_corto = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$agente))
    {
        $navegador = 'Google Chrome';
        $navegador_corto = "Chrome";
    }
    elseif(preg_match('/Safari/i',$agente))
    {
        $navegador = 'Apple Safari';
        $navegador_corto = "Safari";
    }
    elseif(preg_match('/Opera/i',$agente))
    {
        $navegador = 'Opera';
        $navegador_corto = "Opera";
    }
    elseif(preg_match('/Netscape/i',$agente))
    {
        $navegador = 'Netscape';
        $navegador_corto = "Netscape";
    }


    return array(
        'nombre'      => $navegador_corto,
    );

}


$ew = obtenerNavegadorWeb();
$navegador = $ew['nombre']; //Nombre del Navegador en Uso
$version= $ew['version']; //Version
$plataforma= $ew['platforma']; //Plataforma

?>
<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title><?=$migas[module];?> | <?=$migas[options];?></title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

        <style>
            .color-input{
                padding: 7px !important;
                border: 1px solid #ebedf2 !important;
                border-radius: 3px !important;
            }
            .color-dtl{
                width: 91% !important;
                border: 1px solid #e2e2e2 !important;
                height: 24px !important;
            }
        </style>

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.min.js"></script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		<link href="vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--begin::Page Vendors Styles -->
		<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">

						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="index.html" class="m-brand__logo-wrapper">
										<img alt="" src="assets/demo/media/img/logo/logo_default_dark.png" />
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">

									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>

									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
								
							</div>

							<!-- END: Horizontal Menu -->

							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click"
										 m-dropdown-persistent="1">
											<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
												<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
												<span class="m-nav__link-icon"><i class="flaticon-alarm"></i></span>
											</a>
											<? require('php/notifications.php');?>
										</li>
										<li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-text">
													<img class="m-topbar__language-selected-img" src="assets/app/media/img/flags/020-flag.svg">
												</span>
											</a>
											<? require('php/language.php');?>
										</li>
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
										 m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="srvs/media/images/profiles/<?=$_SESSION['UserAvatar'];?>" style="width: 41px; height: 41px" class="m--img-rounded m--marginless" alt="" />
												</span>
												<span class="m-topbar__username m--hide">Nick</span>
											</a>
											<? require('php/profile.php');?>
										</li>
									</ul>
								</div>
							</div>

							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>

			<!-- END: Header -->

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

					<!-- BEGIN: Aside Menu -->
					<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							
						<?
						$OpenStatus = '';
						$sql  = "SELECT it_modules.name AS module ,it_modules.icon,it_options.name AS OPTIONS, path, it_modules.id as ModId
								FROM it_profile_option
								INNER JOIN it_options ON it_profile_option.it_options_id = it_options.id
								INNER JOIN it_modules ON it_modules.id = it_options.it_modules_id
								WHERE it_profile_option.it_profiles_id = '".$_SESSION['UserProfile']."' and it_options.status='A'
								group by it_modules.name
								ORDER BY it_modules.order, it_options.order";
						$micon->consulta($sql);
						while($DtaModule = $micon->campoconsultaA()){	  
							if($_GET[open] == $DtaModule[ModId]){
								$OpenStatus = ' m-menu__item--expanded m-menu__item--open';
							}else{
								$OpenStatus = '';
							}
						?>
							<li class="m-menu__item  m-menu__item--submenu <?=$OpenStatus;?>" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon <?=$DtaModule[icon];?>"></i><span class="m-menu__link-text"><?=$DtaModule[module];?></span><i
									 class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									
									<ul class="m-menu__subnav">
										<?
										$active = '';
										$sql = "SELECT it_options.name AS options, path
												FROM it_profile_option
												INNER JOIN it_options ON it_profile_option.it_options_id = it_options.id
												WHERE it_modules_id = '".$DtaModule[ModId]."' and it_profile_option.it_profiles_id = '".$_SESSION['UserProfile']."' AND it_options.status='A'
												ORDER BY  it_options.order";
										$micon1->consulta($sql);	
										while($Dtaoption = $micon1->campoconsultaA()){
											$Url = '/'.$Dtaoption[path];
											if($page_actual == $Url){
												$active = 'm-menu__item--active';
											}else{
												$active = '';
											}
										?>
										<li class="m-menu__item <?=$active;?>" aria-haspopup="true"><a href="<?=$Dtaoption[path].'?open='.$DtaModule[ModId];?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?=$Dtaoption[options]?></span></a></li>
									<? } ?>
									</ul>

								</div> 
							</li>
						<?
						}	
						?>	
						</ul>
					</div>

					<!-- END: Aside Menu -->
				</div>

				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<div class="m-subheader "> 
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									<i class="fal  <?=$migas[icon]?>"></i> <?=$migas[module]?></h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text"><?=$migas[options]?></span>
										</a> 
									</li>
								</ul>
							</div>

							<!--estacio para indacadores -->
							<div class="col-lg-4 col-xl-4 col-md-12 col-xs-12">
								<div class="m-portlet__body  m-portlet__body--no-padding">

							</div>
								
							</div>
							<!--fin estacio para indacadores -->
						</div>
					</div>

					<!-- END: Subheader -->
					<div class="m-content">

						<!--Begin::Section-->
						<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<? if($_GET[edit]=='true'){ echo 'Edit';}else{ echo 'Create';} ?> Category
										</h3>
									</div>
								</div>

							</div>
							<div class="m-portlet__body">

								<!--begin: Datatable -->
								<form class="m-form m-form--state m-form--fit m-form--label-align-right" action="php/NewsCategoriesCreate.php" method="post" id="FormCategory">
									
									<div class="form-group m-form__group row">
										<div class="col-lg-4">
											<label>Category name:</label>
											<div class="m-input-icon m-input-icon--right">
												<input type="text" class="form-control m-input" placeholder="Category name" name="CategoryName" id="CategoryName" value="<?=$DtaCategoryE[name];?>">
												<input type="hidden" name="companyID" value="<?=$_SESSION['companyID'];?>">
												<input type="hidden" name="action" value="<?=$Action;?>">
												<input type="hidden" name="id" value="<?=$DtaCategoryE[id];?>">
												<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fal  fal fa-signature"></i></span></span>
											</div>
										</div>
										<div class="col-lg-4">
										<label class="">Select color:</label>
                                            <?php if ($navegador == 'Safari'){ ?>
                                                <div class="m-input-icon m-input-icon--right  color-input">
                                                    <input class="color-dtl form-control m-input jscolor {hash:true}" name="CategoryColor"  value="<?=$DtaCategoryE[color];?>">
                                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="far fa-paint-brush"></i></span></span>
                                                </div>
                                            <?php }else{ ?>
											<div class="m-input-icon m-input-icon--right">
												<input type="color" class="form-control m-input" placeholder="Extra Info Company" name="CategoryColor"  value="<?=$DtaCategoryE[color];?>">
												<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fal  fal fa-paint-brush"></i></span></span>
											</div>
                                            <?php } ?>
                                        </div>
										<div class="col-lg-4">
											<div class="text-right">
												<div class="m-portlet__head-tools">
													<a href="#" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
														<span>
															<i class="fal fa-times"></i>
															<span>Cancel</span>
														</span>
													</a>
													<div class="btn-group">
														<button type="submit" class="btn btn-danger  m-btn m-btn--icon m-btn--wide m-btn--md">
															<span>
																<i class="fal fa-newspaper"></i>
																<span>Save</span>
															</span>
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Category Listing
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item"></li>
										<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
											<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
												Actions
											</a>
											<div class="m-dropdown__wrapper" style="z-index: 101;">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36px;"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav">
																<li class="m-nav__section m-nav__section--first">
																	<span class="m-nav__section-text">Export Tools</span>
																</li>
																<li class="m-nav__item">
																	<a href="#" class="m-nav__link" id="export_print">
																		<i class="m-nav__link-icon la la-print"></i>
																		<span class="m-nav__link-text">Print</span>
																	</a>
																</li>
																<li class="m-nav__item">
																	<a href="#" class="m-nav__link" id="export_copy">
																		<i class="m-nav__link-icon la la-copy"></i>
																		<span class="m-nav__link-text">Copy</span>
																	</a>
																</li>
																<li class="m-nav__item">
																	<a href="#" class="m-nav__link" id="export_excel">
																		<i class="m-nav__link-icon la la-file-excel-o"></i>
																		<span class="m-nav__link-text">Excel</span>
																	</a>
																</li>
																<li class="m-nav__item">
																	<a href="#" class="m-nav__link" id="export_csv">
																		<i class="m-nav__link-icon la la-file-text-o"></i>
																		<span class="m-nav__link-text">CSV</span>
																	</a>
																</li>
																<li class="m-nav__item">
																	<a href="#" class="m-nav__link" id="export_pdf">
																		<i class="m-nav__link-icon la la-file-pdf-o"></i>
																		<span class="m-nav__link-text">PDF</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">

								<!--begin: Datatable -->
								<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
									<thead>
										<tr>
											<th>ID</th>
											<th>Company Name</th>
											<th>Category</th>
											<th>Color</th>
											<th>Status</th>
											<th>Created at</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?
										$sql = "SELECT it_categories_news.id, it_categories_news.name,color,it_categories_news.status,it_categories_news.created_at,it_business.name as business
											FROM  it_categories_news
											INNER JOIN it_business ON it_business.id = it_categories_news.it_business_id";
											if($_SESSION['UserBranch']='true'){
												$sql .=" WHERE it_business_id ='".$_SESSION['companyID']."'";
											}	
											$micon->consulta($sql);
											while($DtaContegory = $micon->campoconsultaA()){	
										?>
										<tr>
											<td><?=$DtaContegory[id];?></td>
											<td><?=$DtaContegory[business];?></td>
											<td><?=$DtaContegory[name];?></td>
											<td class="text-center"><span class="badge" style="background-color: #<?=$DtaContegory[color];?>;
										">#<?=$DtaContegory[color];?></span></td>
											<td class="text-center">
												<? if($DtaContegory[status]=='A'){?>
													<span class="m-badge m-badge--primary m-badge--wide">Active</span>
												<?}else{?>
													<span class="m-badge m-badge--danger m-badge--wide">Suspend</span>
												<? } ?>	
												</td>
											<td><?=$DtaContegory[created_at];?></td>
											<td>
												<a href='NewCategories.php?open=<?=$_GET[open]?>&edit=true&id=<?=$DtaContegory[id];?>' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' title='Edit'><i class='la la-edit'></i></a>
												<? if($DtaContegory[status]=='A'){?>
													<a href='php/NewsCategoriesCreate.php?delete=true&id=<?=$DtaContegory[id];?>&open=<?=$_GET[open]?>' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' title='Suspend'><i class='fal fa-trash-alt'></i></a>
												<?}else{?>	
													<a href='php/NewsCategoriesCreate.php?activate=true&id=<?=$DtaContegory[id];?>&open=<?=$_GET[open]?>' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' title='Activate'><i class='fal fa-check'></i></a>
												<? }?>
											</td>
										</tr>
										<?
											}
										?>
									</tbody>
								</table>
							</div>
						</div>


					</div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			<footer class="m-grid__item		m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								<?=date('Y')?> &copy; Inmov S.A.S. 
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<!-- lado derecho del footer -->
						</div>
					</div>
				</div>
			</footer>

			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->



		<!-- end::Quick Sidebar -->

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<div class="modal fade" id="m_modal_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Create Position </h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form>
												<div class="form-group">
													<label for="recipient-name" class="form-control-label">Position:</label>
													<input type="text" class="form-control" id="recipient-name">
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-danger">Create Position</button>
										</div>
									</div>
				</div>

		</div>

		<!-- end::Scroll Top -->

	

		<!--begin:: Global Mandatory Vendors -->
		<script src="vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="assets/demo/base/scripts.bundle.js" type="text/javascript"></script>



		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
		<script src="components/NewCategory.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>

		<script type="text/javascript">
		var FormControls = function () {
	    //== Private functions
	    

	    var demo3 = function () {
	        $( "#FormCategory" ).validate({
	            // define validation rules
	            rules: {
	                //=== Client Information(step 3)
	                //== Billing Information
	                CategoryName: {
	                    required: true
	                },
	                CategoryColor: {
	                    required: true,
	                }
	            },
	            
	            //display error alert on form submit  
	            invalidHandler: function(event, validator) {
	                mUtil.scrollTo("m_form_3", -200); 

	                swal({
	                    "title": "", 
	                    "text": "There are some errors in your submission. Please correct them.", 
	                    "type": "error",
	                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide",
	                    "onClose": function(e) {
	                        console.log('on close event fired!');
	                    }
	                });

	                event.preventDefault();
	            },

	            submitHandler: function (form) {
	                form[0].submit(); // submit the form
	                swal({
	                    "title": "", 
	                    "text": "Form validation passed. All good!", 
	                    "type": "success",
	                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
	                });

	                return false;
	            }
	        });       
	    }

	    return {
	        // public functions
	        init: function() {
	            demo3(); 
	        }
	    };
	}();

	jQuery(document).ready(function() {    
	    FormControls.init();
	});
		</script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>