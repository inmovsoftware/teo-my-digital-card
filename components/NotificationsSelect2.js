var Select2={
	init:function(){
		$("#UsersPositions").select2({
			placeholder:"Select a Positions Users"
		}),
		$("#UsersGroup").select2({
			placeholder:"Select a Group of Users"
		}),
		$("#OperativeSystem").select2({
			placeholder:"Select a Operative System"
		}),
		$("#SelectUsers").select2({
			placeholder:"Select Users"
		}),
		$("#SelectPartners").select2({
			placeholder:"Select Partners"
		});
	}
};
jQuery(document).ready(function(){
	Select2.init()
});