var DatatablesExtensionButtons = {
	init: function () {
		var t;
		 t = $("#m_table_2").DataTable({
			responsive: !0,
			buttons: ["print", "copyHtml5", "excelHtml5", "csvHtml5", "pdfHtml5"],
			processing: !0,
			serverSide: !0,
			ajax: {
				url: "../php/positionsList.php",
				type: "POST",
				data: {
					columnsDef: ["name", "status", "cantidad", "actions"]
				}
			},
			columns: [{
				data: "name"
			}, {
				data: "cant"
			}, {
				data: "status"
			}, {
                    "defaultContent": "<button type='button' class='selec ttt btn btn-primary sm'></button><a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' title='Edit'><i class='la la-edit'></i></a>'",
                    "className": "text-center"
                
			}],
			columnDefs: [ 
				{
				targets: 2,
				render: function (t, e, a, n) {
					var s = {
						'A': {
							title: "Activate",
							class: "m-badge--brand"
						},
						'C': {
							title: "Inactive",
							class: " m-badge--danger"
						}
					};
					return void 0 === s[t] ? t : '<span class="m-badge ' + s[t].class + ' m-badge--wide">' + s[t].title + "</span>"
				}
			}]
			
		}), $("#export_print").on("click", function (e) {
			e.preventDefault(), t.button(0).trigger()
		}), $("#export_copy").on("click", function (e) {
			e.preventDefault(), t.button(1).trigger()
		}), $("#export_excel").on("click", function (e) {
			e.preventDefault(), t.button(2).trigger()
		}), $("#export_csv").on("click", function (e) {
			e.preventDefault(), t.button(3).trigger()
		}), $("#export_pdf").on("click", function (e) {
			e.preventDefault(), t.button(4).trigger()
		})
	}
};
jQuery(document).ready(function () {
	DatatablesExtensionButtons.init();
});
